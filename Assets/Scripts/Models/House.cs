// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.1
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Ucss;

public class House
{
	private bool _internal;

	public bool Internal {
		get {
			return _internal;
		}
	}

	GameObject _gameObject;

	public GameObject HouseGO {
		get {
			return _gameObject;
		}
		set {
			_gameObject = value;
		}
	}

	Texture2D _texture;

	public Texture2D HouseTexture {
		get {
			return _texture;
		}
		set {
			_texture = value;
		}
	}

	private static string _resourcePath;
	public static string ResourcePath {
		get {
			return _resourcePath;
		}
		set {
			_resourcePath = value;
		}
	}
	Product _product;

	public Product Product {
		get {
			return _product;
		}
	}

	bool _loading;
	public bool Loading{
		get{
			return _loading;
		}
	}

	float _lowestY;

	Material _material;
	Material _selectedMaterial;

	public House (Product product)
	{
		_product = product;
		_loading = true;

		if (product.Pattern.Equals ("texture_house")) {
			_texture = (Texture2D)Resources.Load ("images/default-house");
			CreateGameObject (_product);
			_internal = true;
		} else if (product.Pattern.StartsWith ("/")) {
			#if UNITY_WEBPLAYER
			#else
			byte[] fileData;

			if (File.Exists(product.Pattern))     {
				fileData = File.ReadAllBytes(product.Pattern);
				_texture = new Texture2D(2, 2);
				_texture.LoadImage(fileData); //..this will auto-resize the texture dimensions.
				CreateGameObject(_product);
			}
			#endif
		} else if (product.Pattern.StartsWith ("file://") || product.Pattern.StartsWith ("http")) {
			string url = product.Pattern;
			_internal = false;

			UCSS.HTTP.GetTexture (url, this.LoadCallback, this.LoadError);
		} else {
			string url = _resourcePath + product.Pattern;
			_internal = false;

			UCSS.HTTP.GetTexture (url, this.LoadCallback, this.LoadError);
		}
	}

	void LoadError(string error, string id){
		Debug.Log("House.Load download error:" + error);
		GV_UI.ShowError ( _product.Pattern + " not found");
		GV_UI.GardenScript.LoadNextProduct ();
	}

	void LoadCallback(Texture2D texture, string id){
		Debug.Log("House.Load texture:" + id);
		_texture = texture;
		CreateGameObject(_product);
		GV_UI.GardenScript.LoadNextProduct ();
	}

	public void AdjustScale(float delta){
		//float lowestY = _lowestY * _gameObject.transform.localScale.x;
		//Vector3 pos = _gameObject.transform.position;
		//float posY = pos.y * _gameObject.transform.localScale.x;
		//pos.y = -_lowestY;
		//_gameObject.transform.position = pos;
		_gameObject.transform.localScale += new Vector3(delta, delta, delta);
		//pos.y = posY/_gameObject.transform.localScale.x;
		//_gameObject.transform.position = pos;
	}

	public void SetHeight(float delta){
		_gameObject.transform.Translate (0, delta, 0);
		Vector3 pos = _gameObject.transform.position;
		float lowestY = _lowestY * _gameObject.transform.localScale.x;
		if (pos.y<-lowestY) pos.y = -lowestY;
		if (pos.y>(1-lowestY)) pos.y = 1-lowestY;
		_gameObject.transform.position = pos;
		pos.y += lowestY;
		_product.Position = pos * 1000;
	}

	public int GetScaledSize(){
		if (_gameObject == null) return (int)_product.Size;
		return (int)(_product.Size * _gameObject.transform.localScale.x);
	}

	public void CreateGameObject(Product product){
		Vector2 size = new Vector2(product.Size, product.Size/product.AspectRatio);
		size *= 0.001f;
		// Create Vector2 vertices
		Vector2[] vertices2D = new Vector2[product.SourceVertices.Count];
		Vector2[] uvs = new Vector2[product.SourceVertices.Count];

		//Debug.Log ("House.CreateGameObject texture size " + _texture.width + " x " + _texture.height);
		int i=0;
		float lowestY = 1000000000.0f;
		_lowestY = 0.0f;

		foreach(Vector2 v in product.SourceVertices){
			uvs[i] = new Vector2(v.x/(float)_texture.width, 1.0f - v.y/(float)_texture.height);
			vertices2D[i] = new Vector2( uvs[i].x * size.x - size.x*0.5f, uvs[i].y * size.y );
			if (vertices2D[i].y<lowestY) lowestY = vertices2D[i].y;
			i++;
		}

		for(i=0; i<vertices2D.Length; i++){
			vertices2D[i].y -= lowestY;
		}
		// Use the triangulator to get indices for creating triangles
		Triangulator tr = new Triangulator(vertices2D);
		int[] indices = tr.Triangulate(false);

		//Flip all the triangles
		for(i=0; i<indices.Length; i+=3){
			int tmp = indices [i];
			indices [i] = indices [i + 2];
			indices [i + 2] = tmp;
		}
		
		// Create the Vector3 vertices
		Vector3[] vertices = new Vector3[vertices2D.Length];
		for (i=0; i<vertices.Length; i++) {
			vertices[i] = new Vector3(0, vertices2D[i].y, vertices2D[i].x);
			//Debug.Log ("House.CreateGameObject " + i + ":" + vertices[i].ToString ());
		}

		GameObject go = new GameObject( product.Name );
		go.AddComponent<MeshFilter>();
		go.AddComponent<MeshRenderer>();

		_material = new Material (Shader.Find("Custom/Driveway"));
		_selectedMaterial = new Material (Shader.Find("Custom/DrivewaySelected"));
		_selectedMaterial.mainTexture = _texture;
		_material.mainTexture = _texture;

		MeshFilter filter = go.GetComponent<MeshFilter>();
		Renderer renderer = go.GetComponent<MeshRenderer>().GetComponent<Renderer>();
		renderer.material = _material;

		// Create the mesh
		Mesh mesh = filter.mesh;
		mesh.vertices = vertices;
		mesh.triangles = indices;
		mesh.uv = uvs;
		mesh.RecalculateNormals();
		mesh.RecalculateBounds();

		go.transform.position = product.Position * 0.001f;
		//go.transform.Translate (0, -_lowestY, 0);
		go.transform.rotation = Quaternion.Euler (0, (float)_product.Rotation, 0);

		go.AddComponent<BoxCollider>();

		_gameObject = go;
		_product.GameObject = _gameObject;

		_loading = false;
		//go.transform.parent = GardenScript.Root;

		/*if (LoginScript.gardenScript!=null){
			LoginScript.gardenScript.AddChild(go);
		}else if (TestPatternScript._gardenScript!=null){
			TestPatternScript._gardenScript.AddChild(go);
		}*/

	}

	public void Highlight(bool selected){
		if (_gameObject==null || _selectedMaterial==null || _material==null) return;
		if (selected){
			_gameObject.GetComponent<Renderer>().material = _selectedMaterial;
		}else{
			_gameObject.GetComponent<Renderer>().material = _material;
		}
	}
}