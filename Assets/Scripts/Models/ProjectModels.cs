using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.models
{
	public class CreateProjectModel
	{
		public string Guid { get; set; }
		public float PlannerVersion { get; set; }
		public string Name { get; set; }
		public float Size { get; set; }
		public string Xml { get; set; }
		public string ProjectImageFileContent { get; set; }
		public List<ProjectPhoto> ProjectPhotos{ get; set; }
	}

	public class CreateNewProjectModel
	{
		public float PlannerVersion { get; set; }
		public string Name { get; set; }
		public float Size { get; set; }
		public string Xml { get; set; }
		public string ProjectImageFileContent { get; set; }
	}

	public class ProjectPhoto{
		public string Guid{ get; set; }
		public string FileContent{ get; set; }

		public ProjectPhoto(string guid){
			Guid = guid;
			FileContent = "";
		}
	}

	public class ProjectPDF{
		public string ProjectId{ get; set; }
		public string EmailAddress{ get; set; }
		public string [] ProjectShots{ get; set; }
	}
}

