// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.1
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using System;
using System.Xml;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;

public class Project
{
	private bool _invertZ = false;

	public bool InvertZ {
		get {
			return _invertZ;
		}
		set {
			_invertZ = value;
		}
	}

	private String version;
	private string guid = "";

	public string Guid {
		get {
			guid = guid.Replace("\"", "");
			return guid;
		}
		set {
			value = value.Replace("\"", "");
			guid = value;
		}
	}

	private float _size;

	public float Size {
		get {
			if (_size==0) return constraints.x * constraints.y / 1000000;
			return _size;
		}
		set {
			_size = value;
		}
	}

	private string _thumbnail;

	public string Thumbnail {
		get {
			return _thumbnail;
		}
		set {
			_thumbnail = value;
		}
	}

	private String visualiser;
	private String brand;
	private String projectSerial;
	private String productSerial;

	private String name;

	public String Name {
		get {
			return name;
		}
		set {
			name = value;
		}
	}

	private bool cuts;
	private String userEmail;
	private String displayUnit;
	private Vector2 constraints;

	public Vector2 Constraints {
		get {
			return constraints;
		}
		set {
			constraints = value;
		}
	}

	private String pavingShape;
	private int editStepCurrent;
	private int editStepAllowed;

	private List<Product> products = new List<Product>();
	public List<Product> Products{
		get{
			return products;
		}
	}

	private List<Snapshot> _shots = new List<Snapshot>();

	public List<Snapshot> Shots {
		get {
			return _shots;
		}
		set {
			_shots = value;
		}
	}

	private List<string> _photos = new List<string>();
	
	public List<string> Photos {
		get {
			return _photos;
		}
		set {
			_photos = value;
		}
	}

	public float MaxProductY{
		get{
			float y = (products.Count>0) ? products[0].Position.y : 0;
			foreach(Product product in products){
				if (product.Position.y>y) y = product.Position.y;
			}
			return y;
		}
	}

	public Project(string _name, int length, int width){
		name = _name;
		constraints = new Vector2(length, width);
	}

	public Project(string xml, JSONArray photos){
		Load(xml);

		_photos = new List<string>();
			
		if (photos != null && photos.Count>0){
			for(int i=0; i<photos.Count; i++){
				JSONNode photo = photos[i];
				string photoGuid = (photo["Guid"] != null) ? photo["Guid"].ToString () : photo.ToString();
				_photos.Add ( photoGuid );
			}
		}
	}

	public Project (String xml)
	{
		Load(xml);
	}

	private void Load(string xml){
		XmlDocument doc = new XmlDocument();
		doc.LoadXml(xml);
		XmlNodeList project = doc.GetElementsByTagName( "project" );
		Debug.Log ("Project " + xml);

		if (project!=null){

			version = getAttribute(project[0], "visualiserversion");
			visualiser = getAttribute(project[0], "visualiser");
			brand = getAttribute(project[0], "manufacturerbrand");
			projectSerial = getAttribute(project[0], "projectserial");
			name = getAttribute(project[0], "name");
			cuts = (getAttribute(project[0], "plannerversion") == "true");
			userEmail = getAttribute(project[0], "useremail");
			guid = getAttribute(project[0], "guid");

			XmlNodeList settings = doc.GetElementsByTagName ("settings");

			if (settings!=null && settings.Count>0 && settings[0].HasChildNodes){
				constraints = new Vector2();
				for (int i=0; i<settings[0].ChildNodes.Count; i++)
				{
					XmlNode setting = settings[0].ChildNodes[i];
					String attr = getAttribute(setting, "name");
					String value = getAttribute(setting, "value");

					switch(attr){
					case "displayunit":
						displayUnit = value;
						break;
					case "constraintx":
						float.TryParse(value, out constraints.x);
						break;
					case "constrainty":
						float.TryParse(value, out constraints.y);
						break;
					case "constraintz":
						float.TryParse(value, out constraints.y);
						break;
					case "pavingShape":
						pavingShape = value;
						break;
					case "productserial":
						productSerial = value;
						break;
					case "editstepcurrent":
						Int32.TryParse(value, out editStepCurrent);
						break;
					case "editstepallowed":
						Int32.TryParse(value, out editStepAllowed);
						break;
					}
				}
			}

			XmlNodeList shots = doc.GetElementsByTagName ("shots");
			
			if (shots!=null && shots.Count>0 && shots[0].HasChildNodes){
				for (int i=0; i<shots[0].ChildNodes.Count; i++)
				{
					Snapshot shot = new Snapshot(shots[0].ChildNodes[i]);
					_shots.Add (shot);
				}
			}

			XmlNodeList productsList = doc.GetElementsByTagName ("products");

			products = new List<Product>();

			if (productsList != null && productsList.Count>0){
				for(int i=0; i<productsList[0].ChildNodes.Count; i++){
					products.Add ( new Product(productsList[0].ChildNodes[i] ) );
				}
			}

			Debug.Log (ToString() );
		}

			//products.Attributes["name"].Value;
	}

	public int NextID(){
		int id = 0;

		foreach(Product product in products){
			if (product==null||product.Name==null||product.Name.Length<8) continue;
			try{
				Debug.Log("Project.NextID name:" + product.Name);
				int idx = Int32.Parse(product.Name.Substring(8));
				if (idx>id) id = idx;
			}catch (FormatException e){
				Debug.Log("Project.NextID error:" + e.Message);
			}
		}

		return id + 1;
	}

	public void Add(Product product){
		products.Add (product);
	}

	public void AddPhoto(string guid){
		if (_photos == null) _photos = new List<string>();
		_photos.Add (guid);
	}

	String getAttribute(XmlNode node, String str){
		XmlAttribute attr = node.Attributes[str];
		if (attr==null) return "";
		return attr.Value;
	}

	public string ToXML(){
		string xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<xml>\n";
		//string xml = "<xml>\n";
		//string xml = "";
		xml += "<project visualizerversion=\"" + version + "\" visualizer=\"garden\" name=\"" + name + "\">\n" + 
			"\t<settings>\n" +
			"\t\t<setting name=\"displayunit\" value=\"" + displayUnit + "\" />\n" +
			"\t\t<setting name=\"constraintx\" value=\"" + constraints.x + "\" />\n" +
			"\t\t<setting name=\"constrainty\" value=\"" + constraints.y + "\" />\n" +
			"\t\t<setting name=\"productserial\" value=\"" + productSerial + "\" />\n" +
			"\t</settings>\n" +
			"\t<products>\n";
		foreach(Product product in products) xml += product.ToXML ();
		xml += "\t</products>\n\t<shots>\n";
		foreach(Snapshot snapshot in _shots) xml += snapshot.ToXML ();
		xml += "\t</shots>\n<quantities />\n</project>\n</xml>";

		return xml;
	}

	public string LogPhotos(){
		string str = "";

		if (_photos!=null){
			bool first = true;
			foreach(string photo in _photos){
				if (!first) str += ", ";
				str = str + photo;
				first = false;
			}
		}

		return str;
	}

	public void RemovePhoto(string guid){
		Debug.Log ("Project.RemovePhoto guid:" + guid + " count before:" + _photos.Count);
		_photos.Remove (guid);
		Debug.Log ("Project.RemovePhoto guid:" + guid + " count after:" + _photos.Count);
	}

	override public String ToString(){
		return "Project Guid:" + guid + " version:" + version + " brand:" + brand + " projectSerial:" + projectSerial + " name:" + name + 
			" displayUnit:" + displayUnit + " pavingShape:" + pavingShape + " productSerial:" + productSerial + 
			" editStepCurrent:" + editStepCurrent + " editStepAllowed:" + editStepAllowed + 
				" constraints:(" + constraints.x + ", " + constraints.y + ") products:" + products.Count + " photos:" + LogPhotos();
	}
	
}

