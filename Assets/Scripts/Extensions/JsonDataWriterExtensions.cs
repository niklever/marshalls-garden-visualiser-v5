﻿using JsonFx.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace JsonFx.Json
{
	public static class JsonDataWriterExtensions
	{
		public static byte[] SerializeToByteArray(this JsonDataWriter writer, object obj)
		{
			string json = writer.SerializeToString(obj);
			
			return UTF32Encoding.Default.GetBytes(json);
		}

		public static string SerializeToString(this JsonDataWriter writer, object obj)
		{
			using (StringWriter sw = new StringWriter())
			{
				writer.Serialize(sw, obj);

				StringBuilder builder = sw.GetStringBuilder();
				string json = builder.ToString();

				return json;
			}
		}
	}
}