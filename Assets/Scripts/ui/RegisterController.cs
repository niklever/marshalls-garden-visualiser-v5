﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RegisterController : MonoBehaviour {
	GV_UI _gv_ui;

	// Use this for initialization
	void Start () {
		GameObject go = GameObject.Find ("Canvas");
		_gv_ui = go.GetComponent<GV_UI>();
	}
	
	public void BackPressed(){
		GV_UI.Animate ("loginIn");
	}

	public void CreatePressed(){
		GameObject go = transform.Find ("FirstNameTxt").gameObject;
		InputField txt = go.GetComponent<InputField>();
		string firstname = txt.text;
		go = transform.Find ("LastTxt").gameObject;
		txt = go.GetComponent<InputField>();
		string lastname = txt.text;
		go = transform.Find ("EmailTxt").gameObject;
		txt = go.GetComponent<InputField>();
		string email = txt.text;
		go = transform.Find ("PasswordTxt").gameObject;
		txt = go.GetComponent<InputField>();
		string password = txt.text;
		GVWebService.CreateAccount(firstname, lastname, email, password);
		Analytics.ga ("User account", "Create account");
	}
}
