﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RealityController : MonoBehaviour {
	Image _sent;
	InputField _emailTxt;
	GameObject _emailGO;
	GameObject _emailLabel;
	SnapshotController _snapshotController;

	public void Init(){
		GameObject go = transform.Find ("sent").gameObject;
		_sent = go.GetComponent<Image>();
		_sent.enabled = false;
		_emailLabel = transform.Find ("Text").gameObject;
		_emailGO = transform.Find ("EmailTxt").gameObject;
		_emailTxt = _emailGO.GetComponent<InputField>();
	}

	public void Show(){
		gameObject.SetActive(true);
		_emailGO.SetActive (true);
		_emailLabel.SetActive (true);
		_sent.enabled = false;
	}

	public void ClosePressed(){
		GV_UI.CloseModalPanel();
	}

	public void InstallerPressed(){
		GV_UI.ShowModalPanel("Installer");
	}

	public void StockistPressed(){
		GV_UI.ShowModalPanel("Stockist");
	}

	public void PDFPressed(){
		_emailGO.SetActive (false);
		_emailLabel.SetActive (false);
		_sent.enabled = true;
		if (_snapshotController == null) {
			GameObject go = GameObject.Find ("ThumbnailCamera");
			if (go) _snapshotController = go.GetComponent<SnapshotController> ();
		}

		if (_snapshotController != null) {
			_snapshotController.Grab (GardenScript.Project.Shots, this);
		} else {
			GV_UI.ShowAlert ("Warning", "No Snapshot Controller found");
		}
	}

	public void SendPDF(){
		GVWebService.SendPDF(_emailTxt.text);
		Analytics.ga ("Project", "Generate PDF");
	}
}
