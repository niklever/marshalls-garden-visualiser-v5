﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class ForgotController : MonoBehaviour {
	GV_UI _gv_ui;
	EventSystem _es;

	public string UserName;
	
	// Use this for initialization
	void Start () {
		GameObject go = GameObject.Find ("Canvas");
		_gv_ui = go.GetComponent<GV_UI>();
		_es = EventSystem.current;
	}

	public void Update()
	{

		if (Input.GetKeyDown(KeyCode.Tab))
		{
			Selectable next = _es.currentSelectedGameObject.GetComponent<Selectable>().FindSelectableOnDown();

			if (next!= null) {

				InputField inputfield = next.GetComponent<InputField>();
				if (inputfield !=null) inputfield.OnPointerClick(new PointerEventData(_es));  //if it's an input field, also set the text caret

				_es.SetSelectedGameObject(next.gameObject, new BaseEventData(_es));
			}

		}
	}

	public void BackPressed(){
		GV_UI.Animate ("loginIn");
	}

	public void ForgotPressed(){
		Transform t = transform.Find ("EmailTxt");
		UserName = "";
		if (t!=null){
			GameObject go = t.gameObject;
			if (go) {
				InputField ip = t.gameObject.GetComponent<InputField> ();
				UserName = ip.text;
			}
		}
		GVWebService.ForgottenPassword (this);
	}

	public void ForgottenPasswordSuccess(){
		GV_UI.ShowAlert("Forgotten password", "A reminder has been sent to your email address");
		GV_UI.Animate ("loginIn");
	}
}
