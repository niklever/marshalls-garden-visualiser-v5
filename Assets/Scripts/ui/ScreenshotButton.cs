﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ScreenshotButton : MonoBehaviour {
	GameObject _deleteBtn;
	GameObject _confirmBtn;
	float _confirmTime;
	bool _confirm = false;
	Snapshot _snapshot;
	public Snapshot Snapshot {
		get {
			return _snapshot;
		}
		set {
			_snapshot = value;
		}
	}

	// Use this for initialization
	void Start () {
		_deleteBtn = transform.Find ("DeleteButton").gameObject;
		_confirmBtn = transform.Find ("ConfirmButton").gameObject;
		_confirmBtn.SetActive (false);
		GameObject go = transform.Find ("Image").gameObject;
		if (go && _snapshot!=null && _snapshot.Texture!=null) {
			Image image = go.GetComponent<Image> ();
			Texture2D tex = (Texture2D)_snapshot.Texture;
			image.sprite = Sprite.Create (tex, new Rect (0, 0, tex.width, tex.height), Vector2.zero);
		}
	}

	void Update(){
		if (_confirm){
			float elapsedTime = Time.time - _confirmTime;
			if (elapsedTime>3){
				_confirm = false;
				_deleteBtn.SetActive (true);
				_confirmBtn.SetActive(false);
			}
		}
	}

	public void SelectPressed(){
		GameObject go = GameObject.Find ("3D");
		if (go) {
			GardenScript gardenScript = go.GetComponent<GardenScript> ();
			gardenScript.SnapshotSelected (_snapshot);
		}
		GV_UI.CloseModalPanel ();
	}

	public void DeletePressed(){
		Debug.Log ("ScreenshotButton.DeletePressed");
		_deleteBtn.SetActive (false);
		_confirmBtn.SetActive(true);
		_confirm = true;
		_confirmTime = Time.time;
	}

	public void ConfirmPressed(){
		Debug.Log ("ScreenshotButton.ConfirmPressed");
		Destroy(gameObject);
		GardenScript.Project.Shots.Remove (_snapshot);
		GardenScript.Modified = true;
	}
}
