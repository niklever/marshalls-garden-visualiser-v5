﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class CameraControlWidget : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {
	public bool _up;

	float _prevHeightY;
	Vector3 _initialMousePos;
	int _mouseMode;
	RectTransform _rt;
	Vector2 _moveCentre;
	CameraScript _cameraScript;
	bool _mouseDown;
	Camera _camera;

	// Use this for initialization
	void Start () {
		_rt = GetComponent<RectTransform> ();
		_moveCentre = _rt.sizeDelta/2;
		GameObject go = GameObject.Find ("Main Camera");
		_cameraScript = go.GetComponent<CameraScript> ();
		_mouseDown = false;
		go = GameObject.Find ("Canvas");
		Canvas canvas = go.GetComponent<Canvas> ();
		_camera = canvas.worldCamera;
	}

	void Update(){
		if (GV_UI.ButtonCapturedMouse && _mouseDown) {
			Vector3 pos = _rt.worldToLocalMatrix * Input.mousePosition;
			if (!_up) {
				Vector2 pt;
				RectTransformUtility.ScreenPointToLocalPointInRectangle(_rt, Input.mousePosition, _camera, out pt);
				Vector2 offset = new Vector2( pt.x, pt.y) * Time.deltaTime;
				_cameraScript.Move (offset.x, -offset.y);
			} else {
				float offset = pos.y - _prevHeightY;
				_prevHeightY = pos.y;
				GardenScript.AdjustHeight(offset * Time.deltaTime * 0.1f);
			}
		}
	}

	public void OnPointerDown(PointerEventData eventData)
	{
		Debug.Log("Mouse down");
		GV_UI.ButtonCapturedMouse = true;
		_mouseDown = true;
		if (_up) {
			Vector2 pos = _rt.worldToLocalMatrix * Input.mousePosition;
			_prevHeightY = pos.y;
		}
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		Debug.Log("Mouse up");
		GV_UI.ButtonCapturedMouse = false;
		_mouseDown = false;
	}
}
