﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BusyController : MonoBehaviour {
	RectTransform _loaderRT;

	// Use this for initialization
	void Start () {
		GameObject go = transform.Find ("Image").gameObject;
		_loaderRT = go.GetComponent<RectTransform>();
	}

	public void Show(){
		gameObject.SetActive (true);
	}

	public void Hide(){
		gameObject.SetActive (false);
	}

	// Update is called once per frame
	void Update () {
		if (gameObject.activeSelf){
			Vector3 rot = _loaderRT.rotation.eulerAngles;
			int segment = (int)((Time.time * 12) % 12);
			rot.z = -segment * 30;
			Quaternion rotQ = Quaternion.identity;
			rotQ.eulerAngles = rot;
			_loaderRT.rotation = rotQ;
		}
	}
}
