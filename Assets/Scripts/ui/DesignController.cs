﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public enum FooterButtons { DRAW, SQUARE, RECTANGLE, LSHAPE, CIRCLE, SEMICIRCLE, PAVING, FEATURES, PATHS, WALLING, FENCES, AGGREGATES, ACCESSORIES, 
	BUILDINGS, TREES, POSITION, ROTATE, POSITION_PATTERN, ROTATE_PATTERN, SCALE_HOUSE, FLIP_BOUNDARY, FLIP_EDGING, FLIP_KERB, FLIP_WALLING,
	ADD_DOLLY, ADD_PILLAR, CHANGE_PAVING, CHANGE_FEATURE, CHANGE_KERB, CHANGE_WALLING, CHANGE_FENCE, CHANGE_AGGREGATES, CHANGE_ACCESSORY,
	CHANGE_BUILDING, CHANGE_TREE, RESTART, UNDO};

public class DesignController : MonoBehaviour {
	string _editState = "";
	GardenScript _gardenScript;
	GV_UI _gv_ui;
	GameObject _snapshotsPanel;
	GameObject _bg;
	public GameObject Bg;
	GameObject _deleteBtn;
	GameObject _deleteConfirmText;
	GameObject _deleteConfirmBtn;
	GameObject _selected;
	GameObject _threeD;
	GameObject _housePanel;
	GameObject _header;
	GameObject _moveWidget;
	GameObject _heightWidget;
	GameObject _help;
	GameObject Help {
		get {
			if (_help == null) {
				bool active = gameObject.activeSelf;
				if (!active) gameObject.SetActive (true);
				_help = GameObject.Find ("Canvas/DesignPanel/Footer/Buttons/Help");
				gameObject.SetActive (active);
			}
			return _help;
		}
	}
	GameObject _cameraPanel;
	GameObject CameraPanel {
		get {
			if (_cameraPanel == null) {
				bool active = gameObject.activeSelf;
				gameObject.SetActive (true);
				_cameraPanel = transform.Find ("CameraPanel").gameObject;
				gameObject.SetActive (active);
			}
			return _cameraPanel;
		}
	}

	static AddHouseController _addHouseController;
	public static AddHouseController AddHouseController {
		set {
			_addHouseController = value;
		}
	}
	bool _closing;
	public bool Closing {
		get{ return _closing; }
	}
	BusyController _busy;
	public BusyController Busy {
		get {
			return _busy;
		}
	}
	FooterButtons _changeButton;

	// Use this for initialization
	void Start () {
		GameObject go = GameObject.Find ("Canvas");
		_gv_ui = go.GetComponent<GV_UI>();
		_snapshotsPanel = transform.Find ("SnapshotsPanel").gameObject;
		_snapshotsPanel.SetActive(false);
		_header = transform.Find ("Header").gameObject;
		_moveWidget = transform.Find ("MoveWidget").gameObject;
		_heightWidget = transform.Find ("HeightWidget").gameObject;
		if (_editState=="") SetFooterState("draw");
		_bg = GameObject.Find ("Canvas/Image");
		_deleteBtn = GameObject.Find ("Canvas/DesignPanel/Footer/DeleteButton/DeleteButton");
		_deleteConfirmText = GameObject.Find ("Canvas/DesignPanel/Footer/DeleteButton/Text");
		_deleteConfirmBtn = GameObject.Find ("Canvas/DesignPanel/Footer/DeleteButton/ConfirmButton");
		SetDeleteBtn(false);
		_threeD = GV_UI.GardenScript.gameObject;
		_gardenScript = GV_UI.GardenScript; 
		_selected = GameObject.Find ("Canvas/DesignPanel/Footer/Buttons/Selected");
		_selected.SetActive (false);
		_help = GameObject.Find ("Canvas/DesignPanel/Footer/Buttons/Help");
		_help.SetActive (false);
		_cameraPanel = GameObject.Find ("Canvas/DesignPanel/CameraPanel");
		_cameraPanel.SetActive (false);
		_housePanel = transform.Find ("HousePanel").gameObject;
		_housePanel.SetActive (false);
		go = transform.Find ("Header/Busy").gameObject;
		_busy = go.GetComponent<BusyController> ();
		_busy.Hide ();
		GardenScript.DesignController = this;
		_closing = false;
	}

	void SetDeleteBtn(bool confirm){
		if (_deleteBtn) _deleteBtn.SetActive (!confirm);
		if (_deleteConfirmText) _deleteConfirmText.SetActive (confirm);
		if (_deleteConfirmBtn) _deleteConfirmBtn.SetActive (confirm);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void AxisPressed(){
		ShowCameraControls (!_cameraPanel.activeSelf);
	}

	public void DeletePressed(){
		Debug.Log ("DesignController.DeletePressed");
		SetDeleteBtn(true);
	}

	public void DeleteConfirmPressed(){
		Debug.Log ("DesignController.DeleteConfirmPressed");
		_gardenScript.DeleteSelected();
		SetFooterState("edit");
	}

	void EnableThreeDChildren(bool show){
		for(int i=0; i<_threeD.transform.childCount; i++){
			Transform t = _threeD.transform.GetChild(i);
			if (t.gameObject != null) t.gameObject.SetActive(show);
		}
	}

	public void ChangePressed(){
		Debug.Log ("DesignController.ChangePressed");
		SetFooterState ("draw");
	}

	public void ChangeSizePressed(){
		Debug.Log ("DesignController.ChangeSizePressed");
		GV_UI.ShowModalPanel ("NewProject");
		GV_UI.NewProjectController.Project = GardenScript.Project;
	}

	public void CancelBPressed(){
		Debug.Log ("DesignController.CancelBPressed");
		GV_UI.CloseModalPanel ();
	}
		
	public void CancelPressed(){
		Debug.Log ("DesignController.CancelPressed");
		if (_editState == "crop_house") {
			GV_UI.ShowModalPanel ("StopCropHouse");
		} else {
			GV_UI.DrawShape.enabled = false;
			_gardenScript.MouseMode = (int)MouseModes._NONE;
			SetFooterState ("edit");
		}
	}

	public void StopCropHousePressed(){
		_gardenScript.MouseMode = (int)MouseModes._NONE;
		GV_UI.ButtonCapturedMouse = false;
		SetFooterState ("edit");
		GV_UI.CloseModalPanel ();
		_housePanel.SetActive(false);
		GV_UI.HouseDrawShape.Active = false;
		//TO DO clear vector line
	}

	public void DesignPressed(){
		Debug.Log ("DesignController.DesignPressed");
		SetFooterState ("edit");
	}

	public void AddHousePressed(){
		Debug.Log ("DesignController.AddHousePressed");
		GV_UI.ShowModalPanel ("AddHouse");
	}

	public void BackPressed(){
		Debug.Log ("DesignController.BackPressed");
		//GV_UI.DrawShape.Clear();
		_closing = true;
		GVWebService.SaveProject (GardenScript.Project, this);
	}

	public void BackAfterSave(){
		GV_UI.Animate("designOut");
		Invoke("Hide", 2);
		if (_bg) _bg.SetActive (true);
		GVWebService.LoadProjects (false);
		_gardenScript.CameraControl.Reset ();
		_gardenScript.Clear();
		_gardenScript.Hide();
		_busy.Hide ();
		_closing = false;
	}

	void Hide(){
		gameObject.SetActive (false);
	}

	public void SnapshotPressed(){
		Debug.Log ("DesignController.SnapshotPressed");
		_snapshotsPanel.SetActive(!_snapshotsPanel.activeSelf);
	}

	public void ActionPressed(){
		Debug.Log ("DesignController.ActionPressed");
		//GV_UI.DrawShape.Clear();
		GV_UI.ShowModalPanel("Reality");
	}

	public void HelpPressed(){
		Debug.Log ("DesignController.HelpPressed");
		//GV_UI.DrawShape.Clear();
	#if UNITY_EDITOR
		Application.OpenURL (string.Format ("{0}gv/help/index.html", GVWebService.ResourcePath));
	#else
		#if UNITY_WEBPLAYER
			Application.ExternalCall("showHelp", true);
		#else
			Application.OpenURL (string.Format ("{0}gv/help/index.html", GVWebService.ResourcePath));
		#endif
	#endif	
		Analytics.ga ("Help", "Open help");
	}

	public void SettingsPressed(){
		Debug.Log ("DesignController.SettingsPressed");
		//GV_UI.DrawShape.Clear();
		GV_UI.ShowModalPanel("Settings");
	}

	public void TakeScreenshotPressed(){
		Debug.Log ("DesignController.TakeScreenshotPressed");
		_snapshotsPanel.SetActive (false);
		_gardenScript.TakeSnapshot ();
	}

	public void ViewScreenshotsPressed(){
		Debug.Log ("DesignController.ViewScreenshotsPressed");
		_snapshotsPanel.SetActive (false);
		GV_UI.ShowModalPanel("Screenshots");
	}

	public void FooterDrawPressed(){
		Debug.Log ("DesignController.FooterChangePressed");
		SetFooterState ("draw");
	}

	public void FooterPlotPressed(string type){
		Debug.Log ("DesignController.FooterPlotPressed");
		if (type == "shape") {
			_gardenScript.DrawPlot ();
		} else {
			_gardenScript.CreatePlot (type);
		}
		Analytics.ga ("Project", "Draw", type);
	}

	public void FooterPavingPressed(){
		Debug.Log ("DesignController.FooterPavingPressed");
		SetSelected(FooterButtons.PAVING);
		_gardenScript.MouseMode = (int)MouseModes._NONE;
		GV_UI.ShowModalPanel("Products", "paving");
	}

	public void FooterChangePressed(){
		Debug.Log ("DesignController.FooterChangePressed");
		if (_changeButton!=0) SetSelected((FooterButtons)_changeButton);
		_gardenScript.MouseMode = (int)MouseModes._NONE;
		GV_UI.ShowChangePanel(GardenScript.SelectedProduct);
	}

	public void FooterFeaturesPressed(){
		Debug.Log ("DesignController.FooterFeaturesPressed");
		SetSelected(FooterButtons.FEATURES);
		_gardenScript.MouseMode = (int)MouseModes._NONE;
		GV_UI.ShowModalPanel("Products", "features");
	}

	public void FooterPathsPressed(){
		Debug.Log ("DesignController.FooterPathsPressed");
		SetSelected(FooterButtons.PATHS);
		_gardenScript.MouseMode = (int)MouseModes._NONE;
		GV_UI.ShowModalPanel("Products", "paths");
	}

	public void FooterWallingPressed(){
		Debug.Log ("DesignController.FooterWallingPressed");
		SetSelected(FooterButtons.WALLING);
		_gardenScript.MouseMode = (int)MouseModes._NONE;
		GV_UI.ShowModalPanel("Products", "walling");
	}

	public void FooterFencesPressed(){
		Debug.Log ("DesignController.FooterFencesPressed");
		SetSelected(FooterButtons.FENCES);
		_gardenScript.MouseMode = (int)MouseModes._NONE;
		GV_UI.ShowModalPanel("Products", "fences");
	}

	public void FooterAggregatesPressed(){
		Debug.Log ("DesignController.FooterAggregatesPressed");
		SetSelected(FooterButtons.AGGREGATES);
		_gardenScript.MouseMode = (int)MouseModes._NONE;
		GV_UI.ShowModalPanel("Products", "aggregates");
	}

	public void FooterAccessoriesPressed(){
		Debug.Log ("DesignController.FooterAccessoriesPressed");
		SetSelected(FooterButtons.ACCESSORIES);
		_gardenScript.MouseMode = (int)MouseModes._NONE;
		GV_UI.ShowModalPanel("Products", "accessories");
	}

	public void FooterBuildingsPressed(){
		Debug.Log ("DesignController.FooterBuildingsPressed");
		SetSelected(FooterButtons.BUILDINGS);
		_gardenScript.MouseMode = (int)MouseModes._NONE;
		GV_UI.ShowModalPanel("Products", "buildings");
	}

	public void FooterTreesPressed(){
		Debug.Log ("DesignController.FooterTressPressed");
		SetSelected(FooterButtons.TREES);
		_gardenScript.MouseMode = (int)MouseModes._NONE;
		GV_UI.ShowModalPanel("Products", "trees");
	}

	public void FooterAddHousePressed(){
		Debug.Log ("DesignController.FooterAddHousePressed");
		GV_UI.ShowModalPanel ("AddHouse");
	}

	public void FooterPositionPressed(){
		Debug.Log ("DesignController.FooterPositionPressed");
		SetSelected(FooterButtons.POSITION);
		_gardenScript.MouseMode = (int)MouseModes._MOVE;
	}
		
	public void FooterRotatePressed(){
		Debug.Log ("DesignController.FooterRotatePressed");
		SetSelected(FooterButtons.ROTATE);
		_gardenScript.MouseMode = (int)MouseModes._ROTATE;
	}

	public void FooterPositionPatternPressed(){
		Debug.Log ("DesignController.FooterPositionPatternPressed");
		SetSelected(FooterButtons.POSITION_PATTERN);
		_gardenScript.MouseMode = (int)MouseModes._MOVE_PATTERN;
	}

	public void FooterRotationPatternPressed(){
		Debug.Log ("DesignController.FooterRotationPatternPressed");
		SetSelected(FooterButtons.ROTATE_PATTERN);
		_gardenScript.MouseMode = (int)MouseModes._ROTATE_PATTERN;
	}

	public void FooterScaleHousePressed(){
		Debug.Log ("DesignController.FooterScaleHousePressed");
		SetSelected(FooterButtons.SCALE_HOUSE);
		_gardenScript.MouseMode = (int)MouseModes._SCALE_HOUSE;
	}

	public void FooterFlipPressed(string type){
		Debug.Log ("DesignController.FooterFlipPressed " + type);
		switch (type) {
		case "boundary":
			SetSelected (FooterButtons.FLIP_BOUNDARY);
			break;
		case "edging":
			SetSelected (FooterButtons.FLIP_EDGING);
			break;
		case "kerb":
			SetSelected (FooterButtons.FLIP_KERB);
			break;
		case "walling":
			SetSelected (FooterButtons.FLIP_WALLING);
			break;
		}
		_gardenScript.FlipSelected ();
		_gardenScript.MouseMode = (int)MouseModes._NONE;
	}

	public void FooterAddDollyPressed(){
		Debug.Log ("DesignController.FooterAddDollyPressed");
		SetSelected(FooterButtons.ADD_DOLLY);
		_gardenScript.MouseMode = (int)MouseModes._ADD_DOLLY;
	}

	public void FooterAddPillarPressed(){
		Debug.Log ("DesignController.FooterAddPillarPressed");
		SetSelected(FooterButtons.ADD_PILLAR);
		_gardenScript.MouseMode = (int)MouseModes._ADD_PILLAR;
	}

	public void FooterRestartPressed(){
		Debug.Log ("DesignController.FooterRestartPressed");
		SetSelected(FooterButtons.RESTART);
		if (GV_UI.HouseDrawShape)
			GV_UI.HouseDrawShape.Restart ();
	}

	public void FooterUndoPressed(){
		Debug.Log ("DesignController.FooterUndoPressed");
		SetSelected(FooterButtons.UNDO);
		if (GV_UI.HouseDrawShape)
			GV_UI.HouseDrawShape.Undo ();
	}

	public void FinishPressed(){
		GV_UI.ButtonCapturedMouse = false;
		Debug.Log ("DesignController.FinishPressed");
		Sprite sprite = _addHouseController.HouseSprite;
		float ar = (float)sprite.texture.width / (float)sprite.texture.height;
		GV_UI.GardenScript.AddHouse(_addHouseController.HousePath, _addHouseController.HouseGuid, GV_UI.HouseDrawShape.SourceVertices2d, ar);
		_housePanel.SetActive(false);
		GV_UI.HouseDrawShape.Active = false;
	}

	public void CameraViewPressed(int id){
		_gardenScript.CameraControl.SetCamera (id, true);
		ShowCameraControls (false);
	}

	public void ShowCameraControls(bool show){
		_snapshotsPanel.SetActive (false);
		if (CameraPanel) _cameraPanel.SetActive (show);
	}

	public void SetFooterButtons(string buttonStr){
		Debug.Log ("DesignController.SetFooterButtons " + buttonStr);
		string[] buttons = buttonStr.Split (',');
		int [] btns = new int[buttons.Length+1];
		Dictionary<string, int> options = new Dictionary<string, int> ()
			{
				{"move",(int)FooterButtons.POSITION}, {"rotate",(int)FooterButtons.ROTATE}, 
				{"move_pattern",(int)FooterButtons.POSITION_PATTERN},{"rotate_pattern",(int)FooterButtons.ROTATE_PATTERN},
				{"scale_house",(int)FooterButtons.SCALE_HOUSE},{"edging_flip",(int)FooterButtons.FLIP_EDGING},
				{"edging_add_post",(int)FooterButtons.ADD_DOLLY},{"walling_add_post",(int)FooterButtons.ADD_PILLAR},
				{"walling_flip",(int)FooterButtons.FLIP_WALLING},{"boundary_flip",(int)FooterButtons.FLIP_BOUNDARY},
				{"kerb_flip",(int)FooterButtons.FLIP_KERB}
			};
		int i = 0;
		foreach (string option in buttons) {
			if (options.ContainsKey (option)) {
				btns [i++] = options [option];
			} else {
				Debug.Log(string.Format("DesignController.SetFooterButtons '{0}' not found in dictionary", option));
			}
		}

		Product product = GardenScript.SelectedProduct;
		if (product == null)
			return;
		switch (product.Type) {
		case "paving":
		case "driveway":
			btns [btns.Length - 1] = (int)FooterButtons.CHANGE_PAVING;
			_changeButton = FooterButtons.CHANGE_PAVING;
			break;
		case "circle":
		case "octant":
			btns [btns.Length - 1] = (int)FooterButtons.CHANGE_FEATURE;
			_changeButton = FooterButtons.CHANGE_FEATURE;
			break;
		case "path":
		case "kerb":
		case "edging":
			btns [btns.Length - 1] = (int)FooterButtons.CHANGE_KERB;
			_changeButton = FooterButtons.CHANGE_KERB;
			break;
		case "walling":
		case "coping":
		case "palisades":
		case "post":
			btns [btns.Length - 1] = (int)FooterButtons.CHANGE_WALLING;
			_changeButton = FooterButtons.CHANGE_WALLING;
			break;
		case "boundary":
			btns [btns.Length - 1] = (int)FooterButtons.CHANGE_FENCE;
			_changeButton = FooterButtons.CHANGE_FENCE;
			break;
		case "aggregate":
		case "surface":
			btns [btns.Length - 1] = (int)FooterButtons.CHANGE_AGGREGATES;
			_changeButton = FooterButtons.CHANGE_AGGREGATES;
			break;
		case "rock":
		case "accessory":
			btns [btns.Length - 1] = (int)FooterButtons.CHANGE_ACCESSORY;
			_changeButton = FooterButtons.CHANGE_ACCESSORY;
			break;
		case "summerhouse":
		case "greenhouse":
			btns [btns.Length - 1] = (int)FooterButtons.CHANGE_BUILDING;
			_changeButton = FooterButtons.CHANGE_BUILDING;
			break;
		case "vegetation":
			btns [btns.Length - 1] = (int)FooterButtons.CHANGE_TREE;
			_changeButton = FooterButtons.CHANGE_TREE;
			break;
		default:
			btns [btns.Length - 1] = -1;
			break;
		}
		SetFooterState ("selected", btns);
	}

	public void SetFooterButtons(bool paving){
		//GV_UI.ShowAlert ("TO DO", "DesignController.SetFooterButtons " + paving);
		int count = (paving) ? 4 : 2;
		int[] btns = new int[count]; 
		btns [0] = (int)FooterButtons.POSITION;
		btns [1] = (int)FooterButtons.ROTATE;
		if (paving) {
			btns [2] = (int)FooterButtons.POSITION_PATTERN;
			btns [3] = (int)FooterButtons.ROTATE_PATTERN;
		}
		SetFooterState ("selected", btns);
		_changeButton = FooterButtons.CHANGE_PAVING;
	}

	public void ShowSelectedBtn(FooterButtons btn){
		SetSelected (btn);
	}

	void SetSelected(FooterButtons btn){
		if (!_selected) return;
		Transform t = transform.Find ("Footer/Buttons");
		Transform btnT = t.GetChild ((int)btn); 
		_selected.SetActive(true);
		_selected.transform.SetParent (btnT, false);
	}

	public void SetFooterState(string state, int [] selectBtns=null){
		if (state == _editState) return;
		Debug.Log ("DesignController.SetFooterState " + state);

		SetDeleteBtn(false);
		if (_selected) _selected.SetActive (false);

		string leftButton = "";
		string rightButton = "";
		List<GameObject> btns = new List<GameObject>(); 
		int help = 0;

		//Set defaults
		if (_housePanel)
			_housePanel.SetActive (false);

		if (_header)
			_header.SetActive (true);

		if (_moveWidget)
			_moveWidget.SetActive (true);

		if (_heightWidget)
			_heightWidget.SetActive (true);
		
		Transform t = transform.Find ("Footer/Buttons");
		int cellWidth;

		switch(state){
		case "draw":
			leftButton = "ChangeSize";
			rightButton = "Design";
			//help = 1;
			btns.Add (t.GetChild ((int)FooterButtons.DRAW).gameObject);
			btns.Add (t.GetChild ((int)FooterButtons.SQUARE).gameObject);
			btns.Add (t.GetChild ((int)FooterButtons.RECTANGLE).gameObject);
			btns.Add (t.GetChild ((int)FooterButtons.LSHAPE).gameObject);
			btns.Add (t.GetChild ((int)FooterButtons.CIRCLE).gameObject);
			btns.Add (t.GetChild ((int)FooterButtons.SEMICIRCLE).gameObject);
			cellWidth = 210;
			break;
		case "edit":
			leftButton = "Change";
			rightButton = "AddHouse";
			btns.Add (t.GetChild ((int)FooterButtons.PAVING).gameObject);
			btns.Add (t.GetChild ((int)FooterButtons.FEATURES).gameObject);
			btns.Add (t.GetChild ((int)FooterButtons.PATHS).gameObject);
			btns.Add (t.GetChild ((int)FooterButtons.WALLING).gameObject);
			btns.Add (t.GetChild ((int)FooterButtons.FENCES).gameObject);
			btns.Add (t.GetChild ((int)FooterButtons.AGGREGATES).gameObject);
			btns.Add (t.GetChild ((int)FooterButtons.ACCESSORIES).gameObject);
			btns.Add (t.GetChild ((int)FooterButtons.BUILDINGS).gameObject);
			btns.Add (t.GetChild ((int)FooterButtons.TREES).gameObject);
			if (_threeD) _threeD.SetActive (true);
			cellWidth = 160;
			break;
		case "crop_house":
			leftButton = "Cancel";
			rightButton = "Finish";
			btns.Add (t.GetChild ((int)FooterButtons.RESTART).gameObject);
			btns.Add (t.GetChild ((int)FooterButtons.UNDO).gameObject);
			GameObject house = _housePanel.transform.Find ("Image").gameObject;
			Image image = house.GetComponent<Image> ();
			image.sprite = _addHouseController.HouseSprite;
			if (GV_UI.HouseDrawShape) {
				GV_UI.HouseDrawShape.Active = true;
				GV_UI.HouseDrawShape.Clear ();
			}
			_housePanel.SetActive (true);
			_header.SetActive (false);
			_moveWidget.SetActive (false);
			_heightWidget.SetActive (false);
			GV_UI.ButtonCapturedMouse = true;
			cellWidth = 180;
			break;
		case "selected":
			leftButton = "Delete";
			help = 2;
			foreach (int id in selectBtns) {
				if (id >= 0)
					btns.Add (t.GetChild (id).gameObject);
			}
			cellWidth = 180;
			help = 2;
			break;
		default:
			Debug.Log (string.Format ("DesignController.SetFooterState {0} not supported", state));
			return;
		}

		if (state != "selected") _changeButton = 0;
		_editState = state;

		GameObject go = transform.Find ("Footer/DeleteButton").gameObject;
		go.SetActive (leftButton=="Delete");
		go = transform.Find ("Footer/ChangeButton").gameObject;
		go.SetActive (leftButton=="Change");
		go = transform.Find ("Footer/CancelButton").gameObject;
		go.SetActive (leftButton=="Cancel");
		go = transform.Find ("Footer/ChangeSize").gameObject;
		go.SetActive (leftButton=="ChangeSize");
		go = transform.Find ("Footer/DesignButton").gameObject;
		go.SetActive (rightButton=="Design");
		go = transform.Find ("Footer/AddHouseButton").gameObject;
		go.SetActive (rightButton=="AddHouse");
		go = transform.Find ("Footer/FinishButton").gameObject;
		go.SetActive (rightButton=="Finish");

		if (Help) {
			Image helpImage = _help.GetComponent<Image> ();
			switch (help) {
			case 0:
				_help.SetActive (false);
				break;
			case 1://info_paving
				helpImage.sprite = Resources.Load<Sprite> ("Textures/info_paving");
				_help.SetActive (true);
				break;
			case 2://info_selected
				helpImage.sprite = Resources.Load<Sprite> ("Textures/info_selected");
				_help.SetActive (true);
				break;
			default:
				_help.SetActive (false);
				break;
			}
			if (_help.activeSelf) {
				Vector3 hpos = _help.transform.position;
				hpos.x = 2048;
				_help.transform.position = hpos;
				iTween.MoveTo(_help, iTween.Hash("x", 1036, "time", 0.5f, "easeType", "easeOutQuad", "loopType", "none", "delay", 0.3f, "isLocal", true));
			}
		}

		for(int i=0; i<t.childCount; i++){
			go = t.GetChild (i).gameObject;
			if (go!=_help) go.SetActive(false);
		}

		int left = 0;
		float delay = 0.0f;

		for(int i=0; i<btns.Count; i++){
			Vector3 pos = btns[i].transform.position;
			pos.x = 2048;
			btns[i].transform.position = pos;
			btns[i].SetActive (true);
			iTween.MoveTo(btns[i], iTween.Hash("x", left, "time", 0.5f, "easeType", "easeOutQuad", "loopType", "none", "delay", delay, "isLocal", true));
			delay += 0.05f;
			left += cellWidth;
		}
	}
}
