﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Xml;

public class SliderController : MonoBehaviour {
	Text _sizeTxt;
	Slider _slider;
	int _increment;
	int _min;
	int _max;
	XmlNode _xml;
	public XmlNode Xml {
		get{
			return _xml;
		}
		set {
			_xml = value;
		}
	}

	// Use this for initialization
	void Start () {
		GameObject go = transform.Find ("Slider").gameObject;
		_slider = go.GetComponent<Slider> ();
		go = transform.Find ("SizeTxt").gameObject;
		_sizeTxt = go.GetComponent<Text> ();
	}
	
	public void UpdatePanel (XmlNode node) {
		_xml = node.PreviousSibling;
		XmlNode slider = FindNodeByName ("slider", node);
		if (slider != null) {
			_increment = Int32.Parse(GetXmlAttribute (slider, "coursesize"));
			_min = Int32.Parse(GetXmlAttribute (slider, "mincourses"));
			_max = Int32.Parse(GetXmlAttribute (slider, "maxcourses"));
			if (_slider == null) {
				GameObject go = transform.Find ("Slider").gameObject;
				_slider = go.GetComponent<Slider> ();
			}
			_slider.minValue = _min;
			_slider.maxValue = _max;
			int num = _min;
			int a;
			do {
				a = num * _increment;
				num++;
			} while(a < 500 && num <= _max);
			while ((num * _increment) > 500) {
				num--;
				if (num <= _min)
					break;
			}
			_slider.value = num;

			SliderChanged ();
		}
	}

	public void SliderChanged(){
		if (_sizeTxt == null) {
			GameObject go = transform.Find ("SizeTxt").gameObject;
			_sizeTxt = go.GetComponent<Text> ();
		}
		_sizeTxt.text = string.Format("{0}mm", _slider.value * _increment);
	}

	XmlNode FindNodeByName(string name, XmlNode node){
		XmlNode result = null;
		for (int i=0; i<node.ChildNodes.Count; i++) {
			XmlNode child = node.ChildNodes [i];
			if (child.Name == name) {
				result = child;
				break;
			}
		}
		return result;
	}

	string GetXmlAttribute(XmlNode node, string str){
		XmlAttribute attr = node.Attributes[str];
		if (attr==null) return "";
		return attr.Value;
	}

	public int Value {
		get {
			if (_slider == null)
				return 0;
			return (int)(_slider.value * _increment);
		}
		set{
			if (_slider == null) {
				GameObject go = transform.Find ("Slider").gameObject;
				_slider = go.GetComponent<Slider> ();
			}
			_slider.value = value / _increment;
			SliderChanged ();
		}
	}
}
