﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class ProjectsController : MonoBehaviour {
	public GameObject _thumbnail;

	GV_UI _gv_ui;
	GameObject _mainButtons;
	GameObject _editButtons;
	GameObject _confirmButton;
	GameObject _scroller;
	Text _title;
	Text _confirmTxt;
	Image _editBg;
	Image _trashImg;
	Image _duplicateImg;
	Button _trashBtn;
	Button _duplicateBtn;
	int _loadIndex;
	bool _loading = true;
	public bool Loading {
		get {
			return _loading;
		}
		set{
			_loading = value;
		}
	}
	JSONArray _projects;
	bool _show;
	List<ThumbnailController>_thumbnails;

	// Use this for initialization
	void Start () {
		GameObject go = GameObject.Find ("Canvas");
		_gv_ui = go.GetComponent<GV_UI>();
		ClearThumbnails();
		_mainButtons = GameObject.Find ("Canvas/ProjectsPanel/Header/MainButtons");
		_editButtons = GameObject.Find ("Canvas/ProjectsPanel/Header/EditButtons");
		_confirmButton = GameObject.Find ("Canvas/ProjectsPanel/Header/EditButtons/ConfirmButton");
		go = GameObject.Find ("Canvas/ProjectsPanel/Header/EditButtons/ConfirmButton/Text");
		_confirmTxt = go.GetComponent<Text>();
		go = GameObject.Find ("Canvas/ProjectsPanel/Header/EditButtons/DeleteButton");
		_trashImg = go.GetComponent<Image>();
		_trashBtn = go.GetComponent<Button>();
		go = GameObject.Find ("Canvas/ProjectsPanel/Header/EditButtons/DuplicateButton");
		_duplicateImg = go.GetComponent<Image>();
		_duplicateBtn = go.GetComponent<Button>();
		_editButtons.SetActive(false);
		_confirmButton.SetActive (false);
		go = GameObject.Find ("Canvas/ProjectsPanel/EditBg");
		_editBg = go.GetComponent<Image>();
		go = GameObject.Find ("Canvas/ProjectsPanel/Header/Text");
		_title = go.GetComponent<Text>();
		_scroller = GameObject.Find ("Canvas/ProjectsPanel/Projects/Scroller");
	}

	public void Show(){
		gameObject.SetActive (true);
	}

	public void Hide(){
		gameObject.SetActive (false);
	}

	void ClearThumbnails(){
		Transform t = transform.Find ("Projects/Scroller");
		if (t){
			for(int i=t.childCount-1; i>=0; i--) Destroy(t.GetChild (i).gameObject);
		}
	}
		
	public void UpdatePanel(JSONNode data, bool show=true){
		ClearThumbnails ();
		Debug.Log ("ProjectsController.UpdatePanel");
		_projects = data.AsArray;
		_show = show;
		_thumbnails = new List<ThumbnailController> ();
		//Debug.Log (projects);
		for (int i = 0; i < _projects.Count; i++) {
			JSONNode project = _projects [i];
			if (project != null && project.Value != "null" && project.Value != "0") {
				try {
					GameObject go = (GameObject)Instantiate (_thumbnail);
					ThumbnailController controller = go.GetComponent<ThumbnailController> ();
					Transform t = transform.Find ("Projects/Scroller");
					controller.Set (project, this);
					go.transform.SetParent (t, false);
					_thumbnails.Add(controller);
				} catch (ArgumentNullException e) {
					Debug.Log ("ProjectsController.LoadThumbnail " + e);
				}
			}
		}

		int rows = (int)Math.Ceiling ((double)(_thumbnails.Count / 4.0));
		ResizePanelAfterLoad (rows, 444);
		if (_show)
			GV_UI.Animate ("projectsIn");

		_loading = true;
		_loadIndex = 0;

		LoadNextThumbnail ();
	}

	public void LoadNextThumbnail(){
		if (!_loading)
			return;
		if (_thumbnails == null)
			return;

		int count = _thumbnails.Count;
		Debug.Log (string.Format ("ProjectsController.LoadNextThumbnail {0} of {1}", _loadIndex, count));

		if (_loadIndex >= count) {
			//Finished loading

		} else {
			if (!gameObject.activeSelf)
				gameObject.SetActive (true);
			StartCoroutine ("LoadThumbnail");
		}
	}

	IEnumerator LoadThumbnail(){
		ThumbnailController thumbnail = _thumbnails[_loadIndex];
		if (thumbnail!=null){
			thumbnail.Load ();
		}
		_loadIndex++;
		return null;
	}

	void ResizePanelAfterLoad(int count, int cellSize){
		RectTransform rt = _scroller.GetComponent<RectTransform>();
		Vector2 size = rt.sizeDelta;
		size.y = Math.Max(1410, count * cellSize + 85);
		rt.sizeDelta = size;
		Vector2 pos = rt.anchoredPosition;
		pos.y = 0;
		rt.anchoredPosition = pos;
	}

	public void NewProjectPressed(){
		Debug.Log ("ProjectsController.NewProjectPressed");
		GV_UI.ShowModalPanel("NewProject");
		GV_UI.NewProjectController.Clear ();
	}

	public void ActionPressed(){
		Debug.Log ("ProjectsController.ActionPressed");
		SetEditState(true);
	}

	void SetEditState(bool mode){
		if (_editBg) _editBg.enabled = mode;
		Transform t = transform.Find ("Projects/Scroller");
		for(int i=0; i<t.childCount; i++){
			ThumbnailController controller = t.GetChild (i).gameObject.GetComponent<ThumbnailController>();
			controller.SetEditState(mode);
		}
		_mainButtons.SetActive (!mode);
		_editButtons.SetActive (mode);
		if (mode){
			_title.text = "SELECT A PROJECT";
			_trashImg.color = new Color(1,1,1,0.5f);
			_duplicateImg.color = new Color(1,1,1,0.5f);
			UpdateEditHeader();
		}else{
			_title.text = "GARDEN PROJECTS";
		}
	}

	public void UpdateEditHeader(){
		Color col = (SelectedCount==0) ? new Color(1,1,1,0.5f) : new Color(1,1,1,1);
		_trashImg.color = col;
		_duplicateImg.color = col;
		_trashBtn.interactable = (SelectedCount>0);
		_duplicateBtn.interactable = (SelectedCount>0);
	}

	public void InfoPressed(){
		Debug.Log ("ProjectsController.InfoPressed");
		//GV_UI.ShowModalPanel ("Help");
	#if UNITY_EDITOR
		Application.OpenURL (string.Format ("{0}dv/help/index.html", GVWebService.ResourcePath));
	#else
		#if UNITY_WEBPLAYER
			Application.ExternalCall("showHelp", true);
		#else
		Application.OpenURL (string.Format ("{0}dv/help/index.html", GVWebService.ResourcePath));
		#endif
	#endif
	}

	public void SettingsPressed(){
		Debug.Log ("ProjectsController.SettingsPressed");
		GV_UI.ShowModalPanel ("Settings");
	}

	public int SelectedCount{
		get{
			int count = 0;
			Transform t = transform.Find ("Projects/Scroller");
			for(int i=0; i<t.childCount; i++){
				ThumbnailController controller = t.GetChild (i).gameObject.GetComponent<ThumbnailController>();
				if (controller.Selected) count++;
			}
			return count;
		}
	}
	
	public void DeletePressed(){
		Debug.Log ("ProjectsController.DeletePressed");
		if (_confirmButton.activeSelf){
			_confirmButton.SetActive (false);
		}else{
			_confirmTxt.text = string.Format ("Delete {0} projects", SelectedCount);
			_confirmButton.SetActive(true);
		}
	}

	public void DuplicatePressed(){
		Debug.Log ("ProjectsController.DuplicatePressed");
		Transform t = transform.Find ("Projects/Scroller");
		List<string> guids = new List<string>();
		
		for(int i=0; i<t.childCount; i++){
			ThumbnailController controller = t.GetChild (i).gameObject.GetComponent<ThumbnailController>();
			if (controller.Selected) guids.Add (controller.Guid );
		}

		GVWebService.CloneGuids = guids;//This will start the cloning process one project at a time.

		SetEditState(false);
	}

	public void DonePressed(){
		Debug.Log ("ProjectsController.DonePressed");
		SetEditState(false);
	}

	public void ConfirmPressed(){
		Debug.Log ("ProjectsController.ConfirmPressed");
		Transform t = transform.Find ("Projects/Scroller");
		List<GameObject> remove = new List<GameObject>();
		List<string> deletedGuids = new List<string>();

		for(int i=0; i<t.childCount; i++){
			ThumbnailController controller = t.GetChild (i).gameObject.GetComponent<ThumbnailController>();
			if (controller.Selected){
				remove.Add(controller.gameObject);
				deletedGuids.Add (controller.Guid );
			}
		}

		GVWebService.DeleteProjects(deletedGuids);

		while(remove.Count>0){
			GameObject go = remove[0];
			remove.Remove(go);
			Destroy(go);
		}

		_confirmButton.SetActive (false);
		SetEditState(false);
	}

	public void HelpClosePressed(){
		Debug.Log ("ProjectsController.HelpClosePressed");
		GV_UI.CloseModalPanel ();
	}

	public string GetBase64FromGuid(string guid){
		string result = "";

		Transform t = transform.Find ("Projects/Scroller");
		for(int i=0; i<t.childCount; i++){
			ThumbnailController controller = t.GetChild (i).gameObject.GetComponent<ThumbnailController>();
			if (controller.Guid == guid){
				result = controller.Base64;
				break;
			}
		}
		
		return result;
	}
}
