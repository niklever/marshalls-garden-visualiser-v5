﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;
using Ucss;
using System.IO;
using ImageVideoContactPicker;

using Color=UnityEngine.Color;

public class AddHouseController:MonoBehaviour
{
	string _housePath;
	string _houseGuid;

	GameObject _house;
	GameObject _cropButton;
	GameObject _button1;
	GameObject _button2;
	GameObject _button3;
	GameObject _infoTxt;
	Text _title;
	public Sprite HouseSprite {
		get {
			if (_house == null)
				return null;
			Image house = _house.GetComponent<Image> ();
			if (house == null)
				return null;
			return house.sprite;
		}
	}
	public string HousePath {
		get {
			return _housePath;
		}
	}
	public string HouseGuid {
		get {
			return _houseGuid;
		}
	}

	bool _houseShown;

	public void Start(){
		_house = transform.Find ("House").gameObject;
		_cropButton = transform.Find ("CropButton").gameObject;
		_button1 = transform.Find ("CameraButton").gameObject;
		_button2 = transform.Find ("PhotosButton").gameObject;
		_button3 = transform.Find ("DefaultButton").gameObject;
		_infoTxt = transform.Find ("InfoText").gameObject;
		ShowHouse (false);
		Button button = _button1.GetComponent<Button> ();
		#if UNITY_WEBPLAYER
			button.interactable = false;
		#endif
		DesignController.AddHouseController = this;
	}

	public void Show(){
		ShowHouse (false);
		gameObject.SetActive (true);
	}

	public void closePressed(){
		GV_UI.ButtonCapturedMouse = false;

		if (_houseShown){
			//_stop_panel.Visible = true;
		}else{

		}

		#if UNITY_EDITOR
		#else
		#if UNITY_WEBPLAYER
			Application.ExternalEval("selectHouseImage(false)");
		#endif
		#endif

		if (GV_UI.GardenScript.Browser != null) {
			GV_UI.GardenScript.Browser = null;
		}
		GV_UI.CloseModalPanel ();
	}

	public void button1Pressed(){
		Debug.Log ("AddHousePanel btn1 pressed");
		GV_UI.ShowModalPanel("WebCam");
	}

	public void button2Pressed(){
		Debug.Log ("AddHousePanel btn2 pressed");
	#if UNITY_EDITOR
		/*GV_UI.GardenScript.Browser = new FileBrowser(
			new Rect(212, 100, 600, 500),
			"Choose Image File",
			FileSelectedCallback
		);
		GV_UI.GardenScript.Browser.SelectionPattern = "*.jpg";
		GV_UI.GardenScript.Browser.DirectoryImage = GV_UI.GardenScript.folderIcon;
		GV_UI.GardenScript.Browser.FileImage = GV_UI.GardenScript.fileIcon;
		string curDir = PlayerPrefs.GetString("BrowserDir","");
		if (curDir!="") GV_UI.GardenScript.Browser.CurrentDirectory = curDir;*/
	#elif UNITY_WEBPLAYER
		Application.ExternalEval("selectHouseImage(true, \"" + GVWebService.Token + "\")");
		_cropButton.GetComponent<Button>().interactable = false;
	#elif UNITY_ANDROID
		AndroidPicker.BrowseImage(true);
	#elif UNITY_IPHONE
		IOSPicker.BrowseImage(false); // pick and crop
		PickerEventListener.onImageSelect -= UploadImage;//Remove if already added
		PickerEventListener.onImageSelect += UploadImage;
	#endif
		ShowHouse (true);
	}

	public void button3Pressed(){
		if (GV_UI.GardenScript!=null) GV_UI.GardenScript.AddHouse("default", null, null, 1.33f);
		GV_UI.CloseModalPanel ();
		Debug.Log ("AddHousePanel btn3 pressed");
	}
		
	public void cropPressed(){
		Debug.Log ("AddHousePanel crop pressed ");
		GV_UI.DesignController.SetFooterState ("crop_house");
		GV_UI.CloseModalPanel ();
	}

	public void stopConfirmPressed(){	
		Debug.Log ("AddHousePanel stop confirm pressed");
		gameObject.SetActive (false);
		//Hide the overlay
	#if UNITY_EDITOR
		#else
		#if UNITY_WEBPLAYER
		Application.ExternalEval("selectHouseImage(false)");
		#endif
	#endif
	}

	public void cancelPressed(){	
		Debug.Log ("AddHousePanel cancel pressed");
		//_stop_panel.SetActive(false);
	}
		
	public void ShowHouse(bool show){
		_house.SetActive(show);
		_cropButton.SetActive (show);
		_infoTxt.SetActive (show);

		_button1.SetActive (!show);
		_button2.SetActive (!show);
		_button3.SetActive (!show);

		_houseShown = show;
	}

	public void UploadImage(string str){
		Debug.Log(string.Format("AddHouseController.UploadImage {0}", str));
		#if UNITY_WEBPLAYER
		#else
		if (File.Exists(str)){
			byte [] fileData = File.ReadAllBytes((String)str);
			Texture2D tex = new Texture2D(2, 2);
			tex.LoadImage (fileData);
			byte[] jpg = tex.EncodeToJPG (); 
			GVWebService.UploadImage (jpg, this);
		}
		#endif
	}

	public void UploadImage(WebCamTexture texture){
		Texture2D tex = new Texture2D (texture.width, texture.height);
		tex.SetPixels (texture.GetPixels ());
		tex.Apply ();
		byte[] jpg = tex.EncodeToJPG ();
		GVWebService.UploadImage (jpg, this);
		Debug.Log("AddHouseController.UploadImage from webcam");
	}

	public void FileSelectedCallback(string str) {
		Debug.Log (string.Format("AddHouseController.FileSelectedCallback {0}", str));

		GV_UI.GardenScript.Browser = null;
		if (str == null)
			return;

		JSONNode json = null;

		try{
			json = JSON.Parse(str);
		}catch(NullReferenceException e){
			Debug.Log ("AddHouseController.FileSelectedCallback JSON.Parse triggered exception str:" + str);
		}


		if (json == null) {
			Debug.Log ("AddHouseController.FileSelectedCallback json null");
		#if UNITY_WEBPLAYER
			//This does not apply to web player
		#else
			int pos = str.LastIndexOf("/");
			string dir = (pos == -1) ? str : str.Substring (0, pos);
			PlayerPrefs.SetString ("BrowserDir", dir);
			Texture2D tex = null;
			byte[] fileData;

			if (File.Exists(str))     {
				fileData = File.ReadAllBytes((String)str);
				tex = new Texture2D(2, 2);
				tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
			}

			_housePath = str;
			_houseGuid = "Guid";

			LoadCallback (tex, "From local file system");
			Debug.Log ("AddHousePanel.FileSelectedCallback local file:" + str);
		#endif
		} else {
			string path = json ["Url"];
			string url = (path.Contains ("http")) ? path : "file://" + path;
			int pos = url.LastIndexOf ("/");
			if (pos != -1)
				url = string.Format ("services/project-photos/{0}", url.Substring (pos + 1));
			_housePath = url;
			_houseGuid = json ["Guid"];

			UCSS.HTTP.GetTexture (url, this.LoadCallback, this.LoadError);
			Debug.Log ("AddHousePanel.FileSelectedCallback remote file:" + url);
		}
	}

	void LoadError(string error, string id){
		Debug.Log("House.Load download error:" + error);
		GV_UI.ShowError ( _housePath + " not found");
	}

	void LoadCallback(Texture2D texture, string id){
		Debug.Log("House.Load texture:" + id);

		Image house = _house.GetComponent<Image> ();
		house.sprite = Sprite.Create (texture, new Rect (0, 0, texture.width, texture.height), Vector2.zero);

		Vector2 displaySize = new Vector2(texture.width, texture.height);
		_cropButton.GetComponent<Button>().interactable = true;

		Debug.Log (string.Format("AddHousePanel.LoadCallback image size:{0}x{1}", texture.width, texture.height));
		GVWebService.Busy = false;
	}
}