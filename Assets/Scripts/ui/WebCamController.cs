﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WebCamController : MonoBehaviour {
	AddHouseController _addHouseController;
	public AddHouseController AddHouseController {
		set {
			_addHouseController = value;
		}
	}
	WebCamTexture _texture;

	// Use this for initialization
	void Start () {
		WebCamDevice[] devices = WebCamTexture.devices; 
		string camName = "";

		for( var i = 0 ; i < devices.Length ; i++ ) {
			if (!devices[i].isFrontFacing || devices.Length==1) {
				camName = devices[i].name;
				break;
			} else {
				Debug.Log(string.Format("WebCamController.Start Back-facing camera {0}", devices[i].name));
			}
		}
		if (camName != "") {
			_texture = new WebCamTexture (camName, 1024, 768, 25);
			GameObject go = transform.Find ("RawImage").gameObject;
			RawImage rawimage = go.GetComponent<RawImage> ();
			rawimage.texture = _texture;
			rawimage.material.mainTexture = _texture;
			_texture.Play();
		}
	}
		
	public void Show(){
		gameObject.SetActive (true);
	}

	public void TriggerPressed(){
		_addHouseController.UploadImage (_texture);
		GV_UI.CloseModalPanel ();
	}

	public void CancelPressed(){
		GV_UI.CloseModalPanel ();
	}
}
