﻿using UnityEngine;
using UnityEngine.UI;
using Vectrosity;
using System.Collections;
using System.Collections.Generic;

public class HouseDrawShape : MonoBehaviour {
	public Texture2D _dotTexture;
	public Texture2D _lineTexture;

	GameObject _canvas;
	VectorLine _line;
	VectorLine _dots;
	bool _drawing = false;
	bool _closed = false;
	int _dragIndex = -1;
	float _clickTime;
	float [] _mouseLegal;

	public bool Active {
		get {
			return _dots.active;
		}
		set {
			Clear ();
			_dots.active = value;
			_line.active = value;
			if (_canvas == null) _canvas = GameObject.Find ("VectorCanvas");
			_canvas.SetActive( value );
			enabled = value;
		}
	}

	// Use this for initialization
	void Start () {
		_clickTime = Time.time;
		_dots = new VectorLine("Dots", new List<Vector2>(), _dotTexture, 15.0f, LineType.Points);
		_line = new VectorLine("Line", new List<Vector2>(), _lineTexture, 3.0f, LineType.Continuous);
		_line.textureScale = 3;
		RectTransform rt = GetComponent<RectTransform>();
		_mouseLegal = new float[2];
		#if UNITY_EDITOR
		_mouseLegal[0] = 100 * rt.localScale.y;
		_mouseLegal[1] = 768 * rt.localScale.y;
		#else
		#if UNITY_WEBPLAYER
		//float scale = 960.0f/1024.0f;
		_mouseLegal[0] = 100;
		_mouseLegal[1] = 768;
		#else
		_mouseLegal[0] = 100 * rt.localScale.y;
		_mouseLegal[1] = 768 * rt.localScale.y;
		#endif
		#endif
		Vector2 s = new Vector2(rt.sizeDelta.x * rt.localScale.x, rt.sizeDelta.y * rt.localScale.y);
		Debug.Log(string.Format("Mouse legal {0},{1} scale:{2} screen:{3},{4}", _mouseLegal[0], _mouseLegal[1], rt.localScale.y, s.x, s.y));
		//Invoke ("InitActive", 1.0f);
		GV_UI.HouseDrawShape = this;
		enabled = false;
	}

	void InitActive(){
		_canvas = GameObject.Find ("VectorCanvas");
		if (_canvas == null) {
			Invoke ("InitActive", 1.0f);
		} else {
			Active = false;
		}
	}

	public void Clear(){
		if (_dots == null)
			return;
		_dots.points2.Clear();
		_line.points2.Clear();
		_dots.Draw ();
		_line.Draw ();
	}

	public void Restart(){
		Clear();
		_dragIndex = -1;
		_drawing = false;
	}

	public void Undo(){
		_line.points2.RemoveAt(_line.points2.Count-1);
		_line.Draw();
		_dots.points2.RemoveAt(_dots.points2.Count-1);
		_dots.Draw();
	}

	bool MouseLegal(){
		//Debug.Log(string.Format("mousePosition:{0},{1}", Input.mousePosition.x, Input.mousePosition.y));
		return Input.mousePosition.y>_mouseLegal[0] && Input.mousePosition.y<_mouseLegal[1];
	}

	// Update is called once per frame
	void Update () {
		bool modified = false;

		if (Input.GetMouseButtonDown(0) && MouseLegal()){
			Vector2 mousePosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
			float elapsedTime = Time.time - _clickTime;
			bool doubleClick = (elapsedTime<0.5f);
			_dragIndex = -1;
			int insertIndex = -1;
			int index;

			if (!_drawing){
				if (_line.points2.Count>0){
					for(int i=0; i<_line.points2.Count; i++){
						if (Vector2.Distance (_line.points2[i], mousePosition)<15){
							_dragIndex = i;
							break;
						}
					}
					if (_dragIndex==-1){
						_line.Selected (mousePosition, 15, out index);
						insertIndex = index;
					}

					if (doubleClick && _dragIndex!=-1 && insertIndex==-1){
						if (_dragIndex<_line.points2.Count) _line.points2.RemoveAt(_dragIndex);
						_line.Draw ();
						modified = true;
						_dragIndex = -1;
					}
				}

				if (_dragIndex==-1 && insertIndex==-1 && !doubleClick){
					_line.points2.Clear();
					_drawing = true;
					_closed = false;
					_line.points2.Add (mousePosition);
					modified = true;
				}
				//_lineGO.SetActive (true);
			}

			if (doubleClick && _line.points2.Count>0 && _closed){
				if (_dragIndex==-1){
					_line.Selected (mousePosition, 15, out index);
					if (index!=-1){
						if (index<_line.points2.Count){
							_line.points2.Insert(index+1, mousePosition);
						}else{
							_line.points2.Add (mousePosition);
						}
						_line.Draw ();
						modified = true;
						insertIndex = index;
					}
				}
			}

			//Debug.Log ("TestDrawing.OnMouseDown " + Input.mousePosition);
			if (_dragIndex==-1 && insertIndex==-1){
				if (doubleClick){
					//Close shape
					_drawing = false;
					_closed = true;
					if (_line.points2.Count>0){
						_line.points2[_line.points2.Count-1] = _line.points2[0];
						_line.Draw ();
						modified = true;
					}
				}else{
					//Add new point
					_line.points2.Add (mousePosition);
					modified = true;
				}
			}
			_clickTime = Time.time;
		}

		if (!Input.GetMouseButton(0)) _dragIndex = -1;

		if (_drawing && _line.points2.Count>0){
			if (MouseLegal()){
				Vector2 point = _line.points2[_line.points2.Count-1];
				point.x = Input.mousePosition.x;
				point.y = Input.mousePosition.y;
				_line.points2[_line.points2.Count-1] = point;
				_line.Draw ();
				modified = true;
			}
		}else if (_dragIndex!=-1){
			Vector2 point = _line.points2[_dragIndex];
			point.x = Input.mousePosition.x;
			point.y = Input.mousePosition.y;
			if (_dragIndex<_line.points2.Count) _line.points2[_dragIndex] = point;
			if (_dragIndex==0) _line.points2[_line.points2.Count-1] = point;
			_line.Draw ();
			modified = true;
		}

		if (modified){
			_dots.points2.Clear ();
			for(int i=0; i<_line.points2.Count; i++){
				if (i==_line.points2.Count-1 && _closed) break;
				_dots.points2.Add (_line.points2[i]);
			}
			_dots.Draw ();
		}
	}

	List<Vector2> MapVertices(){
		//Line points are in device screen space - bottom left origin
		//Mapped points are in image space top left origin
		List<Vector2> vertices = _line.points2;
		List<Vector2> mapped = new List<Vector2> ();

		RectTransform rt = (RectTransform)GameObject.Find("VectorCanvas").transform;
		Vector2 screenSize = new Vector2 (rt.rect.width, rt.rect.height);//VectorCanvas gives the actual screen pixel size
		rt = (RectTransform)GameObject.Find("Canvas/DesignPanel/HousePanel/Image").transform;
		Image image = rt.gameObject.GetComponent<Image> ();
		Vector2 size = new Vector2 (image.sprite.texture.width, image.sprite.texture.height);
		float ar1 = rt.rect.width / rt.rect.height;
		float ar2 = size.x / size.y;
		float scale = 1.0f;
		Vector2 imageRectScaled = new Vector2 (screenSize.x, screenSize.x / ar1);
		Vector2 imageScaled;
		if (ar1 < ar2) {
			//Uses width
			scale = screenSize.x/size.x;
			imageScaled = new Vector2 (size.x, size.x / ar1);//Image rect scaled to Image resolution
		} else {
			//Uses height
			scale = imageRectScaled.y/size.y;
			imageScaled = new Vector2 (size.y * ar1, size.y);//Image rect scaled to Image resolution
		}
		Vector2 offset = new Vector2 (screenSize.x / 2, imageRectScaled.y / 2);
		Vector2 imageOrigin = new Vector2 ((imageScaled.x - size.x) / 2, (imageScaled.y - size.y) / 2);//Find the top left corner of Image in the same space as the Image rect
		for(int i=0; i<vertices.Count; i++){
			Vector2 pt = new Vector2(vertices[i].x, vertices[i].y);
			pt.y = screenSize.y - pt.y;//Invert y so origin is at the top
			//pt -= offset;//Work from the centre of Image rect in screen coordinates
			pt /= scale;//Convert the point into the same space as the image;
			pt -= imageOrigin;//Set origin as top left
			mapped.Add(pt);
		}
		//if (mapped.Count > 1 && Vector2.Distance (mapped [mapped.Count - 1], mapped [0]) > 1) {
			mapped.Add (mapped [0]);
		//}
		/*if (mapped.Count > 2) {
			//Check that this is CCW
			Vector2 v1 = mapped [0] - mapped [1];
			Vector2 v2 = mapped [2] - mapped [1];
			if (Vector2.Dot (v1, v2) > 0) {
				Debug.Log ("HouseDrawShape.MapVertices dot>0");
			} else {
				Debug.Log ("HouseDrawShape.MapVertices dot<0");
			}
		}*/

		return mapped;
	}

	public List<Vector2> SourceVertices2d{
		get{
			if (_line==null) return null;
			return MapVertices();
		}
	}
}