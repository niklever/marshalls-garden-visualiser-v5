﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GV_UI : MonoBehaviour {
	static GameObject _modalPanels;
	static Animator _animator;
	GameObject _busy;
	GameObject _alertPanel;

	public static bool ModalActive{
		get{
			return (_activeModalPanel!=null && _activeModalPanel.Count>0);
		}
	}
	
	static NewProjectController _newProjectController;
	public static NewProjectController NewProjectController{
		get{
			return _newProjectController;
		}
	}
	static PhotoController _photoController;
	static ProjectsController _projectsController;
	public static ProjectsController ProjectsController{
		get{
			return _projectsController;
		}
	}
	static DesignController _designController;
	public static DesignController DesignController{
		get{
			return _designController;
		}
	}
	static AlertController _alertController;
	static ProductsController _productsController;
	static RealityController _realityController;
	static InstallerStockistController _installerStockistController;
	static ScreenshotsController _screenshotsController;
	static SettingsController _settingsController;
	static HelpController _helpController;
	static DrawShape _drawShape;
	static AddHouseController _addHouseController;
	static GameObject _drawGardenPanel;
	static GameObject _mainBg;
	static GameObject _stopPanel;
	static WebCamController _webcamController;

	Vector3 _imagePos;
	Quaternion _imageRot;
	Vector3 _imageScale;

	public float Scale {
		get {
			return transform.localScale.y;
		}
	}

	public static void ShowMainBg(bool show){
		if (_mainBg!=null) _mainBg.SetActive (show);
	}

	public void ResetImage(){
		Transform t = GameObject.Find ("CanvasBehind3D/Image").transform;
		t.position = _imagePos;
		t.rotation = _imageRot;
		t.localScale = _imageScale;
	}

	public static DrawShape DrawShape{
		get{
			return _drawShape;
		}
		set{
			_drawShape = value;
		}
	}

	static HouseDrawShape _houseDrawShape;
	public static HouseDrawShape HouseDrawShape{
		get{
			if (_houseDrawShape == null) {
				GameObject go = GameObject.Find ("Canvas");
				_houseDrawShape = go.GetComponent<HouseDrawShape> ();
			}
			return _houseDrawShape;
		}
		set{
			_houseDrawShape = value;
		}
	}

	public static bool ButtonCapturedMouse = false;
	public static bool Change{ get; set; }

	static Stack<GameObject> _activeModalPanel;
	static GardenScript _gardenScript;
	public static GardenScript GardenScript{
		get{
			if (_gardenScript==null){
				GameObject go = GameObject.Find ("3D");
				_gardenScript = go.GetComponent<GardenScript>();
			}
			return _gardenScript;
		}
		set{
			_gardenScript = value;
		}
	}

	public static AddHouseController AddHouseController{
		get{
			if (_addHouseController == null) {
				GameObject go = GameObject.Find ("Canvas/ModalPanels/AddHousePanel");
				if (go) _addHouseController = go.GetComponent<AddHouseController> ();
			}
			return _addHouseController;
		}
	}

	public void ShowLoading(bool show){
 		if (_busy!=null) _busy.SetActive (show);
	}

	public static bool DesignActive{
		get{
			bool active = false;
			if (_designController!=null) active = _designController.gameObject.activeSelf;
			return active;
		}
	}

	public static bool ProductGUIShown{
		get{
			if (ModalActive) {
				GameObject[] panels = _activeModalPanel.ToArray ();
				if (panels [panels.Length - 1] == _productsController.gameObject) return true;
			}
			return false;
		}
	}

	// Use this for initialization
	void Start () {
		_animator = GetComponent<Animator>();
		_busy = GameObject.Find ("Canvas/BusyPanel");
		_busy.SetActive (false);
		GameObject go = GameObject.Find ("Canvas/ModalPanels/NewProjectPanel");
		_newProjectController = go.GetComponent<NewProjectController> ();
		go.SetActive (false);
		if (_modalPanels==null) _modalPanels = GameObject.Find ("Canvas/ModalPanels");
		go = GameObject.Find ("Canvas/AlertPanel");
		if (_alertController==null) _alertController = go.GetComponent<AlertController>();
		go = GameObject.Find ("Canvas/ModalPanels/PhotoPanel");
		_photoController = go.GetComponent<PhotoController>();
		go.SetActive (false);
		go = GameObject.Find ("Canvas/ProjectsPanel");
		_projectsController = go.GetComponent<ProjectsController>();
		go = GameObject.Find ("Canvas/ModalPanels/ProductsPanel");
		_productsController = go.GetComponent<ProductsController>();
		go.SetActive (false);
		go = GameObject.Find ("Canvas/ModalPanels/RealityPanel");
		_realityController = go.GetComponent<RealityController>();
		_realityController.Init ();
		go.SetActive (false);
		go = GameObject.Find ("Canvas/ModalPanels/InstallerStockistPanel");
		_installerStockistController = go.GetComponent<InstallerStockistController>();
		_installerStockistController.Init();
		go.SetActive (false);
		go = GameObject.Find ("Canvas/ModalPanels/ScreenshotsPanel");
		_screenshotsController = go.GetComponent<ScreenshotsController>();
		go.SetActive (false);
		go = GameObject.Find ("Canvas/ModalPanels/SettingsPanel");
		_settingsController = go.GetComponent<SettingsController>();
		_settingsController.Init ();
		go.SetActive (false);
		go = GameObject.Find ("Canvas/ModalPanels/HelpPanel");
		_helpController = go.GetComponent<HelpController>();
		go.SetActive (false);
		go = GameObject.Find ("Canvas/ModalPanels/AddHousePanel");
		_addHouseController = go.GetComponent<AddHouseController>();
		go.SetActive (false);
		go = GameObject.Find ("Canvas/ModalPanels/WebCamPanel");
		_webcamController = go.GetComponent<WebCamController> ();
		_webcamController.AddHouseController = _addHouseController;
		go.SetActive (false);
		_drawGardenPanel = GameObject.Find ("Canvas/ModalPanels/DrawGardenPanel");
		_drawGardenPanel.SetActive (false);
		_stopPanel = GameObject.Find ("Canvas/ModalPanels/StopPanel");
		_stopPanel.SetActive (false);
		_modalPanels.SetActive (false);
		go = GameObject.Find ("Canvas/DesignPanel");
		_designController = go.GetComponent<DesignController>();
		go.SetActive(false);
		_alertController.Hide ();
		_activeModalPanel = new Stack<GameObject>();
		_mainBg = GameObject.Find ("Canvas/Image");

		GVWebService.LogVisit ();
		GVWebService.LoadProductsConfig ();
	}

	public void ShowHelp(){
		#if UNITY_EDITOR
		Application.OpenURL (string.Format ("{0}gv/help/index.html", GVWebService.ResourcePath));
		#else
		#if UNITY_WEBPLAYER
		Application.ExternalCall("showHelp", true);
		#else
		Application.OpenURL (string.Format ("{0}gv/help/index.html", GVWebService.ResourcePath));
		#endif
		#endif
	}

	public void helpPressed(){
		Debug.Log ("Help pressed");
		ShowHelp ();
	}

	public void drawPressed(){
		CloseModalPanel ();
		GardenScript.DrawPlot ();
		ButtonCapturedMouse = false;
	}

	public static void Animate(string trigger){
		_animator.SetTrigger (trigger);
	}

	public bool Busy{
		set{
			_busy.SetActive (value);
		}
	}

	public static void ShowDesignPanel(bool show){
		if (_designController!=null) _designController.gameObject.SetActive (show);
		if (show) Animate("designIn");
	}

	public static void ShowError(string message){
		Debug.Log (string.Format ("GV_UI.ShowError message:{0}", message));
		ButtonCapturedMouse = true;
		if (_modalPanels) _modalPanels.SetActive (true);
		if (_alertController!=null) _alertController.Show ("ERROR", message);
	}
	
	public static void ShowAlert(string title, string message){
		Debug.Log (string.Format ("GV_UI.ShowError title:{0} message:{1}", title, message));
		ButtonCapturedMouse = true;
		if (_modalPanels) _modalPanels.SetActive (true);
		if (_alertController!=null) _alertController.Show (title, message);
	}

	public void CloseAlert(){
		ButtonCapturedMouse = false;
		if (_modalPanels){
			bool activePanel = false;
			for(int i=0; i<_modalPanels.transform.childCount; i++){
				GameObject go = _modalPanels.transform.GetChild (i).gameObject;
				if (go.activeSelf){
					activePanel = true;
					break;
				}
			}
			_modalPanels.SetActive (activePanel);
		}
		if (_alertController!=null) _alertController.Hide ();
	}

	public static void ShowChangePanel(Product product){
		Change = true;
		_modalPanels.SetActive(true);
		_productsController.Show(product);
		_activeModalPanel.Push(_productsController.gameObject);
	}

	public static void ShowModalPanel(string panel, string type="", bool change=false){
		Change = change;
		_modalPanels.SetActive(true);
		switch(panel){
		case "StopCropHouse":
			_stopPanel.SetActive (true);
			_activeModalPanel.Push(_stopPanel);
			break;
		case "AddHouse":
			_addHouseController.Show();
			_activeModalPanel.Push(_addHouseController.gameObject);
			break;
		case "NewProject":
			_newProjectController.Show ();
			_activeModalPanel.Push(_newProjectController.gameObject);
			break;
		case "Draw":
			_photoController.Show(true);
			_activeModalPanel.Push(_photoController.gameObject);
			break;
		case "Photo":
			_photoController.Show(false);
			_activeModalPanel.Push(_photoController.gameObject);
			break;
		case "Products":
			_productsController.Show(type);
			_activeModalPanel.Push(_productsController.gameObject);
			break;
		case "Reality":
			_realityController.Show();
			_activeModalPanel.Push(_realityController.gameObject);
			break;
		case "Installer":
			_installerStockistController.Show (true);
			_activeModalPanel.Push(_installerStockistController.gameObject);
			break;
		case "Stockist":
			_installerStockistController.Show (false);
			_activeModalPanel.Push(_installerStockistController.gameObject);
			break;
		case "Screenshots":
			_screenshotsController.Show ();
			_activeModalPanel.Push(_screenshotsController.gameObject);
			break;
		case "Settings":
			_settingsController.Show();
			_activeModalPanel.Push(_settingsController.gameObject);
			break;
		case "Help":
			_helpController.Show();
			_activeModalPanel.Push(_helpController.gameObject);
			break;
		case "DrawGarden":
			_drawGardenPanel.SetActive (true);
			_activeModalPanel.Push(_drawGardenPanel);
			break;
		case "WebCam":
			_webcamController.Show ();
			_activeModalPanel.Push (_webcamController.gameObject);
			break;
		default:
			Debug.Log ( string.Format("GV_UI.ShowModalPanel {0} not supported", panel));
			_modalPanels.SetActive(false);
			break;
		}
	}

	public static void CloseModalPanel(){
		Debug.Log ("GV_UI.CloseModalPanel " + _activeModalPanel.Count);
		if (_activeModalPanel.Count > 0) {
			GameObject panel = _activeModalPanel.Pop ();
			_modalPanels.SetActive (_activeModalPanel.Count > 0);
			panel.SetActive (false);
		}
	}

	public void ObjectSelected(bool selected, string buttons="edit")
	{
		if (_designController != null) {
			if (selected) {
				_designController.SetFooterButtons (buttons);
			} else {
				_designController.SetFooterState ("edit");
			}
		}
	}

	public void ObjectSelected(bool selected, bool paving)
	{
		if (_designController != null) {
			if (selected) {
				if (paving) {
					_designController.SetFooterButtons ("move,rotate,move_laying_pattern,rotate_laying_pattern");
				} else {
					_designController.SetFooterButtons ("move,rotate");
				}
			} else {
				_designController.SetFooterState ("edit");
			}
		}
	}

	public static void UpdateFooter(Product product){
		if (_designController!=null) _designController.SetFooterButtons(product.ModifyButtons);
	}

}
