﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SimpleJSON;

public class InstallerListItem : MonoBehaviour {
	Text _name;
	Text _address;
	Text _telephone;
	Button _profileButton;
	string _url;

	public void Init(JSONNode node, bool installer, bool white){
		GameObject go = transform.GetChild (0).gameObject;
		_name = go.GetComponent<Text>();
		_name.text = node["Name"];
		go = transform.GetChild (1).gameObject;
		_address = go.GetComponent<Text>();
		_address.text = node["FullAddress"];
		go = transform.GetChild (2).gameObject;
		_telephone = go.GetComponent<Text>();
		_telephone.text = node["Telephone"];
		if (installer) {
			_url = node ["ViewMapUrl"];
		}else{
			go = transform.GetChild (3).gameObject;
			go.SetActive (false);
		}
		if (white){
			Image image = GetComponent<Image>();
			image.color = Color.white;
		}
	}

	public void ProfilePressed(){
		Application.OpenURL (_url);
		Analytics.ga ("Project", "View installers profile", _name.text);
	}
}
