﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class NewProjectController : MonoBehaviour {
	bool _metres = true;
	Project _project = null;
	public Project Project {
		set {
			_project = value;
			if (_project != null) {
				GameObject go = transform.Find ("NameTxt").gameObject;
				InputField ip = go.GetComponent<InputField> ();
				ip.text = _project.Name;
				float multiple = (_metres) ? 0.001f : 1;
				go = transform.Find ("LengthTxt").gameObject;
				ip = go.GetComponent<InputField> ();
				ip.text = (_project.Constraints.x * multiple).ToString();
				go = transform.Find ("WidthTxt").gameObject;
				ip = go.GetComponent<InputField> ();
				ip.text = (_project.Constraints.y * multiple).ToString();
			}
		}
	}

	// Use this for initialization
	void Start () {
		if (_project == null) {
			Clear ();
		} 
	}

	public void Clear(){
		string[] inputs = new string[] { "NameTxt", "LengthTxt", "WidthTxt" };
		foreach (string input in inputs) {
			GameObject go = transform.Find (input).gameObject;
			InputField ip = go.GetComponent<InputField> ();
			ip.text = "";
		}
		_metres = false;
		metresCmPressed ();//This will set the correct image and flip _metres to true
	}

	public void Show(){
		gameObject.SetActive(true);
	}

	public void closePressed(){
		GV_UI.CloseModalPanel ();
		_project = null;
	}

	public void metresCmPressed(){
		GameObject go = transform.Find ("LengthTxt").gameObject;
		InputField ip1 = go.GetComponent<InputField>();
		string lengthStr = ip1.text;
		if (lengthStr != null && lengthStr != "") {
			float length = float.Parse (ip1.text);
			if (_metres) {
				length *= 1000;
			} else {
				length /= 1000;
			}
			ip1.text = length.ToString ();
		}
		go = transform.Find ("WidthTxt").gameObject;
		InputField ip2 = go.GetComponent<InputField>();
		string widthStr = ip2.text;
		if (widthStr != null && widthStr != "") {
			float width = float.Parse (ip2.text);
			if (_metres) {
				width *= 1000;
			} else {
				width /= 1000;
			}
			ip2.text = width.ToString ();
		}
		_metres = !_metres;
		string name = (_metres) ? "metres_cm_btn" : "cm_metres_btn";
		go = transform.Find ("UnitsButton").gameObject;
		Image image = go.GetComponent<Image> ();
		image.sprite = Resources.Load<Sprite> ("Textures/" + name);
	}

	public void NewProjectPressed(){
		Debug.Log ("NewProjectController.NewProjectPressed");
		GameObject go = transform.Find ("NameTxt").gameObject;
		InputField ip1 = go.GetComponent<InputField>();
		go = transform.Find ("LengthTxt").gameObject;
		InputField ip2 = go.GetComponent<InputField>();
		go = transform.Find ("WidthTxt").gameObject;
		InputField ip3 = go.GetComponent<InputField>();
		if (ip1.text == "") {
			GV_UI.ShowAlert ("New project error", "Please enter a project name");
		}else if (ip2.text==""){
			GV_UI.ShowAlert("New project error", "Please enter a length");
		}else if (ip3.text==""){
			GV_UI.ShowAlert("New project error", "Please enter a width");
		}else{
			float length = float.Parse (ip2.text);
			float width = float.Parse(ip3.text);
			if (_metres) {
				length *= 1000;
				width *= 1000;
			}
			if (_project == null) {
				GardenScript.CreateNewProject (ip1.text, (int)length, (int)width);
			} else {
				_project.Name = ip1.text;
				Vector2 constraints = new Vector2 (length, width);
				_project.Constraints = constraints;
				go = GameObject.Find ("Main Camera");
				CameraScript cameraScript = go.GetComponent<CameraScript> ();
				cameraScript.Constraints = _project.Constraints * 0.001f;
				cameraScript.SetCamera (1, true);//Overhead
				GardenScript.Modified = true;
				GV_UI.CloseModalPanel ();
				_project = null;
			}
		}
		Analytics.ga ("Project", "Size of project");
	}
}
