﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

public class ScreenshotsController : MonoBehaviour {
	public GameObject _thumbnail;
	SnapshotController _snapshotController;
	Transform _pagePanel;

	// Use this for initialization
	void Start () {
		_pagePanel = transform.Find ("PagePanel");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Init(){
		gameObject.SetActive (true);

		UpdatePanel();
	}

	public void UpdatePanel (int page = 0){
		Transform t = transform.Find ("Buttons");
		while (t.childCount > 0) {
			Transform t1 = t.GetChild (0);
			t1.parent = null;
			Destroy (t1.gameObject);
		}
		if (GardenScript.Project != null && GardenScript.Project.Shots != null) {
			List<Snapshot> snapshots = GardenScript.Project.Shots;
			int first = page * 12;
			for(int i=first; i<(first+12); i++){
				if (i>=snapshots.Count) break;
				Snapshot snapshot = snapshots[i];
				GameObject go = (GameObject)Instantiate(_thumbnail);
				ScreenshotButton button = go.GetComponent<ScreenshotButton> ();
				button.Snapshot = snapshot;
				go.transform.SetParent (t, false);
			}
		}
		UpdatePageControl(page);
	}

	void UpdatePageControl(int current=0){
		int count = 0;
		if (GardenScript.Project != null && GardenScript.Project.Shots != null)
			count = (int)Math.Floor((double)(GardenScript.Project.Shots.Count/12));
		if (count < 1) {
			_pagePanel.gameObject.SetActive (false);
		} else {
			_pagePanel.gameObject.SetActive (true);
			for(int i=0; i<_pagePanel.childCount; i++){
				GameObject go = _pagePanel.GetChild (i).gameObject;
				go.SetActive (i<count);
				Image image = go.GetComponent<Image>();
				if (i==current){
					image.sprite = Resources.Load<Sprite>("Textures/page_selected");
				}else{
					image.sprite = Resources.Load<Sprite>("Textures/page_unselected");
				}
			}
		}
	}

	public void Show(){
		if (_snapshotController == null) {
			GameObject go = GameObject.Find ("ThumbnailCamera");
			if (go) _snapshotController = go.GetComponent<SnapshotController> ();
		}

		if (_snapshotController != null) {
			_snapshotController.Grab (GardenScript.Project.Shots, this);
		} else {
			GV_UI.ShowAlert ("Warning", "No Snapshot Controller found");
		}
	}

	public void ClosePressed(){
		GV_UI.CloseModalPanel ();
	}

	public void DeleteSnapshot(ScreenshotButton thumbnail){
		
	}
}
