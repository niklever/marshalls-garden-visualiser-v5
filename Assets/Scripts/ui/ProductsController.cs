﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Xml;
using System.Text;

public class ProductsController : MonoBehaviour {
	public GameObject _crumbButton;

	XmlDocument _menu;
	GameObject _crumbTrail;
	List<ProductIcon> _iconData;
	int _firstIcon;
	string _type;
	string _panelType;
	GameObject _swatchPanel;
	GameObject _heroPanel;
	GameObject _heroBPanel;
	GameObject _heightPanel;
	GameObject _widthPanel;
	Transform _pagePanel;
	Stack<XmlNode> _crumbTrailStack;
	float _crumbTrailLeft;
	bool _hero;
	Text _title;
	int _page;
	Product _product;
	Dictionary<string, string> _products;

	//ReaderXML _readerXML;

	// Use this for initialization
	void Start () {
		_crumbTrail = transform.Find ("CrumbtrailPanel").gameObject;
		_crumbTrail.SetActive (false);

		_swatchPanel = transform.Find ("Buttons").gameObject;
		_heroPanel = transform.Find ("HeroPanel").gameObject;
		_heroPanel.SetActive (false);
		_heroBPanel = transform.Find ("HeroBPanel").gameObject;
		_heroBPanel.SetActive (false);
		_heightPanel = transform.Find ("HeightPanel").gameObject;
		_heightPanel.SetActive (false);
		_widthPanel = transform.Find ("WidthPanel").gameObject;
		_widthPanel.SetActive (false);

		_pagePanel = transform.Find ("PagePanel");
		_pagePanel.gameObject.SetActive (false);

		TextAsset data= Resources.Load<TextAsset>("XML/product-menu-patterns");
		_menu = new XmlDocument();
		_menu.LoadXml(data.text);
		_products = PreProcessMenu ();

		GameObject go = transform.Find ("Title").gameObject;
		_title = go.GetComponent<Text> ();

		_crumbTrailStack = new Stack<XmlNode>();

		if (_type!=null) Show(_type);

		_hero = false;
	}

	Dictionary<string, string>PreProcessMenu(){
		return null;
	}

	void UpdatePopular(int page){
		List<XmlNode> nodes = GetPopular ();
		bool last = false;
		if (page == 100) {
			//Next page
			page = _page + 1;
		}else if (page==99){
			//Previous page
			page = _page - 1;
		} else {
			last = (page == Math.Ceiling ((double)nodes.Count / 4.0));
		}
		_page = page;
		last = (page == Math.Ceiling ((double)nodes.Count / 4.0));
		_pagePanel.gameObject.SetActive (true);
		for(int i=1; i<_pagePanel.childCount-1; i++){
			GameObject go = _pagePanel.GetChild (i).gameObject;
			if (!go.activeSelf)
				break;
			Image image = go.GetComponent<Image>();
			if (i==page){
				image.sprite = Resources.Load<Sprite>("Textures/page_selected");
			}else{
				image.sprite = Resources.Load<Sprite>("Textures/page_unselected");
			}
		}
		Transform buttons = _heroPanel.transform;

		_pagePanel.GetChild (0).gameObject.SetActive (page > 1);
		_pagePanel.GetChild (_pagePanel.childCount - 1).gameObject.SetActive (!last);

		for (int i = 2; i < buttons.childCount; i++) {
			GameObject go = buttons.GetChild (i).gameObject;
			PavingButtonController controller = go.GetComponent<PavingButtonController> ();
			int idx = (page - 1) * 4 + (i - 2);
			if (idx<nodes.Count){
				XmlNode node = nodes [idx];
				ProductIcon icon = new ProductIcon (node);
				controller.UpdateButton (icon);
				go.SetActive(true);
			}else{
				go.SetActive(false);
			}
		}
	}

	// Update is called once per frame
	void UpdatePanel (string type, int first=0, XmlNode node=null) {
		int page = 0;
		bool prevOrNext = false;
		if (first == 792) {
			page = _page + 1;
			prevOrNext = true;
		} else if (first == 784) {
			page = _page - 1;
			prevOrNext = true;
		}
		if (type == "hero") {
			if (prevOrNext)
				first = page * 4;
			_page = (int)Math.Ceiling ((double)(first / 4.0));
		} else {
			if (prevOrNext)
				first = page * 8;
			_page = (int)Math.Ceiling ((double)(first / 8.0));
		}
		_firstIcon = first;
		_panelType = type;
		Transform buttons = null; 

		_swatchPanel.SetActive (false);
		_heroPanel.SetActive (false);
		_heroBPanel.SetActive (false);
		_heightPanel.SetActive (false);
		_widthPanel.SetActive (false);
		_crumbTrail.SetActive (false);

		_hero = false;

		if (type=="swatch"){
			buttons = _swatchPanel.transform;
			_swatchPanel.SetActive (true);
			_crumbTrail.SetActive (true);
		}else if (type=="hero"){
			buttons = _heroPanel.transform;
			_heroPanel.SetActive (true);
			_hero = true;
		}else if (type=="herob"){
			buttons = _heroBPanel.transform;
			_heroBPanel.SetActive (true);
		}else if (type=="width"){
			_widthPanel.SetActive (true);
			SliderController sliderController = _widthPanel.GetComponent<SliderController>();
			sliderController.UpdatePanel (node);
			if (GV_UI.Change) {
				sliderController.Value = (int)GardenScript.SelectedProduct.Size;
			}
			_crumbTrail.SetActive (true);
		}else if (type=="height"){
			_heightPanel.SetActive (true);
			SliderController sliderController = _heightPanel.GetComponent<SliderController>();
			sliderController.UpdatePanel (node);
			if (GV_UI.Change) {
				sliderController.Value = (int)GardenScript.SelectedProduct.Size;
			}
			_crumbTrail.SetActive (true);
		}

		if (buttons != null) {
			for (int i = 0; i < buttons.childCount; i++) {
				GameObject button = buttons.GetChild (i).gameObject;
				if (_iconData != null && (first + i) < _iconData.Count) {
					PavingButtonController pbc = button.GetComponent<PavingButtonController> ();
					if (pbc != null)
						pbc.UpdateButton (_iconData [first + i]);
					button.SetActive (true);
				} else {
					button.SetActive (false);
				}
				if (type == "hero" && i == 1)
					break;
			}
		}

		if (_iconData == null) {
			_pagePanel.gameObject.SetActive (false);
		} else if (type == "hero") {
			//Update Page Control
			List<XmlNode>nodes = GetPopular();
			int count = (int)Math.Ceiling((double)(nodes.Count/4.0));
			int current = 0;
			if (count>1){
				_pagePanel.gameObject.SetActive (true);
				for (int i = 0; i < _pagePanel.childCount-1; i++) {
					GameObject go = _pagePanel.GetChild (i).gameObject;
					go.SetActive (i <= count);
				}
				UpdatePopular(1);
			}else{
				_pagePanel.gameObject.SetActive (false);
			}
		}else{
			//Update Page Control
			int count = (int)Math.Ceiling(_iconData.Count/8.0);
			int current = (int)Math.Floor (first/8.0);
			if (count>1){
				_pagePanel.gameObject.SetActive (true);
				for(int i=1; i<_pagePanel.childCount-1; i++){
					GameObject go = _pagePanel.GetChild (i).gameObject;
					go.SetActive (i<=count);
					Image image = go.GetComponent<Image>();
					if (i==(current+1)){
						image.sprite = Resources.Load<Sprite>("Textures/page_selected");
					}else{
						image.sprite = Resources.Load<Sprite>("Textures/page_unselected");
					}
				}
				_pagePanel.GetChild (0).gameObject.SetActive (first > 0);
				_pagePanel.GetChild (_pagePanel.childCount-1).gameObject.SetActive (current < (count-1));
			}else{
				_pagePanel.gameObject.SetActive (false);
			}
		}
	}

	void AddXmlAttribute(XmlNode node, string name, string value){
		//node = node.PreviousSibling;
		string str = GetXmlAttribute (node, name);
		XmlAttribute attr;
		if (str == "") {
			XmlDocument doc = node.OwnerDocument;
			attr = doc.CreateAttribute (name);
			attr.Value = value;
			node.Attributes.Append (attr);
		} else {
			attr = node.Attributes[name];
			attr.Value = value;
		}
	}

	public void OKPressed(bool width){
		SliderController controller;
		if (width) {
			controller = _widthPanel.GetComponent<SliderController> ();
		} else {
			controller = _heightPanel.GetComponent<SliderController> ();
		}
		int value = controller.Value;
		XmlNode xml = controller.Xml;
		AddXmlAttribute(xml, "size", value.ToString());
		if (GV_UI.Change){
			GV_UI.Change = false;
			GV_UI.GardenScript.ChangeProduct(controller.Xml);
		}else{
			GardenScript.NewProduct(controller.Xml);
		}
		GV_UI.CloseModalPanel();
	}

	List<ProductIcon> GetPage(XmlNode node, string type){
		List<ProductIcon> icons = new List<ProductIcon>();
		if (_menu!=null){
			XmlNodeList pages = _menu.GetElementsByTagName("page");
			for(int i=0; i<pages.Count; i++){
				XmlNode page = pages[i];
				string pagetype = GetXmlAttribute(page, "producttypes");
				if (pagetype==type){
					for(int j=0; j<page.ChildNodes.Count; j++){
						ProductIcon icon = new ProductIcon(page.ChildNodes[j]);
						icons.Add (icon);
					}
					break;
				}
			}
		}
		return icons;
	}

	public void Show(string type){
		Debug.Log (string.Format ("ProductsController type:{0}", type));
		gameObject.SetActive(true);
		_type = type;
		if (_menu==null) return;
		switch(type){
		case "driveway":
		case "paving":
			_iconData = GetPage (_menu.FirstChild, "paving,driveway");
			CreateCrumbTrail ("Start");
			UpdatePanel ("hero", 0);
			UpdateTitle ("PAVING");
			break;
		case "edging":
		case "edging_supported":	
		case "paths":
		case "path":
		case "kerb":
			_iconData = GetPage (_menu.FirstChild, "path,edging,edging_supported,kerb");
			CreateCrumbTrail ("Start");
			UpdatePanel ("herob", 0);
			UpdateTitle ("PATH");
			break;
		case "post":
			_iconData = GetPage (_menu.FirstChild, "walling,post");
			CreateCrumbTrail ("Start");
			UpdatePanel ("herob", 0);
			UpdateTitle ("POST");
			break;
		case "features":
		case "circle":
		case "octant":
			_iconData = GetPage (_menu.FirstChild, "circle,octant");
			UpdatePanel ("swatch", 0);
			UpdateTitle ("FEATURE");
			CreateCrumbTrail("Features");
			break;
		case "accessory":
		case "accessories":
			_iconData = GetPage (_menu.FirstChild, "accessory");
			UpdatePanel ("swatch", 0);
			UpdateTitle ("ACCESSORY");
			CreateCrumbTrail("Accessories");
			break;
		case "aggregate":
		case "rock":
		case "surface":
		case "aggregates":
			_iconData = GetPage (_menu.FirstChild, "aggregate,rock,surface");
			UpdatePanel ("swatch", 0);
			UpdateTitle ("AGGREGATES");
			CreateCrumbTrail("Aggregates & Surfaces");
			break;
		case "walling":
		case "palisades":
			_iconData = GetPage (_menu.FirstChild, "walling,palisades");
			UpdatePanel ("swatch", 0);
			UpdateTitle ("WALL");
			CreateCrumbTrail("Walling");
			break;
		case "fences":
		case "boundary":
			_iconData = GetPage (_menu.FirstChild, "boundary");
			UpdatePanel ("swatch", 0);
			UpdateTitle ("FENCES");
			CreateCrumbTrail("Fences & Hedges");
			break;
		case "trees":
		case "vegetation":
			_iconData = GetPage (_menu.FirstChild, "vegetation");
			UpdatePanel ("swatch", 0);
			UpdateTitle ("TREE");
			CreateCrumbTrail("Trees & Plants");
			break;
		case "buildings":
		case "summerhouse":
		case "greenhouse":
			_iconData = GetPage (_menu.FirstChild, "summerhouse,greenhouse");
			UpdatePanel ("swatch", 0);
			UpdateTitle ("BUILDING");
			CreateCrumbTrail("Buildings");
			break;
		default:
			GV_UI.ShowAlert ("Warning", string.Format ("{0} not supported", type));
			break;
		}
	}

	void UpdateTitle(string name){
		if (_title == null) {
			GameObject go = transform.Find ("Title").gameObject;
			_title = go.GetComponent<Text> ();
		}
		string title = string.Format("CHOOSE YOUR {0}", name);
		_title.text = title;
	}

	public void Show(XmlNode node, int hideCrumb=-1, bool fullcrumb=false){
		List<ProductIcon> icons = new List<ProductIcon>();

		XmlNode options = null;
		string type = "swatch";

		if (node.Name == "page") {
			options = node;
		}else{
			for (int i = 0; i < node.ChildNodes.Count; i++) {
				if (node.ChildNodes [i].Name == "page") {
					options = node.ChildNodes [i];
					string pageType = GetXmlAttribute (options, "type");
					if (pageType == "width" || pageType == "height") {
						type = pageType;
					}
				}
			}
		}

		if (options != null) {
			try {
				Debug.Log ("ProductsController.Show " + options.InnerXml);
			} catch (NullReferenceException e) {
				Debug.Log ("ProductsController.Show NullReferenceException");
			}

			for (int j = 0; j < options.ChildNodes.Count; j++) {
				ProductIcon icon = new ProductIcon (options.ChildNodes [j]);
				icons.Add (icon);
			}
		}

		_iconData = icons;

		if (type != "swatch") {
			UpdatePanel (type, 0, options);
		} else {
			UpdatePanel (type, 0, node);
		}

		if (fullcrumb) {
			CreateCrumbTrail ("Start");
			List<XmlNode> nodes = new List<XmlNode> ();
			while (node != null) {
				nodes.Add (node);
				node = node.ParentNode;
				while (node != null && node.Name != "option")
					node = node.ParentNode;
			}
			for (int i = nodes.Count - 1; i >= 0; i--) {
				node = nodes [i];
				AppendCrumbTrail (GetXmlAttribute (node, "name"), node);
			}
		} else {
			if (hideCrumb == -1) {
				AppendCrumbTrail (GetXmlAttribute (node, "name"), node);
			} else {
				InitCrumbTrail (hideCrumb);
			}
		}
		if (_crumbTrail.activeSelf)
			UpdateCrumbTrailAfterCreation ();
	}

	void UpdateCrumbTrailAfterCreation(){
		Transform panel = _crumbTrail.transform.Find ("Panel");
		for (int i = 0; i < panel.childCount; i++) {
			GameObject button = panel.GetChild (i).gameObject;
			CrumbButtonController controller = button.GetComponent<CrumbButtonController>();
			Text txt = button.transform.GetChild (0).gameObject.GetComponent<Text> ();
			if (i == panel.childCount - 1) {
				controller.UpdateButton (CrumbButtonType.LAST);
			}else if (i == panel.childCount - 2) {
				controller.UpdateButton (CrumbButtonType.PENULTIMATE);
			} else {
				controller.UpdateButton (CrumbButtonType.NORMAL);
			}
		}
	}

	void InitCrumbTrail(int hideCrumb){
		Transform panel = _crumbTrail.transform.Find ("Panel");
		for (int i = panel.childCount - 1; i >= hideCrumb; i--) {
			GameObject go = panel.GetChild (i).gameObject;
			go.transform.parent = null;
			Destroy (go);
		}
		if (hideCrumb == 0) {
			_crumbTrailLeft = 10;
		} else {
			GameObject button = panel.GetChild (panel.childCount - 1).gameObject;
			CrumbButtonController controller = button.GetComponent<CrumbButtonController> ();
			_crumbTrailLeft = controller.Right;
		}
	}

	void CreateCrumbTrail(string name){
		int left = 10;
		Transform panel = _crumbTrail.transform.Find ("Panel");
		for(int i=panel.childCount-1; i>=0; i--){
			GameObject go = panel.GetChild (i).gameObject;
			go.transform.parent = null;
			Destroy(go);
		}
		_crumbTrail.SetActive(true);
		GameObject button = Instantiate(_crumbButton);
		button.transform.SetParent(panel, false);
		CrumbButtonController controller = button.GetComponent<CrumbButtonController>();
		controller.UpdateButton(name, left, _type);
		_crumbTrailLeft = controller.Right;
		Debug.Log (string.Format("ProductsController.CreateCrumbTrail left:{0} name:{1}", _crumbTrailLeft, name));
	}

	void AppendCrumbTrail(string name, XmlNode node){
		Transform panel = _crumbTrail.transform.Find ("Panel");
		GameObject button = Instantiate(_crumbButton);
		button.transform.SetParent(panel, false);
		CrumbButtonController controller = button.GetComponent<CrumbButtonController>();
		controller.UpdateButton(name, _crumbTrailLeft, node);
		_crumbTrailLeft = controller.Right;
		Debug.Log (string.Format("ProductsController.AppendCrumbTrail left:{0} name:{1}", _crumbTrailLeft, name));
	}

	public void ChangePagePressed(int page){
		if (_hero) {
			UpdatePopular (page);
		} else {
			UpdatePanel (_panelType, (page - 1) * 8);
		}
	}

	public void ClosePressed(){
		GV_UI.CloseModalPanel ();
	}
	
	string GetXmlAttribute(XmlNode node, string str){
		XmlAttribute attr = node.Attributes[str];
		if (attr==null) return "";
		return attr.Value;
	}

	List<XmlNode> GetNodes(string nodeName, string attributeName, string attributeValue){
		List<XmlNode> nodes = new List<XmlNode>();
		if (_menu!=null){
			XmlNodeList pages = _menu.GetElementsByTagName(nodeName);
			for(int i=0; i<pages.Count; i++){
				XmlNode page = pages[i];
				string attr = GetXmlAttribute(page, attributeName);
				if (attr==attributeValue){
					nodes.Add (page);
					break;
				}
			}
		}
		return nodes;
	}

	List<XmlNode> GetPopular(){
		List<XmlNode> lst = new List<XmlNode> ();
		if (_menu!=null){
			XmlNodeList pages = _menu.GetElementsByTagName("option");
			for(int i=0; i<pages.Count; i++){
				XmlNode page = pages[i];
				string popular = GetXmlAttribute(page, "popular");
				if (popular=="true") lst.Add (page);
			}
		}
		return lst;
	}

	string DetailsToString(Dictionary<string,string>details){
		StringBuilder str = new StringBuilder();
		foreach (string key in details.Keys) {
			str.Append(string.Format ("{0}:{1} ", key, details[key]));
		}
		return str.ToString();
	}

	public Dictionary<string, string> GetDictionaryFromXml(XmlNode node){
		Dictionary<string, string> details = new Dictionary<string, string>();
		StringBuilder str = new StringBuilder();

		if (node!=null){
			bool found = false;
			int count = 0;

			do{
				if (node.Name=="productproperties"){
					foreach(XmlAttribute attr in node.Attributes){
						try{
							details.Add (attr.Name, attr.Value);
							str.Append(string.Format ("{0}:{1} ", attr.Name, attr.Value));
						}catch(ArgumentException e){
							Debug.Log (string.Format("ProductsController.GetDictionaryFromXml {0}:{1}", attr.Name, attr.Value));
						}
					}
				}
				while(node!=null && node.Name!="option") node = node.ParentNode;
				found = false;
				count++;
			}while(node!=null && count<20);
			//Debug.Log ("GardenScript.GetDictionaryFromXml " + str);
		}

		return details;
	}

	void CheckParentAttributes(XmlAttribute attr, XmlNode parent, Dictionary<string,string>details){
		XmlNode parentAttr;
		switch(attr.Name){
		case "color":
			parentAttr = parent.Attributes.GetNamedItem("name");
			if (parentAttr!=null){
				details.Add ("displaycolor", parentAttr.Value);
			}
			parentAttr = parent.Attributes.GetNamedItem("image");
			if (parentAttr!=null){
				details.Add ("colorswatch", parentAttr.Value);
			}
			break;
		case "pattern":
			parentAttr = parent.Attributes.GetNamedItem("name");
			if (parentAttr!=null){
				details.Add ("displaypattern", parentAttr.Value);
			}
			parentAttr = parent.Attributes.GetNamedItem("image");
			if (parentAttr!=null){
				details.Add ("patternswatch", parentAttr.Value);
			}
			break;
		}
	}

	XmlNode GetXmlNodeForProduct(XmlNode page, Product product, Dictionary<string,string>details, int level=0){
		XmlNode node = null;
		Debug.Log (string.Format ("GetXmlNodeForProduct page:{0} details:{1} level:{2}", page.Name, details.Count, level));

		if (page.HasChildNodes && page.ChildNodes.Count==1 && !page.ChildNodes[0].HasChildNodes) {
			//This is top of the hierarchy
			if (page.ChildNodes[0].Name == "productproperties") {
				XmlNode child = page.ChildNodes [0];
				foreach(XmlAttribute attr in child.Attributes){
					try{
						if (details.ContainsKey(attr.Name)){
							details[attr.Name] = attr.Value;
						}else{
							details.Add (attr.Name, attr.Value);
						}
						CheckParentAttributes(attr, page, details);
					}catch(ArgumentException e){
						Debug.Log (string.Format("ProductsController.GetXmlNodeForProduct A {0}:{1}", attr.Name, attr.Value));
					}
				}
			}
			//string str = DetailsToString (details);
			//Debug.Log (string.Format("ProductsController.GetXmlNodeForProduct dict:{0}", str));
			if (product.Compare (details)) node = page;
			if (node == null && product.Type != "paving" && product.Type != "driveway" && product.Compare (details, false)) {
				Dictionary<string, string> detailsB = GardenScript.GetDictionaryFromXml (page.ChildNodes [0]);
				if (product.Compare (detailsB))
					node = page;
			//} else if (product.Type == "post") {
			//	if (product.Compare (details, false))
			//		node = page;
			}
		} else {
			if (level == 0) details = new Dictionary<string, string> ();
			foreach (XmlNode child in page.ChildNodes) {
				if (child.Name == "page" || child.Name == "option") {
					node = GetXmlNodeForProduct (child, product, details, level + 1);
					if (node != null)
						break;
				} else if (child.Name == "productproperties") {
					foreach(XmlAttribute attr in child.Attributes){
						try{
							if (details.ContainsKey(attr.Name)){
								details[attr.Name] = attr.Value;
							}else{
								details.Add (attr.Name, attr.Value);
							}
							CheckParentAttributes(attr, page, details);
						}catch(ArgumentException e){
							Debug.Log (string.Format("ProductsController.GetXmlNodeForProduct B {0}:{1}", attr.Name, attr.Value));
						}
					}
				}
			}
		}

		return node;
	}

	XmlNode GetXmlNodeForProduct(string productTypes, Product product){
		XmlNode node = null;
		List<XmlNode> pages = GetNodes ("page", "producttypes", productTypes);
		Dictionary<string, string> details = new Dictionary<string, string>();

		if (pages != null && pages.Count > 0) {
			XmlNode root = pages [0];
			foreach (XmlNode option in root.ChildNodes) {
				//options
				node = GetXmlNodeForProduct(option, product, details);
				if (node != null)
					break;
			}
		}

		if (node != null) {
			//Move up the hierarchy to find the page tag
			while (node.Name != "page" && node != null)
				node = node.ParentNode;
		} else if (product.Type == "post") {
			node = pages [0];
		}

		return node;
	}

	public void Swipe(bool left){
		if (left && _firstIcon>0){
			_firstIcon-=8;
			UpdatePanel(_type, _firstIcon);
		}else if (!left && (_firstIcon+8)<_iconData.Count){
			_firstIcon+=8;
			UpdatePanel(_type, _firstIcon);
		}
	}

	public void HeightChanged(){
	}

	public void WidthChanged(){
	}

	public void Show(Product product){
		//Find the correct menu node for this product
		string productTypes = "";
		bool useparent = true;
		switch (product.Type) {
		case "paving":
		case "driveway":
			UpdateTitle ("PAVING");
			productTypes = "paving,driveway";
			break;
		case "circle":
		case "octant":
			UpdateTitle ("FEATURE");
			productTypes = "circle,octant";
			break;
		case "path":
		case "edging":
		case "edging_supported":
		case "kerb":
			UpdateTitle ("PATH");
			productTypes = "path,edging,edging_supported,kerb";
			break;
		case "post":
			UpdateTitle ("POST");
			if (product.ModifyButtons.Contains ("walling_add_post")) {
				productTypes = "walling,post";
			} else {
				productTypes = "edging,post";
			}
			break;
		case "walling":
		case "palisades":
			UpdateTitle ("WALL");
			productTypes = "walling,palisades";
			break;
		case "boundary":
			UpdateTitle ("FENCES");
			productTypes = "boundary";
			break;
		case "aggregate":
		case "rock":
		case "surface":
			UpdateTitle ("AGGREGATES");
			productTypes = "aggregate,rock,surface";
			break;
		case "accessory":
			UpdateTitle ("ACCESSORY");
			productTypes = "accessory";
			break;
		case "summerhouse":
		case "greenhouse":
			UpdateTitle ("BUILDING");
			productTypes = "summerhouse,greenhouse";
			break;
		case "vegetation":
			UpdateTitle ("TREE");
			productTypes = "vegetation";
			break;
		default:
			GV_UI.ShowAlert ("Warning", string.Format ("Change product could not find {0}", product.Type));
			break;
		}

		if (productTypes != "") {
			XmlNode node = GetXmlNodeForProduct (productTypes, product);
			if (node != null) {
				_type = product.Type;
				if (node.ParentNode.Name == "menu") {
					Show (node, 0, true);
				} else {
					Show (node.ParentNode, 0, true);
				}
				gameObject.SetActive (true);
			} else {
				GV_UI.CloseModalPanel ();
				GV_UI.ShowAlert ("Warning", "Xml node not found");
			}
		}
	}
}
