﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SimpleJSON;

public class InstallerStockistController : MonoBehaviour {
	public GameObject _listItem;

	GameObject _inputPanel;
	GameObject _listPanel;
	InputField  _postcode;
	GameObject [] _bgs;
	Text _title;
	Text _description;
	bool _installer;
	GameObject _scroller;
	Text _findBtnTxt;
	
	// Use this for initialization
	public void Init () {
		_inputPanel = transform.Find ("InputPanel").gameObject;
		_listPanel = transform.Find ("ListPanel").gameObject;
		_scroller = transform.Find ("ListPanel/Panel/List/Scroller").gameObject;
		_listPanel.SetActive (false);
		GameObject go = transform.Find ("InputPanel/PostcodeTxt").gameObject;
		_postcode = go.GetComponent<InputField>();
		/*_ips = new InputField[5];
		string [] ips = { "HouseTxt", "AddressTxt", "TownTxt", "PostcodeTxt", "TelTxt" };
		int index = 0;
		GameObject go;
		foreach(string ip in ips){
			go = transform.Find (string.Format ("InputPanel/{0}", ip)).gameObject;
			_ips[index] = go.GetComponent<InputField>();
			index++;
		}*/
		go = transform.Find ("InputPanel/FindButton/Text").gameObject;
		_findBtnTxt = go.GetComponent<Text> ();

		_bgs = new GameObject[2];
		_bgs[0] = transform.Find ("InstallerBg").gameObject;
		_bgs[1] = transform.Find ("StockistBg").gameObject;
		go = transform.Find ("Title").gameObject;
		_title = go.GetComponent<Text>();
		go = transform.Find ("InputPanel/Description").gameObject;
		_description = go.GetComponent<Text>();
		ClearList();
	}
	
	void ClearInput(){
		_postcode.text = "";
		//foreach(InputField ip in _ips) ip.text = "";
	}

	void ClearList(){
		for(int i=_scroller.transform.childCount-1; i>=0; i--){
			Destroy(_scroller.transform.GetChild (i).gameObject);
		}
	}

	public void Show(bool installer){
		_installer = installer;
		ClearInput();
		if (installer){
			_bgs[0].SetActive(true);
			_bgs[1].SetActive(false);
			_title.text = "FIND AN APPROVED INSTALLER";
			_description.text = "Give us a call on 0345 820 5000 or use your postcode to find the nearest Marshalls Register approved paving & driveway installer.";
			_findBtnTxt.text = "Find an approved installer";
		}else{
			_bgs[0].SetActive(false);
			_bgs[1].SetActive(true);
			_title.text = "FIND A MARSHALLS STOCKIST";
			_description.text = "Give us a call on 0345 411 2233 or use your postcode to find the nearest Marshalls Stockist";
			_findBtnTxt.text = "Find an approved stockist";
		}
		_inputPanel.SetActive(true);
		_listPanel.SetActive(false);
		gameObject.SetActive(true);
	}

	public void UpdateList(JSONArray items){
		for(int i=0; i<items.Count; i++){
			GameObject item = (GameObject)Instantiate(_listItem);
			item.transform.SetParent(_scroller.transform, false);
			InstallerListItem listItem = item.GetComponent<InstallerListItem>();
			listItem.Init(items[i], _installer, (i % 2)==0);
		}
		RectTransform rt = _scroller.GetComponent<RectTransform>();
		Vector2 size = rt.sizeDelta;
		size.y = items.Count * 90;
		rt.sizeDelta = size;
		Vector2 pos = rt.anchoredPosition;
		pos.y = 0;
		rt.anchoredPosition = pos;
	}

	public void FindPressed(){
		if (_postcode.text == "") {
			GV_UI.ShowAlert ("WARNING", "Please enter your postcode");
		} else {
			ClearList ();
			_inputPanel.SetActive (false);
			_listPanel.SetActive (true);
			GVWebService.GetInstallers (_postcode.text, _installer, this);
			if (_installer) {
				Analytics.ga ("Project", "Find installer", _postcode.text);
			} else {
				Analytics.ga ("Project", "Find stockist", _postcode.text);
			}
		}
	}

	public void ClosePressed(){
		if (_listPanel.activeSelf){
			_inputPanel.SetActive(true);
			_listPanel.SetActive(false);
		}else{
			GV_UI.CloseModalPanel ();
		}
	}

	public void DonePressed(){
		GV_UI.CloseModalPanel ();
	}
}
