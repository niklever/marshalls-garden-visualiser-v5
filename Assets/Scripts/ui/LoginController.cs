﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LoginController : MonoBehaviour {
	InputField _email;
	InputField _password;
	GV_UI _gv_ui;

	// Use this for initialization
	void Start () {
		GameObject go = GameObject.Find ("Canvas/LoginPanel/EmailTxt");
		if (go) _email = go.GetComponent<InputField>();
		go = GameObject.Find ("Canvas/LoginPanel/PasswordTxt");
		if (go) _password = go.GetComponent<InputField>();
		string str = PlayerPrefs.GetString("email", "");
		if (str!="") _email.text = str;
		str = PlayerPrefs.GetString("password", "");
		if (str!="") _password.text = str;
		go = GameObject.Find ("Canvas");
		_gv_ui = go.GetComponent<GV_UI>();
	}

	public void LoginPressed(){
		GVWebService.LoginHandler(_email.text, _password.text);
		Analytics.ga ("User account", "Login");
	}

	public void ForgotPressed(){
		GV_UI.Animate ("forgotIn");
	}

	public void RegisterPressed(){
		GV_UI.Animate ("registerIn");
	}
}
