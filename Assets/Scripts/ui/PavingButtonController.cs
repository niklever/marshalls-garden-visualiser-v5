﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Xml;
using Ucss;

public class PavingButtonController : MonoBehaviour {
	GameObject _image;
	GameObject _loader;
	bool _loading = false;
	string _imageName;
	string _name;
	XmlNode _xml;
	RectTransform _loaderRT;

	static ProductsController _productsController;

	void Start(){
		_loader = transform.Find ("Loading").gameObject;
		_loaderRT = _loader.GetComponent<RectTransform>();
		_image = transform.Find ("Image").gameObject;
		ShowLoading(_loading);
		if (_productsController==null){
			GameObject go = GameObject.Find ("Canvas/ModalPanels/ProductsPanel");
			_productsController = go.GetComponent<ProductsController>();
		}
	}

	void Update(){
		if (_loading){
			Vector3 rot = _loaderRT.rotation.eulerAngles;
			int segment = (int)((Time.time * 12) % 12);
			rot.z = -segment * 30;
			Quaternion rotQ = Quaternion.identity;
			rotQ.eulerAngles = rot;
			_loaderRT.rotation = rotQ;
		}
	}

	public void ButtonPressed(){
		Debug.Log (string.Format ("PavingButtonController.ButtonPressed name:{0} image:{1}", _name, _imageName));
		if (_xml!=null){
			bool childNodes = (_xml.HasChildNodes && _xml.ChildNodes.Count==1);
			string name = "";
			if (childNodes) name = _xml.ChildNodes[0].Name;
			if (childNodes &&  name == "productproperties"){
				if (GV_UI.Change){
					GV_UI.Change = false;
					GV_UI.GardenScript.ChangeProduct(_xml.ChildNodes[0]);
				}else{
					GardenScript.NewProduct(_xml.ChildNodes[0]);
				}
				GV_UI.CloseModalPanel();
			}else{
				_productsController.Show (_xml);
			}
		}else{
		}
	}
		
	public void UpdateButton(ProductIcon info){
		_xml = info.Xml;
		GameObject go = transform.Find ("Text").gameObject;
		Text txt = go.GetComponent<Text>();
		txt.text = info.Name;
		_name = info.Name;
		_imageName = info.Image;
		string url = string.Format ("{0}images/{1}", GVWebService.ResourcePath, info.Image);
		Debug.Log (string.Format("PavingButtonController.UpdateButton url:{0}", url));
		UCSS.HTTP.GetTexture ( url, this.LoadCallback, this.LoadError);

		ShowLoading(true);
		_loading = true;
	}

	void LoadError(string error, string id){
		Debug.Log("PavingButtonController.Load download error:" + error);
		//GV_UI.ShowError ( _imageName + " not found");
		Image image = _image.GetComponent<Image>();
		image.sprite = Resources.Load<Sprite>("Textures/project-blank");
		ShowLoading(false);
	}
	
	void LoadCallback(Texture2D texture, string id){
		//Debug.Log("PavingButtonController.Load texture:" + id);
		Image image = _image.GetComponent<Image>();
		image.sprite = Sprite.Create (texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero);
		ShowLoading(false);
	}

	void ShowLoading(bool mode){
		if (_loader) _loader.SetActive(mode);
		if (_image) _image.SetActive (!mode);
	}
}
