﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class SettingsController : MonoBehaviour {
	EventSystem _system;
	InputField _firstNameTxt;
	InputField _lastNameTxt;
	InputField _emailTxt;
	InputField _passwordTxt;

	// Use this for initialization
	void Start () {
		_system = EventSystem.current;
	}

	public void Init(){
		GameObject go = transform.Find ("FirstNameTxt").gameObject;
		_firstNameTxt = go.GetComponent<InputField>();
		go = transform.Find ("LastTxt").gameObject;
		_lastNameTxt = go.GetComponent<InputField>();
		go = transform.Find ("EmailTxt").gameObject;
		_emailTxt = go.GetComponent<InputField>();
		go = transform.Find ("PasswordTxt").gameObject;
		_passwordTxt = go.GetComponent<InputField>();
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Tab)){
			Selectable next = _system.currentSelectedGameObject.GetComponent<Selectable>().FindSelectableOnDown();
			
			if (next != null){
				InputField inputfield = next.GetComponent<InputField>();
				if (inputfield != null)
					inputfield.OnPointerClick(new PointerEventData(_system));  //if it's an input field, also set the text caret
				
				_system.SetSelectedGameObject(next.gameObject, new BaseEventData(_system));
			}else{
				next = Selectable.allSelectables[0];
				_system.SetSelectedGameObject(next.gameObject, new BaseEventData(_system));
			}
		}
	}

	public void ClosePressed(){
		Debug.Log ("SettingsController.ClosePressed");
		GV_UI.CloseModalPanel ();
	}

	public void SavePressed(){
		Debug.Log ("SettingsController.SavePressed");
		if (GVWebService.UpdateAccount(_firstNameTxt.text, _lastNameTxt.text, _emailTxt.text, _passwordTxt.text)){
			Analytics.ga ("User account", "Change settings");
			GV_UI.CloseModalPanel ();
		}
	}
	
	public void LogoutPressed(){
		Debug.Log ("SettingsController.LogoutPressed");
		Analytics.ga ("User account", "Logout");
		GV_UI.CloseModalPanel ();
		GV_UI.GardenScript.CameraControl.Reset ();
		GV_UI.GardenScript.Clear();
		GV_UI.GardenScript.Hide();
		GV_UI.ShowMainBg (true);
		GV_UI.Animate ("loginIn");
	}

	public void UpdatePanel(string userName, string password, string firstName, string lastName){
		_firstNameTxt.text = (firstName==null || firstName=="null") ? "" : firstName;
		_lastNameTxt.text = (lastName==null || lastName=="null") ? "" : lastName;
		_emailTxt.text = userName;
		_passwordTxt.text = password;
	}

	public void Show(){
		GVWebService.GetAccount ();
		gameObject.SetActive(true);
	}
}
