﻿using SimpleJSON;
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Text;
using Random=UnityEngine.Random;

enum MouseModes{_NONE, _MOVE, _ROTATE, _MOVE_PATTERN, _ROTATE_PATTERN, _SCALE_HOUSE, _FLIP, _ADD_DOLLY, _ADD_PILLAR, _CAMERA_TILT, _CAMERA_HEIGHT, _ADD_FLAG, _ADD_ROCK};

public class GardenScript : MonoBehaviour {
	public GameObject circle_feature;
	public GameObject octant_feature;
	public GameObject square_feature;
	public GameObject bonding_feature;
	public GameObject drawshape;
	public Texture2D folderIcon;
	public Texture2D fileIcon;
	public GUISkin guiSkin;
	private string _housePath;
	private House _house;

	static GameObject _selectBox;
	static Project _project;

	public static Project Project {
		get {
			return _project;
		}
		set {
			_project = value;
		}
	}

	float _scale;
	GameObject _plot;
	Pattern _pattern;
	List<Pattern> _pavingAreas;
	static List<House> _houses;
	List<Surface> _surfaces;
	List<Feature> _features;
	List<Boundary> _boundaries;
	List<Post> _posts;
	List<Palisade> _palisades;

	static Transform _selectedT;
	static Product _selectedProduct;
	static public Product SelectedProduct {
		get {
			return _selectedProduct;
		}
	}

	static Product _storedProduct;//Used to save a products data ready for a mouse event
	Vector3 _screenPoint;
	Vector3 _offset1, _offset2;
	static List<WWWAsset> _assets;
	static private Dictionary<string, AssetBundle> _assetBundles = new Dictionary<string, AssetBundle>();
	GV_UI _gv_ui;
	FileBrowser _browser;
	static float _modifiedTime;
	static bool _modified;
	Material [] storedMaterials;
	int _clickCount = 0;
	bool _mouseDown = false;
	float _clickTime;
	Vector2 _camMousePos;
	Vector3 _initDragMousePos;
	bool _loading = false;
	public bool Loading {
		get {
			return _loading;
		}
		set {
			if (_loading && !value) {
				Debug.Log ("Switch Loading to false");
			}
			_loading = value;
		}
	}
	float _loadTime;
	int _loadIndex;

	static bool _productGUIShown = false;
	public static bool ProductGUIShown {
		get {
			return _productGUIShown;
		}
		set {
			_productGUIShown = value;
		}
	}

	public static bool Modified {
		get {
			return _modified;
		}
		set {
			_modified = value;
			if (value) _modifiedTime = Time.time;
		}
	}

	public FileBrowser Browser {
		get {
			return _browser;
		}
		set {
			_browser = value;
		}
	}

	float [] _mouseLegal = new float[2];

	public static Dictionary<string, AssetBundle> AssetBundles {
		get {
			return _assetBundles;
		}
		set {
			_assetBundles = value;
		}
	}

	GameObject _drawShape;
	int _mouseMode = (int)MouseModes._NONE;

	public int MouseMode {
		get {
			return _mouseMode;
		}
		set {
			_mouseMode = value;
		}
	}

	Vector3 _prevMousePosition;
	Vector3 _rotationCentre;
	bool _GUI = false;

	public static Transform Root{
		get{
			return GameObject.Find ("3D").transform;
		}
	}

	public static List<WWWAsset> Assets {
		get {
			return _assets;
		}
		set {
			_assets = value;
		}
	}

	SnapshotController _snapshotController;
	SnapshotController SnapshotController {
		get {
			if (_snapshotController == null) {
				GameObject go = GameObject.Find ("ThumbnailCamera");
				_snapshotController = go.GetComponent<SnapshotController> ();
			}
			return _snapshotController;
		}
	}

	static DesignController _designController;
	public static DesignController DesignController {
		set {
			_designController = value;
		}
	}

	List<Edging> _edging;
	List<Walling> _walling;
	float _newProductSeparation = 1f;
	float _newProductY;
	Camera _mainCamera;
	static CameraScript _cameraScript;

	public CameraScript CameraControl {
		get {
			return _cameraScript;
		}
	}

	//private readonly HttpConnector _httpConnector = new HttpConnector {ResponseMode = ResponseMode.WWW};
	public static JSONNode ProductsJSON;

	public void Hide(){
		Camera.main.clearFlags = CameraClearFlags.SolidColor;
		gameObject.SetActive(false);
	}

	public void Show(){
		Camera.main.clearFlags = CameraClearFlags.Skybox;
		gameObject.SetActive(true);
	}

	private Walling GetWallFromProduct(Product product){
		foreach(Walling wall in _walling){
			if (wall!=null && wall.Product == product) return wall;
		}
		return null;
	}
	
	public static void CreateNewProject(string name, int length, int width)
	{
		Analytics.ga ("Project", "Size of Project");

		if (GV_UI.DesignActive){
			//Just resizing/renaming the project
			_project.Constraints = new Vector2(length, width);
			_project.Name = name;
			_cameraScript.Constraints = _project.Constraints * 0.001f;
			GV_UI.CloseModalPanel();
			return;
		}

		_project = new Project(name, length, width);
		GV_UI.CloseModalPanel();
		GV_UI.ProjectsController.Hide ();

		GV_UI.DesignController.SetFooterState ("draw");
		GV_UI.ShowDesignPanel (true);
		GV_UI.ShowMainBg (false);

		GV_UI.GardenScript.InitGarden (_project);
	}

	public void ChangeProduct(XmlNode node){
		Debug.Log ("GardenScript.ChangeProduct " + (_selectedProduct==null));
		if (_selectedProduct==null) return;

		Dictionary<string, string> details = GetDictionaryFromXml(node);
		Debug.Log (string.Format ("GardenScript.ChangeProduct {0}", details.ToString()));

		Product product = new Product(details);

		foreach(Pattern paving in _pavingAreas){
			if (paving!=null && paving.Product == _selectedProduct){
				paving.Change(product);
				return;
			}
		}

		foreach(Feature feature in _features){
			if (feature!=null && feature.Product == _selectedProduct){
				feature.Change(product);
				return;
			}
		}

		foreach(Edging edging in _edging){
			if (edging!=null && edging.Product == _selectedProduct){
				edging.Change(product);
				return;
			}
		}

		foreach(Walling wall in _walling){
			if (wall!=null && wall.Product == _selectedProduct){
				wall.Change(product);
				return;
			}
		}

		foreach(Palisade palisade in _palisades){
			if (palisade!=null && palisade.Product == _selectedProduct){
				palisade.Change(product);
				return;
			}
		}

		foreach(Boundary boundary in _boundaries){
			if (boundary!=null && boundary.Product == _selectedProduct){
				boundary.Change(product);
				return;
			}
		}

		foreach(Surface surface in _surfaces){
			if (surface!=null && surface.Product == _selectedProduct){
				surface.Change(product);
				return;
			}
		}

		foreach (Post post in _posts) {
			if (post != null && post.Product == _selectedProduct) {
				post.Change (product);
				return;
			}
		}

		foreach(WWWAsset asset in _assets){
			if (asset!=null && asset.Product == _selectedProduct){
				asset.Change(product);
				return;
			}
		}
	}

	public static Dictionary<string, string> GetDictionaryFromXml(XmlNode node){
		Dictionary<string, string> details = new Dictionary<string, string>();
		StringBuilder str = new StringBuilder();
		XmlNode parentAttr;

		if (node!=null && node.Attributes!=null){
			bool found = false;
			int count = 0;
			do{
				foreach(XmlAttribute attr in node.Attributes){
					XmlNode parent = node.ParentNode;
					try{
						if (details.ContainsKey(attr.Name)){
							details[attr.Name] = attr.Value;
						}else{
							details.Add (attr.Name, attr.Value);
						}
						str.Append(string.Format ("{0}:{1} ", attr.Name, attr.Value));
						if (parent!=null){
							switch(attr.Name){
							case "color":
								parentAttr = parent.Attributes.GetNamedItem("name");
								if (parentAttr!=null && !details.ContainsKey("displaycolor")){
									details.Add ("displaycolor", parentAttr.Value);
									str.Append(string.Format ("{0}:{1} ", "displaycolor", parentAttr.Value));
								}
								parentAttr = parent.Attributes.GetNamedItem("image");
								if (parentAttr!=null && !details.ContainsKey("colorswatch")){
									details.Add ("colorswatch", parentAttr.Value);
									str.Append(string.Format ("{0}:{1} ", "colorswatch", parentAttr.Value));
								}
								break;
							case "pattern":
								parentAttr = parent.Attributes.GetNamedItem("name");
								if (parentAttr!=null && !details.ContainsKey("displaypattern")){
									details.Add ("displaypattern", parentAttr.Value);
									str.Append(string.Format ("{0}:{1} ", "displaypattern", parentAttr.Value));
								}
								parentAttr = parent.Attributes.GetNamedItem("image");
								if (parentAttr!=null && !details.ContainsKey("patternswatch")){
									details.Add ("patternswatch", parentAttr.Value);
									str.Append(string.Format ("{0}:{1} ", "pattenswatch", parentAttr.Value));
								}
								break;
							}
						}
					}catch(ArgumentException e){
						Debug.Log (string.Format("GardenScript.GetDictionaryFromXml {0}:{1}", attr.Name, attr.Value));
					}
				}
				node = node.ParentNode.ParentNode;
				while(node!=null && node.Name!="option") node = node.ParentNode;
				found = false;
				count++;
				if (node!=null && node.HasChildNodes){
					for(int i=0; i<node.ChildNodes.Count; i++){
						if (node.ChildNodes[i].Name=="productproperties"){
							found = true;
							node = node.ChildNodes[i];
							break;
						}
					}
				}
				if (found==false){
					while(node!=null && node.Name!="option") node = node.ParentNode;
				}
			}while(node!=null && count<20);
			//Debug.Log ("GardenScript.GetDictionaryFromXml " + str);
		}

		return details;
	}

	public static void NewProduct(XmlNode node){
		Dictionary<string, string> details = GetDictionaryFromXml(node);
		Debug.Log (string.Format ("GardenScript.NewProduct {0}", details.ToString()));

		Product product = new Product(details);
		Analytics.ga ("Project", "Add/modify product", string.Format("{0} {1}", product.Type, product.DisplayName));
		string name;
		WWWAsset asset;
		GardenScript gardenScript = GameObject.Find ("3D").GetComponent<GardenScript> ();

		switch(product.Type){
		case "paving":
		case "driveway":
			gardenScript.DrawShape ("paving", product);
			GV_UI.DesignController.ShowSelectedBtn (FooterButtons.PAVING);
			break;
		case "path":
			gardenScript.DrawShape ("path", product);
			GV_UI.DesignController.ShowSelectedBtn (FooterButtons.PATHS);
			break;
		case "walling":
		case "palisades":
			gardenScript.DrawShape ("wall", product);
			GV_UI.DesignController.ShowSelectedBtn (FooterButtons.WALLING);
			break;
		case "boundary":
			gardenScript.DrawShape ("fences", product);
			GV_UI.DesignController.ShowSelectedBtn (FooterButtons.FENCES);
			break;
		case "edging":
		case "edging_supported":
		case "kerb":
			gardenScript.DrawShape ("edging", product);
			GV_UI.DesignController.ShowSelectedBtn (FooterButtons.PATHS);
			break;
		case "aggregate":
		case "rock":
		case "surface":
			gardenScript.DrawShape ("aggregates", product);
			GV_UI.DesignController.ShowSelectedBtn (FooterButtons.AGGREGATES);
			break;
		case "circle":
		case "octant":
			Feature feature = new Feature (gardenScript, product);
			if (gardenScript._features != null)
				gardenScript._features.Add (feature);
			GV_UI.DesignController.ShowSelectedBtn (FooterButtons.FEATURES);
			break;
		case "accessory":
			name = (product.Color == "") ? (product.Type + "_" + product.ProductName) : (product.Type + "_" + product.ProductName + "_" + product.Color);
			asset = new WWWAsset (product, name);
			if (_assets != null)
				_assets.Add (asset);
			GV_UI.DesignController.ShowSelectedBtn (FooterButtons.ACCESSORIES);
			break;
		case "summerhouse":
		case "greenhouse":
			name = (product.Color=="") ? (product.Type + "_" + product.ProductName) : (product.Type + "_" + product.ProductName + "_" + product.Color);
			asset = new WWWAsset(product, name );
			if (_assets!=null) _assets.Add (asset );
			GV_UI.DesignController.ShowSelectedBtn (FooterButtons.BUILDINGS);
			break;
		case "vegetation":
			name = (product.Color=="") ? (product.Type + "_" + product.ProductName) : (product.Type + "_" + product.ProductName + "_" + product.Color);
			asset = new WWWAsset(product, name );
			if (_assets!=null) _assets.Add (asset );
			GV_UI.DesignController.ShowSelectedBtn (FooterButtons.TREES);
			break;
		default:
			GV_UI.ShowAlert("Warning", string.Format("Type {0} not supported", product.Type));
			break;
		}

		if (product != null){
			_project.Add(product);
			Modified = true;
		}
	}

	public void UpdateProduct(string dataXML){
		Debug.Log ("GardenScript.UpdateProduct " + (_selectedProduct==null));
		if (_selectedProduct==null) return;

		dataXML = dataXML.Replace (" & ", " &#038; ");

		XmlDocument doc = new XmlDocument();
		doc.LoadXml(dataXML + " name=\"product_xxx\" positionx=\"0\" positiony=\"0\" positionz=\"0\" " +
		            "rotation=\"0\" patternpositionx=\"0\" patternpositionz=\"0\" patternrotation=\"0\"></product>");
		Product product = new Product(doc.FirstChild);

		foreach(Pattern paving in _pavingAreas){
			if (paving!=null && paving.Product == _selectedProduct){
				paving.Change(product);
				return;
			}
		}
		
		foreach(Feature feature in _features){
			if (feature!=null && feature.Product == _selectedProduct){
				feature.Change(product);
				return;
			}
		}

		foreach(Edging edging in _edging){
			if (edging!=null && edging.Product == _selectedProduct){
				edging.Change(product);
				return;
			}
		}

		foreach(Walling wall in _walling){
			if (wall!=null && wall.Product == _selectedProduct){
				wall.Change(product);
				return;
			}
		}

		foreach(Palisade palisade in _palisades){
			if (palisade!=null && palisade.Product == _selectedProduct){
				palisade.Change(product);
				return;
			}
		}

		foreach(Boundary boundary in _boundaries){
			if (boundary!=null && boundary.Product == _selectedProduct){
				boundary.Change(product);
				return;
			}
		}

		foreach(Surface surface in _surfaces){
			if (surface!=null && surface.Product == _selectedProduct){
				surface.Change(product);
				return;
			}
		}

		foreach(WWWAsset asset in _assets){
			if (asset!=null && asset.Product == _selectedProduct){
				asset.Change(product);
				return;
			}
		}
	}

	public static void AdjustHeight(float delta){
		Debug.Log ("GardenScript.AdjustHeight delta:" + delta);
		if (_selectedT!=null){
			bool translate = true;
			if (_selectedProduct!=null && (_selectedProduct.Type=="walling"||_selectedProduct.Type=="edging_supported"||_selectedProduct.Type=="post"||_selectedProduct.Type=="palisades")) translate = false;
			if (translate){
				_selectedT.Translate (0, delta, 0);
				if (_selectedProduct.Type=="house"){
					House house = GetHouseFromSelectedProduct();
					//Debug.Log ("GardenScript.AdjustHeight y<0 house=" + house);
					if (house!=null) house.SetHeight(delta);
				}else{
					Vector3 pos = _selectedT.position;

					if (pos.y<0){
						pos.y = 0;
						_selectedT.position = pos;
					}
					if (pos.y>1){
						pos.y = 1;
						_selectedT.position = pos;
					}
				}
			}
		}else{
			_cameraScript.AdjustHeight (delta);
		}
		if (_selectedProduct!=null && _selectedT!=null && _selectedProduct.Type!="house") _selectedProduct.Position = _selectedT.position * 1000;
		Modified = true;
	}

	public static House GetHouseFromSelectedProduct(){
		if (_houses==null) return null;
		foreach(House house in _houses){
			if (house.Product==_selectedProduct) return house;
		}
		return null;
	}

	public House GetHouseFromProduct(Product product){
		if (_houses==null) return null;
		foreach(House house in _houses){
			if (house.Product==product) return house;
		}
		return null;
	}

	public void InitGarden(Project project){
		//Debug.Log ("GardenScript InitGarden called");
		if (_gv_ui == null) {
			GameObject guiGO = GameObject.Find ("Canvas");
			if (guiGO == null) {
				Debug.Log ("GardenScript.InitGarden GUIScript not found");
			} else {
				_gv_ui = guiGO.GetComponent<GV_UI> ();
				if (_gv_ui == null) {
					Debug.Log ("GardenScript.InitGarden GV_UI not found");
				}
			}
		}

		if (_cameraScript == null) {
			GameObject cam = GameObject.Find ("Main Camera");
			_mainCamera = cam.GetComponent<Camera> ();
			if (cam != null)
				_cameraScript = (CameraScript)cam.GetComponent<CameraScript> ();
		}

		Loading = true;
		_loadTime = Time.time;
		_project = project;
		_scale = 0.001f; //millimetres to metres

		if (_cameraScript != null) {
			_cameraScript.Constraints = _project.Constraints * _scale;
			_cameraScript.SetCamera (1, true);//Overhead
			if (SnapshotController)
				_snapshotController.Thumbnail = _cameraScript.GetOverheadCameraAsSnapshot ();
		}

		Show ();
		Clear ();

		Pattern.ResourcePath = GVWebService.ResourcePath;
		House.ResourcePath = GVWebService.ResourcePath;
		Surface.ResourcePath = GVWebService.ResourcePath;
		Feature.ResourcePath = GVWebService.ResourcePath;

		_newProductY = _project.MaxProductY * 0.001f + _newProductSeparation;
		_modified = false;

		_loadIndex = 0;//Initialise the product index to load

		if (_gv_ui!=null) _gv_ui.ShowLoading (true);

		LoadNextProduct();
	}

	bool isSingleFlag(Product product){
		if (product.Type == "paving" && product.Pattern.StartsWith ("flag_")) {
			return true;
		}
		return false;
	}

	void CheckProducts(){
		int removeIndex;
		do {
			removeIndex = -1;
			int i=0;
			foreach (Product product in _project.Products) {
				switch (product.Type) {
				case "circle":
				case "octant":
				case "post":
				case "accessory":
				case "rock":
				case "summerhouse":
				case "greenhouse":
				case "vegetation":
					break;
				case "path":
				case "palisades":
				case "boundary":
				case "walling":
				case "surface":
				case "aggregate":
				case "kerb":
				case "edging":
				case "edging_supported":
				case "plot":
				case "paving":
				case "driveway":
				case "house":
					if (product.SourceVertices.Count==0 && !isSingleFlag(product)){
						removeIndex = i;
						Modified = true;
					}
					break;
				default:
					Debug.Log (string.Format ("GardenScript.LoadProduct {0} product type not found", product.Type));
					Modified = true;
					removeIndex = i;
					break;
				}
				i++;
				if (removeIndex!=-1)
					break;
			}
			if (removeIndex!=-1) _project.Products.RemoveAt (removeIndex);
		} while(removeIndex!=-1);
	}

	public void LoadNextProduct(){
		if (!_loading)
			return;
		if (_project == null)
			return;
		if (_loadIndex >= _project.Products.Count) {
			//if (_gv_ui!=null) _gv_ui.ShowLoading (false);
			//Loading = false;
			return;
		}
		StartCoroutine ("LoadProduct");
	}

	IEnumerator LoadProduct(){
		string name;
		WWWAsset asset;

		if (_loadIndex < _project.Products.Count) {
			Product product = _project.Products [_loadIndex];
			_loadIndex++;
			if (product == null)
				LoadNextProduct ();
			Debug.Log ("GardenScript.LoadProduct product.Type=" + product.Type);
			switch (product.Type) {
			case "plot":
				CreatePlot (product);
				if (_plot != null)
					_plot.transform.parent = gameObject.transform;
				LoadNextProduct();
				break;
			case "paving":
			case "driveway":
			case "path":
				Pattern pattern = new Pattern (_project, product);
				if (_pavingAreas != null)
					_pavingAreas.Add (pattern);
				break;
			case "house":
				House house = new House (product);
				if (_houses != null)
					_houses.Add (house);
				break;
			case "circle":
			case "octant":
				Feature feature = new Feature (this, product);
				if (_features != null)
					_features.Add (feature);
				break;
			case "kerb":
			case "edging":
			case "edging_supported":
				Edging edging = new Edging (product);
				if (_edging != null)
					_edging.Add (edging);
				break;
			case "post":
				name = (product.Color == "") ? (product.Type + "_" + product.ProductName) : (product.Type + "_" + product.ProductName);
				Post post = new Post (product);
				_project.Products [_loadIndex-1] = post.Product;
				if (_posts != null)
					_posts.Add (post);
				break;
			case "accessory":
			case "rock":
			case "summerhouse":
			case "greenhouse":
			case "vegetation":
				name = (product.Color == "") ? (product.Type + "_" + product.ProductName) : (product.Type + "_" + product.ProductName + "_" + product.Color);
				asset = new WWWAsset (product, name);
				if (_assets != null)
					_assets.Add (asset);
				break;
			case "surface":
			case "aggregate":
				Surface surface = new Surface (product);
				if (_surfaces != null)
					_surfaces.Add (surface);
				break;
			case "walling":
				Walling wall = new Walling (product);
				if (_walling != null)
					_walling.Add (wall);
				break;
			case "palisades":
				Palisade palisade = new Palisade (product);
				if (_palisades != null)
					_palisades.Add (palisade);
				break;
			case "boundary":
				Boundary boundary = new Boundary (product);
				if (_boundaries != null)
					_boundaries.Add (boundary);
				break;
			default:
				Debug.Log (string.Format ("GardenScript.LoadProduct {0} product type not found", product.Type));
				LoadNextProduct ();
				break;
			}
		}else{
			Debug.Log ("GardenScript.LoadProject load complete");
			Loading = false;
		}
		return null;
	}

	public static JSONNode GetProductJSON(string name){
		if (ProductsJSON!=null){
			Debug.Log ("GardenScript.GetProductJSON ProductsJSON count:" + ProductsJSON.Count.ToString () + " looking for " + name);
			for(int i=0; i<ProductsJSON.Count; i++){
				if (((string)ProductsJSON[i]["model"]).Equals(name)) return ProductsJSON[i];
			}
		}else{
			Debug.Log ("GardenScript.GetProductJSON ProductsJSON null");
		}
		Debug.Log ("GardenScript.GetProductJSON name not found");
		return null;
	}

	public void AddChild(GameObject go){
		go.transform.parent = gameObject.transform;
	}
	
	public void Clear(){
		if (_plot != null) Destroy(_plot);
		
		if (_pavingAreas!=null){
			foreach(Pattern pattern in _pavingAreas){
				if (pattern.PatternTexture!= null){
					Destroy(pattern.PatternTexture);
					pattern.PatternTexture = null;
				}
				if (pattern.PatternGO!=null){
					Destroy(pattern.PatternGO);
					pattern.PatternGO = null;
				}
			}
		}
		_pavingAreas = new List<Pattern>();
		
		if (_houses!=null){
			foreach(House house in _houses){
				if (house.HouseTexture!= null && !house.Internal){
					Destroy(house.HouseTexture);
					house.HouseTexture = null;
				}
				if (house.HouseGO!=null){
					Destroy(house.HouseGO);
					house.HouseGO = null;
				}
			}
		}
		_houses = new List<House>();

		if (_surfaces!=null){
			foreach(Surface surface in _surfaces){
				if (surface.SurfaceTexture!= null && !surface.Internal){
					Destroy(surface.SurfaceTexture);
					surface.SurfaceTexture = null;
				}
				if (surface.SurfaceGO!=null){
					Destroy(surface.SurfaceGO);
					surface.SurfaceGO = null;
				}
			}
		}
		_surfaces = new List<Surface>();

		if (_features!=null){
			foreach(Feature feature in _features){
				if (feature.FeatureTexture!= null){
					Destroy(feature.FeatureTexture);
					feature.FeatureTexture = null;
				}
				if (feature.FeatureGO!=null){
					Destroy(feature.FeatureGO);
					feature.FeatureGO = null;
				}
			}
		}
		_features = new List<Feature>();

		if (_assets!=null){
			foreach(WWWAsset asset in _assets){
				if (asset.AssetGO!=null){
					Destroy(asset.AssetGO);
					asset.AssetGO = null;
				}
			}
		}
		_assets = new List<WWWAsset>();

		//Unload all assets in the bundle
		foreach(string key in _assetBundles.Keys){
			AssetBundle asset = _assetBundles [key];
			asset.Unload (true);
		}
		_assetBundles.Clear ();

		if (_edging!=null){
			foreach(Edging edging in _edging){
				if (edging.EdgingGOs!=null){
					Destroy(edging.EdgingGO);
					foreach(GameObject go in edging.EdgingGOs){
						Destroy(go);
						//go = null;
					}
				}
			}
		}
		_edging = new List<Edging>();

		if (_walling!=null){
			foreach(Walling walling in _walling){
				if (walling.WallingGOs!=null){
					walling.Clear ();
					Destroy(walling.WallingGO);
					foreach(GameObject go in walling.WallingGOs){
						Destroy(go);
						//go = null;
					}
				}
			}
		}
		_walling = new List<Walling>();

		if (_palisades!=null){
			foreach(Palisade palisade in _palisades){
				if (palisade.PalisadeGOs!=null){
					palisade.Clear ();
					Destroy(palisade.PalisadeGO);
					foreach(GameObject go in palisade.PalisadeGOs){
						Destroy(go);
						//go = null;
					}
				}
			}
		}
		_palisades = new List<Palisade>();

		if (_boundaries!=null){
			foreach(Boundary boundary in _boundaries){
				boundary.Clear();
			}
		}
		_boundaries = new List<Boundary>();

		if (_posts!=null){
			foreach(Post post in _posts){
				if (post.PostGO!=null){
					Destroy(post.PostGO);
				}
				if (post.CopingGO!=null){
					Destroy(post.CopingGO);
				}
			}
		}
		_posts = new List<Post>();

		System.GC.Collect();
	}

	// Use this for initialization
	void Start () {
		Caching.CleanCache();
		_newProductY = _newProductSeparation;
		GameObject cam = GameObject.Find ("Main Camera");
		if (cam!=null) _cameraScript = (CameraScript)cam.GetComponent<CameraScript>();
		_selectBox = GameObject.Find ("select_box");
		for (int i = 0; i < _selectBox.transform.childCount; i++) {
			Transform t = _selectBox.transform.GetChild (i);
			t.localScale = new Vector3 (100, 100, 100);
		}
		//_selectBox.SetActive (false);
		ClearSelectBox();
		GameObject gui = GameObject.Find ("Canvas");
		if (gui!=null){
			_gv_ui = gui.GetComponent<GV_UI>();
			if (_gv_ui!=null){
				_mouseLegal [0] = 100 * _gv_ui.Scale;
				_mouseLegal[1] = 700 * _gv_ui.Scale;
			}else{
				_mouseLegal[0] = 100;
				_mouseLegal[1] = 700;
			}
		}
		GV_UI.GardenScript = this;
		GVWebService.init ();
		Hide();
	}

	public void DrawShape(string type){
		if (_drawShape!=null) Destroy(_drawShape);
		_drawShape = null;

		if (type.Equals ( "paving")){
			_drawShape = (GameObject)Instantiate(drawshape, Vector3.zero, Quaternion.identity);
			DrawShape script = _drawShape.GetComponent<DrawShape>();
			if (script!=null){
				script.GardenScript = this;
				XmlDocument doc = new XmlDocument();
				doc.LoadXml("<xml><product type=\"paving\" product=\"saxon\" color=\"n\" pattern=\"pattern_0560\" displayname=\"Saxon\" " +
				            "displaycolor=\"Natural (N)\" displaypattern=\"LP-0560\" colorswatch=\"swatch/paving_saxon_n.jpg\" " +
				            "patternswatch=\"swatch/pattern_0560.png\" name=\"product_" + _project.Products.Count + "\" /></xml>");
				Product product = new Product(doc.FirstChild.FirstChild);
				script.Product = product;
			}
		}else if (type.Equals ( "wall")){
			_drawShape = (GameObject)Instantiate(drawshape, Vector3.zero, Quaternion.identity);
			DrawShape script = _drawShape.GetComponent<DrawShape>();
			script.AngleConstraint = 45;
			if (script!=null){
				script.GardenScript = this;
				XmlDocument doc = new XmlDocument();
				doc.LoadXml("<xml><product type=\"walling\" product=\"national_trust\" color=\"pl\" size=\"975\" "+
				            "coping=\"peaked\" copingcolor=\"pl\" displayname=\"Coach House Walling\" displaycolor=\"Heathland (HL)\" " + 
				            "colorswatch=\"swatch/walling_national_trust_pl.jpg\" name=\"product_" + _project.Products.Count + "\"/></xml>");
				Product product = new Product(doc.FirstChild.FirstChild);
				script.Product = product;
			}
		}else if (type.Equals ( "aggregates")){
			_drawShape = (GameObject)Instantiate(drawshape, Vector3.zero, Quaternion.identity);
			DrawShape script = _drawShape.GetComponent<DrawShape>();
			if (script!=null){
				script.GardenScript = this;
				XmlDocument doc = new XmlDocument();
				doc.LoadXml("<xml><product type=\"aggregate\" product=\"slate_chip\" color=\"plum_40\" displayname=\"Slate Chip\" " +
				            "displaycolor=\"Plum 40mm\" colorswatch=\"swatch/aggregate_slate_chip_plum_40.jpg\"  name=\"product_" + 
				            _project.Products.Count + "\" /></xml>");
				Product product = new Product(doc.FirstChild.FirstChild);
				script.Product = product;
			}
		}else if (type.Equals ( "path")){
			_drawShape = (GameObject)Instantiate(drawshape, Vector3.zero, Quaternion.identity);
			DrawShape script = _drawShape.GetComponent<DrawShape>();
			if (script!=null){
				script.GardenScript = this;
				XmlDocument doc = new XmlDocument();
				doc.LoadXml("<xml><product type=\"path\" product=\"tegula_deco\" color=\"t\" pattern=\"pattern_0069\" size=\"678\" " +
				            "displayname=\"Drivesett Deco\" displaycolor=\"Terracotta (T)\" " + 
				            "displaypattern=\"Stretcher Bond\" colorswatch=\"swatch/path_tegula_deco_t.jpg\" " + 
				            "patternswatch=\"swatch/pattern_1502.png\" name=\"product_" + _project.Products.Count + "\" /></xml>");
				Product product = new Product(doc.FirstChild.FirstChild);
				script.Product = product;
			}
		}else if (type.Equals ( "edging")){
			_drawShape = (GameObject)Instantiate(drawshape, Vector3.zero, Quaternion.identity);
			DrawShape script = _drawShape.GetComponent<DrawShape>();
			if (script!=null){
				script.GardenScript = this;
				XmlDocument doc = new XmlDocument();
				doc.LoadXml("<product type=\"edging\" product=\"victorian_rope_top\" color=\"rb\" pattern=\"\" size=\"\" aspectratio=\"\" " +
				            "coping=\"\" copingcolor=\"\" displayname=\"Victorian Rope Top\" displaycolor=\"Red/Black (RB)\" " +
				            "displaypattern=\"\" colorswatch=\"thumb/edging_victorian_rope_top_rb.jpg\" patternswatch=\"\" " +
				            "name=\"product_" + _project.Products.Count + "\" positionx=\"0\" positiony=\"0\" positionz=\"0\" rotation=\"0\" " +
				            "patternpositionx=\"0\" patternpositionz=\"0\" patternrotation=\"0\"></product>");
				Product product = new Product(doc.FirstChild.FirstChild);
				script.Product = product;
			}
		}

		if (_drawShape!=null){
			_cameraScript.SetCamera (1, true);
			GV_UI.DesignController.ShowCameraControls (false);
			GV_UI.ShowAlert("HELP", "Click to draw the corners of your product. Click the first corner or double click to finish drawing");
		}
	}

	public void DrawShape(string type, Product product){
		bool draw = false;

		if (product.Type.Equals ("rock")){
			_mouseMode = (int)MouseModes._ADD_ROCK;
			_storedProduct = product;
			return;
		}else if (product.Type.Equals ("palisades")){
			type = "palisades";
		}

		switch(type){
		case "paving":
			if (product.Pattern.StartsWith ("flag_")) {
				Debug.Log ("GardenScript.DrawShape single flag");
				_storedProduct = product;
				_mouseMode = (int)MouseModes._ADD_FLAG;
			} else {
				draw = true;
			}
			break;
		case "wall":
		case "palisades":
		case "aggregates":
		case "path":
		case "edging":
		case "fences":
		case "boundary":
			draw = true;
			break;
		}

		if (draw){
			_drawShape = (GameObject)Instantiate (drawshape, Vector3.zero, Quaternion.identity);
			DrawShape script = _drawShape.GetComponent<DrawShape> ();
			if (script != null) {
				script.GardenScript = this;
				script.Product = product;
				if (type == "wall") {
					script.AngleConstraint = 45;
				} else if (type == "edging" && product.ProductName == "woodstone_posts_sleepers") {
					script.AngleConstraint = 90;
				}
			}
			_cameraScript.SetCamera (1, true);
			GV_UI.DesignController.ShowCameraControls(false);
			GV_UI.ShowAlert("HELP", "Click to draw the corners of your product. Click the first corner or double click to finish drawing");
		}
	}

	public void ShowCameraControls(bool mode){
		GV_UI.DesignController.ShowCameraControls(mode);
	}
		
	//Nam.Nguyen Adding new function to revices data from xml 
	public void AddProduct(string type, string dataXML){
		dataXML = dataXML.Replace (" & ", " &#038; ");
		Debug.Log("AddProduct type:"+ type);
		Product product = null;
		
		if (type.Equals ("feature")){
			XmlDocument doc = new XmlDocument();
//			doc.LoadXml("<product type=\"octant\" product=\"saxon_2_ring\" color=\"b\" pattern=\"\" size=\"\" aspectratio=\"\" " +
//			            "coping=\"\" copingcolor=\"\" displayname=\"Saxon 2-ring Octant\" displaycolor=\"Buff (B)\" " + 
//			            "displaypattern=\"\" colorswatch=\"\" patternswatch=\"\" name=\"product_" + _project.Products.Count + 
//			            "\" positionx=\"0\" positiony=\"0\" positionz=\"0\" rotation=\"0\" patternpositionx=\"0\" patternpositionz=\"0\" " +
//			            "patternrotation=\"0\"></product>");
			doc.LoadXml(dataXML + " name=\"product_" + _project.Products.Count + "\" positionx=\"0\" positiony=\"0\" positionz=\"0\" " +
			            "rotation=\"0\" patternpositionx=\"0\" patternpositionz=\"0\" patternrotation=\"0\"></product>");
			Debug.Log(dataXML + " name=\"product_" + _project.Products.Count + "\" positionx=\"0\" positiony=\"0\" positionz=\"0\" " +
			            "rotation=\"0\" patternpositionx=\"0\" patternpositionz=\"0\" patternrotation=\"0\"></product>");
			product = new Product(doc.FirstChild);
			Feature feature = new Feature(this, product);
			if (_features!=null) _features.Add (feature);
		}else if (type.Equals ("accessory")){
			XmlDocument doc = new XmlDocument();
//			doc.LoadXml("<product type=\"accessory\" product=\"step_haworth_moor_sawn\" color=\"abm\" pattern=\"\" size=\"1850\" " +
//			            "aspectratio=\"\" coping=\"\" copingcolor=\"\" displayname=\"Fairstone Step\" " + 
//			            "displaycolor=\"Antique Silver Multi (ASM)\" displaypattern=\"\" " +
//			            "colorswatch=\"swatch/paving_haworth_moor_sawn_asm.jpg\" patternswatch=\"\" name=\"product_" + 
//			            _project.Products.Count + "\" positionx=\"0\" positiony=\"0\" positionz=\"0\" rotation=\"0\" " +
//			            "patternpositionx=\"0\" patternpositionz=\"0\" patternrotation=\"0\"></product>");
			doc.LoadXml(dataXML + " name=\"product_" + _project.Products.Count + "\" positionx=\"0\" positiony=\"0\" positionz=\"0\" " +
			            "rotation=\"0\" patternpositionx=\"0\" patternpositionz=\"0\" patternrotation=\"0\"></product>");
			Debug.Log(dataXML + " name=\"product_" + _project.Products.Count + "\" positionx=\"0\" positiony=\"0\" positionz=\"0\" " +
			            "rotation=\"0\" patternpositionx=\"0\" patternpositionz=\"0\" patternrotation=\"0\"></product>");
			product = new Product(doc.FirstChild);
			string name = (product.Color=="") ? (product.Type + "_" + product.ProductName) : (product.Type + "_" + product.ProductName + "_" + product.Color);
			WWWAsset asset = new WWWAsset(product, name );
			if (_assets!=null) _assets.Add (asset );
		}else if (type.Equals ("building")){
			XmlDocument doc = new XmlDocument();
//			doc.LoadXml("<product type=\"summerhouse\" product=\"alton_stratford\" color=\"\" pattern=\"\" size=\"\" aspectratio=\"\" " +
//			            "coping=\"\" copingcolor=\"\" displayname=\"Summerhouse\" displaycolor=\"\" displaypattern=\"\" " +
//			            "colorswatch=\"\" patternswatch=\"\" name=\"product_" + _project.Products.Count + "\" positionx=\"0\" positiony=\"0\" positionz=\"0\" " +
//			            "rotation=\"0\" patternpositionx=\"0\" patternpositionz=\"0\" patternrotation=\"0\"></product>");
			doc.LoadXml(dataXML + " name=\"product_" + _project.Products.Count + "\" positionx=\"0\" positiony=\"0\" positionz=\"0\" " +
			            "rotation=\"0\" patternpositionx=\"0\" patternpositionz=\"0\" patternrotation=\"0\"></product>");
			Debug.Log(dataXML + " name=\"product_" + _project.Products.Count + "\" positionx=\"0\" positiony=\"0\" positionz=\"0\" " +
			            "rotation=\"0\" patternpositionx=\"0\" patternpositionz=\"0\" patternrotation=\"0\"></product>");
			product = new Product(doc.FirstChild);
			string name = (product.Color=="") ? (product.Type + "_" + product.ProductName) : (product.Type + "_" + product.ProductName + "_" + product.Color);
			WWWAsset asset = new WWWAsset(product, name );
			if (_assets!=null) _assets.Add (asset );
		}else if (type.Equals ("tree")){
			XmlDocument doc = new XmlDocument();
			//int idx = Random.Range(2, 4);
//			doc.LoadXml("<product type=\"vegetation\" product=\"plant_" + idx + "\" color=\"\" pattern=\"\" size=\"\" aspectratio=\"\" " +
//			            "coping=\"\" copingcolor=\"\" displayname=\"Plant " + idx + "\" displaycolor=\"\" displaypattern=\"\" " +
//			            "colorswatch=\"thumb/vegetation_plant" + idx + ".jpg\" patternswatch=\"\" name=\"product_" + _project.Products.Count + 
//			            "\" positionx=\"0\" positiony=\"0\" positionz=\"0\" rotation=\"0\" patternpositionx=\"0\" patternpositionz=\"0\" " +
//			            "patternrotation=\"0\"></product>");
			doc.LoadXml(dataXML + " name=\"product_" + _project.Products.Count + "\" positionx=\"0\" positiony=\"0\" positionz=\"0\" " +
			            "rotation=\"0\" patternpositionx=\"0\" patternpositionz=\"0\" patternrotation=\"0\"></product>");
			Debug.Log(dataXML + " name=\"product_" + _project.Products.Count + "\" positionx=\"0\" positiony=\"0\" positionz=\"0\" " +
			            "rotation=\"0\" patternpositionx=\"0\" patternpositionz=\"0\" patternrotation=\"0\"></product>");

			product = new Product(doc.FirstChild);
			string name = (product.Color=="") ? (product.Type + "_" + product.ProductName) : (product.Type + "_" + product.ProductName + "_" + product.Color);
			WWWAsset asset = new WWWAsset(product, name );
			if (_assets!=null) _assets.Add (asset );
		}else if (type.Equals ("post")){
			string xml = dataXML +  " name=\"product_" + _project.Products.Count + "\"></product>";
			Debug.Log("GardenScript.AddProduct " + xml);
			XmlDocument doc = new XmlDocument();
			doc.LoadXml(xml);

			product = new Product(doc.FirstChild);
			Post post = new Post(product);
			if (_posts!=null) _posts.Add (post );
		}

		if (product != null){
			_project.Add(product);

			Modified = true;
		}
	}
	void OnGUI(){
		if (_GUI){
			if (GUI.Button(new Rect(10, 40, 150, 60), "Draw Paving")){
				_drawShape = (GameObject)Instantiate(drawshape, Vector3.zero, Quaternion.identity);
				DrawShape script = _drawShape.GetComponent<DrawShape>();
				if (script!=null){
					script.GardenScript = this;
					XmlDocument doc = new XmlDocument();
					doc.LoadXml("<xml><product type=\"paving\" product=\"saxon\" color=\"n\" pattern=\"pattern_0560\" displayname=\"Saxon\" " +
						"displaycolor=\"Natural (N)\" displaypattern=\"LP-0560\" colorswatch=\"swatch/paving_saxon_n.jpg\" " +
					    "patternswatch=\"swatch/pattern_0560.png\" name=\"product_3\" /></xml>");
					Product product = new Product(doc.FirstChild.FirstChild);
					script.Product = product;
				}
			}else if (GUI.Button(new Rect(170, 40, 150, 60), "Draw Wall")){
				_drawShape = (GameObject)Instantiate(drawshape, Vector3.zero, Quaternion.identity);
				DrawShape script = _drawShape.GetComponent<DrawShape>();
				script.AngleConstraint = 45;
				if (script!=null){
					script.GardenScript = this;
					XmlDocument doc = new XmlDocument();
					doc.LoadXml("<xml><product type=\"walling\" product=\"national_trust\" color=\"pl\" size=\"975\" "+
					            "coping=\"peaked\" copingcolor=\"pl\" displayname=\"Coach House Walling\" displaycolor=\"Heathland (HL)\" " + 
					            "colorswatch=\"swatch/walling_national_trust_pl.jpg\" name=\"product_018\"/></xml>");
					Product product = new Product(doc.FirstChild.FirstChild);
					script.Product = product;
				}
			}else if (GUI.Button(new Rect(330, 40, 150, 60), "Draw Path")){
				_drawShape = (GameObject)Instantiate(drawshape, Vector3.zero, Quaternion.identity);
				DrawShape script = _drawShape.GetComponent<DrawShape>();
				if (script!=null){
					script.GardenScript = this;
					XmlDocument doc = new XmlDocument();
					doc.LoadXml("<xml><product type=\"path\" product=\"tegula_deco\" color=\"t\" pattern=\"pattern_0069\" size=\"678\" " +
					            "displayname=\"Drivesett Deco\" displaycolor=\"Terracotta (T)\" " + 
					            "displaypattern=\"Stretcher Bond\" colorswatch=\"swatch/path_tegula_deco_t.jpg\" " + 
					            "patternswatch=\"swatch/pattern_1502.png\" name=\"product_6\" /></xml>");
					Product product = new Product(doc.FirstChild.FirstChild);
					script.Product = product;
				}
			}
			string [] mouseModes = { "move", "rotate", "pattern move", "pattern rotate" };
			_mouseMode = GUILayout.SelectionGrid(_mouseMode, mouseModes, 4);
		}
		if (_browser!=null){
			GUI.skin = guiSkin; 
			GUI.depth = 0;
			_browser.OnGUI();
		}
	}

	bool IsLoadingComplete(){
		float elapsedTime = Time.time - _loadTime;
		if (elapsedTime > 20) return true;

		//Debug.Log ("GardenScript.IsLoadingComplete patterns");
		if (_pavingAreas!=null && _pavingAreas.Count>0){
			foreach(Pattern pattern in _pavingAreas){
				if  (pattern.Loading) return false;
			}
		}
		//Debug.Log ("GardenScript.IsLoadingComplete houses");
		foreach(House house in _houses){
			if  (house.Loading) return false;
		}
		//Debug.Log ("GardenScript.IsLoadingComplete surfaces");
		foreach(Surface surface in _surfaces){
			if  (surface.Loading) return false;
		}
		//Debug.Log ("GardenScript.IsLoadingComplete features");
		foreach(Feature feature in _features){
			if  (feature.Loading) return false;
		}
		//Debug.Log ("GardenScript.IsLoadingComplete boundaries");
		foreach(Boundary boundary in _boundaries){
			if  (boundary.Loading) return false;
		}

		//Debug.Log ("GardenScript.IsLoadingComplete posts");
		foreach(Post post in _posts){
			if  (post.Loading){
				//Debug.Log ("GardenScript.IsLoadingComplete post:" + post.Product + " elapsedTime:" + elapsedTime);
				return false;
			}
		}
		//Debug.Log ("GardenScript.IsLoadingComplete assets");
		foreach(WWWAsset asset in _assets){
			if  (asset.Loading) return false;
		}

		CheckProducts ();

		return true;
	}

	Vector3 MouseToWorld(bool yZero){
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		// create a plane at 0,0,0 whose normal points to +Y:
		Plane hPlane = new Plane(Vector3.up, Vector3.zero);
		// Plane.Raycast stores the distance from ray.origin to the hit point in this variable:
		float distance = 0; 
		// if the ray hits the plane...
		Vector3 pos = new Vector3(0,0,0);

		if (hPlane.Raycast(ray, out distance)){
			// get the hit point:
			pos = ray.GetPoint(distance);
			if (yZero) pos.y = 0;
		}

		return pos;
	}
	
	// Update is called once per frame
	void Update () {
		float theta;
		Vector3 curScreenPoint, pos;

		if (_loading){
			if (IsLoadingComplete()){
				Loading = false;
				_gv_ui.ShowLoading(false);
			}
		}

		/*if (_pavingAreas != null) {
			try {
				foreach (Pattern pattern in _pavingAreas)
					if (pattern.Loading)
						pattern.CheckTextureLoading ();
			} catch (InvalidOperationException e) {
				//Debug.Log ();
			} catch(NullReferenceException e){
				
			}
		}*/

		bool productGUIShown = GV_UI.ProductGUIShown;
		if (_loading || productGUIShown || GV_UI.ModalActive) return;

		bool doubleClick = false;
		if (Input.GetMouseButtonDown(0) && !_mouseDown){
			_mouseDown = true;
			float elapsedTime = Time.time - _clickTime;
			doubleClick = (elapsedTime<0.3f);
			_clickTime = Time.time;
			if (_selectedT!=null){
				_screenPoint = Camera.main.WorldToScreenPoint(_selectedT.position);
				pos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, _screenPoint.z));
				_offset1 = _selectedT.position - pos;
				_initDragMousePos = Input.mousePosition;
			}
			if (_mouseMode == (int)MouseModes._NONE) _camMousePos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
		}else if (Input.GetMouseButtonUp (0)){
			_mouseDown = false;
		}
		if (doubleClick) Debug.Log ("GardenScript.Update doubleClick");

		if (Input.GetMouseButtonDown(0) && _mouseMode==(int)MouseModes._ADD_ROCK){
			pos = MouseToWorld(true);
			Debug.Log ("GardenScript.Update ADD_ROCK pos:" + pos);
			pos.y = 0;
			_storedProduct.Position = pos * 1000;
			_mouseMode = (int)MouseModes._NONE;
			WWWAsset rock = new WWWAsset(_storedProduct, "rock_" + _storedProduct.ProductName);
			if (_assets==null) _assets = new List<WWWAsset>();
			_assets.Add (rock);
			_project.Add(_storedProduct);
			Modified = true;
		}else if (Input.GetMouseButtonDown(0) && _mouseMode==(int)MouseModes._ADD_PILLAR){
			pos = MouseToWorld(true);
			Debug.Log ("GardenScript.Update ADD_PILLAR pos:" + pos);
			_mouseMode = (int)MouseModes._NONE;
			Product product = new Product(_selectedProduct.ToXMLNode());
			if (_selectedProduct.Type.Equals ("post")){
				pos.y = 0;//_selectedProduct.Position.y * 0.001f;
				product.Size = _selectedProduct.Size;
			}else{
				pos.y = 0;
				if (product.ProductName.Equals ("argent")){
					product.Size += 140;
				}else{
					product.Size += 150;
				}
			}
			product.Position = pos * 1000;
			product.Name = "product_" + _project.NextID();
			product.Type = "post";
			Debug.Log ("GardenScript.Update ADD_PILLAR sourceY:" + _selectedProduct.Position.y + " destY:" + product.Position.y + " sourceSize:" + _selectedProduct.Size + " destSize:" + product.Size);
			Post post = new Post(product);
			if (_posts==null) _posts = new List<Post>();
			_posts.Add (post);
			_project.Add(product);
			Modified = true;
		}else if (Input.GetMouseButtonDown(0) && _mouseMode==(int)MouseModes._ADD_DOLLY){
			pos = MouseToWorld(true);
			Debug.Log ("GardenScript.Update ADD_DOLLY pos:" + pos);
			_mouseMode = (int)MouseModes._NONE;
			Product product = new Product(_selectedProduct.ToXMLNode());
			product.Position = pos * 1000;
			product.Name = "product_" + _project.NextID();
			product.Type = "post";
			product.Size = 0;
			Post post = new Post(product);
			if (_posts==null) _posts = new List<Post>();
			_posts.Add (post);
			_project.Add(post.Product);
			Modified = true;
		}else if (Input.GetMouseButtonDown(0) && _mouseMode==(int)MouseModes._ADD_FLAG){
			pos = MouseToWorld(true);
			pos.y = 0.03f;
			Debug.Log ("GardenScript.Update ADD_FLAG pos:" + pos);
			_mouseMode = (int)MouseModes._NONE;
			Product product = _storedProduct;
			product.Position = pos * 1000;
			product.Name = "product_" + _project.NextID();
			Pattern pattern = new Pattern(_project, product);
			if (_pavingAreas==null) _pavingAreas = new List<Pattern>();
			_pavingAreas.Add (pattern);
			//_project.Add(product);
			Modified = true;
		}else if (!GV_UI.ButtonCapturedMouse && _drawShape==null){
			if (Input.GetMouseButtonDown(0) && Input.mousePosition.y>_mouseLegal[0] && Input.mousePosition.y<_mouseLegal[1]){
				Debug.Log("Mouse is down " + Input.mousePosition);

				RaycastHit hitInfo = new RaycastHit();
				bool hit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);

				if (hit) 
				{
					Transform t = hitInfo.transform;
					while(t.parent && t.parent!=GardenScript.Root) t = t.parent;

					if (doubleClick){
						Debug.Log("Hit " + hitInfo.transform.gameObject.name + " " + t.gameObject.name + " " + (_gv_ui!=null));
						//Bounds bounds = GetRBounds(t);
						//ShowSelectBox(bounds, t);
						if (t != _selectedT){
							ShowSelection(t);
							if (_gv_ui!=null){
								if (_selectedProduct!=null && _selectedProduct.Type.Equals ("house")){
									_gv_ui.ObjectSelected(true, "move,rotate,scale_house");
								}else if (_selectedProduct!=null && !_selectedProduct.ModifyButtons.Equals ("") && !_selectedProduct.ModifyButtons.Equals ("none")){
									_gv_ui.ObjectSelected(true, _selectedProduct.ModifyButtons);
								}else{
									_gv_ui.ObjectSelected(true, isPattern(t));
								}
							}
							_mouseMode = (int)MouseModes._MOVE;
						}
						_screenPoint = Camera.main.WorldToScreenPoint(_selectedT.position);
						pos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, _screenPoint.z));
						_offset1 = _selectedT.position - pos;
						_initDragMousePos = Input.mousePosition;
					}else if (_selectedProduct != GetSelectedProduct(t)){
						Debug.Log ("GardenScript.Update ClearSelection");
						ClearSelection();
						if (_gv_ui!=null) _gv_ui.ObjectSelected(false, false);
						_mouseMode = (int)MouseModes._NONE;
					}
				} else {
					//ClearSelectBox();
					Debug.Log ("GardenScript.Update ClearSelection");
					ClearSelection();
					if (_gv_ui!=null) _gv_ui.ObjectSelected(false, false);
					_mouseMode = (int)MouseModes._NONE;
				}
			} 

			if (Input.GetMouseButton (0) && Input.mousePosition.y>_mouseLegal[0] && Input.mousePosition.y<_mouseLegal[1]){
				switch(_mouseMode){
				case (int)MouseModes._NONE:
					//Debug.Log ("GardenScript.Update MouseModes._NONE _cameraScript.UseMouseMove:" + _cameraScript.UseMouseMove);
					if (_cameraScript!=null && _cameraScript.Interactive){
						Vector3 camOffset = (new Vector2(Input.mousePosition.x, Input.mousePosition.y) - _camMousePos) * Time.deltaTime;
						_cameraScript.PointCamera(camOffset);
					}
					break;
				case (int)MouseModes._MOVE:
					if (_selectedT){
						//Dragging
						if (_cameraScript.Overhead){
							curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, _screenPoint.z);
							pos = Camera.main.ScreenToWorldPoint(curScreenPoint) + _offset1;
							pos.y = _selectedT.position.y;
						}else{
							float diff = Input.mousePosition.y - _initDragMousePos.y;
							curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, _screenPoint.z + diff/30);
							pos = Camera.main.ScreenToWorldPoint(curScreenPoint) + _offset1;
							pos.y = _selectedT.position.y;
						}
						_selectedT.position = pos;
						if (_selectedProduct!=null){
							_selectedProduct.Position = pos * 1000;
						}else{
							Debug.Log ("GardenScript.Update no _selectedProduct");
						}
						Modified = true;
					}
					break;
				case (int)MouseModes._ROTATE://rotate
					theta = Input.mousePosition.x - _prevMousePosition.x;
					if (_selectedT){
						_selectedT.RotateAround(_rotationCentre, Vector3.up, theta * Time.deltaTime * 10);
						if (_selectedProduct!=null){
							_selectedProduct.Position = _selectedT.position * 1000;
							_selectedProduct.Rotation = _selectedT.rotation.eulerAngles.y;
						}
						Modified = true;
					}
					break;
				case (int)MouseModes._MOVE_PATTERN://pattern move
					if (_selectedT){
						if (isPattern(_selectedT)){
							Vector3 delta = Input.mousePosition - _prevMousePosition;
							Vector2 d = new Vector2(delta.x * Time.deltaTime, delta.y * Time.deltaTime);
							_pattern.MoveTexture( d.x, d.y);
							Modified = true;
						}
					}
					break;
				case (int)MouseModes._ROTATE_PATTERN://pattern rotate
					if (_selectedT){
						if (isPattern(_selectedT)){
							Vector3 delta = Input.mousePosition - _prevMousePosition;
							_pattern.RotateTexture( delta.x * Time.deltaTime);
							Modified = true;
						}
					}
					break;
				case (int)MouseModes._SCALE_HOUSE://scale house
					if (_selectedT){
						Vector3 delta = (Input.mousePosition - _prevMousePosition) * Time.deltaTime;
						House house = GetHouseFromSelectedProduct();
						house.AdjustScale(delta.x);
						Modified = true;
					}
					break;
				case (int)MouseModes._FLIP://flip
					if (_selectedT){
						Debug.Log ("GardenScript.Update flip");
					}
					break;
				case (int)MouseModes._ADD_DOLLY://add dolly
					if (_selectedT){
						
					}
					break;
				case (int)MouseModes._ADD_PILLAR://add pillar
					Debug.Log ("GardenScript.Update Add Pillar");
					_mouseMode = (int)MouseModes._NONE;
					if (_selectedProduct!=null){
						curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, _screenPoint.z);
						pos = Camera.main.ScreenToWorldPoint(curScreenPoint) * 1000.0f;
						//<product type="post" product="national_trust" color="pl" aspectratio="0" 
						//coping="peaked" copingcolor="pl" displayname="Coach House Pillar" displaycolor="Heathland (HL)" 
						//colorswatch="walling_national_trust_pl.jpg" 
						//rotation="0" size="1125" positionx="1971.97" positiony="0" positionz="-32.7082" 
						//patternpositionx="0" patternpositiony="0" patternrotation="0" displaypattern="" projectPack="" patternswatch="" pattern="" 
						string xml = "<product type=\"post\" aspectratio=\"0\" rotation=\"0\" ";
						xml += "pattern=\"\" patternpositionx=\"0\" patternpositiony=\"0\" patternrotation=\"0\" projectPack=\"\" displaypattern=\"\" ";
						xml += "product=\"" + _selectedProduct.ProductName + "\" ";
						xml += "color=\"" + _selectedProduct.Color + "\" ";
						if (_selectedProduct.PostCoping.Equals ("") || _selectedProduct.PostCoping.Equals ("none")){
							xml += "coping=\"" + _selectedProduct.Coping + "\" ";
						}else{
							xml += "coping=\"" + _selectedProduct.PostCoping + "\" ";
						}
						if (_selectedProduct.PostCopingColor.Equals ("") || _selectedProduct.PostCopingColor.Equals ("none")){
							xml += "copingcolor=\"" + _selectedProduct.CopingColor + "\" ";
						}else{
							xml += "copingcolor=\"" + _selectedProduct.PostCopingColor + "\" ";
						}
						xml += "displayname=\"" + _selectedProduct.DisplayName + "\" ";
						xml += "displaycolor=\"" + _selectedProduct.DisplayColor + "\" ";
						if (_selectedProduct.DisplayPostCoping.Equals ("") || _selectedProduct.DisplayPostCoping.Equals ("none")){
							xml += "displaycoping=\"" + _selectedProduct.DisplayCoping + "\" ";
						}else{
							xml += "displaycoping=\"" + _selectedProduct.DisplayPostCoping + "\" ";
						}
						if (_selectedProduct.DisplayPostCopingColor.Equals ("") || _selectedProduct.DisplayPostCopingColor.Equals ("none")){
							xml += "displaycopingcolor=\"" + _selectedProduct.DisplayCopingColor + "\" ";
						}else{
							xml += "displaycopingcolor=\"" + _selectedProduct.DisplayPostCopingColor + "\" ";
						}
						xml += "colorswatch=\"" + _selectedProduct.ColorSwatch + "\" ";
						xml += "positionx=\"" + pos.x.ToString () + "\" ";
						xml += "positiony=\"" + pos.y.ToString () + "\" ";
						xml += "positionz=\"" + pos.z.ToString () + "\" ";
						AddProduct("post", xml);
						Modified = true;
					}
					break;
				}
			}
		}

		if (_modified && _project!=null && !_loading){
			float elapsedTime = Time.time - _modifiedTime;
			if (elapsedTime>5){
				//Grab the project Thumbnail and then once this is complete it calls SaveProject passing the thumbnail in
				if (SnapshotController) _snapshotController.Grab();
				//_cameraScript.GrabThumbnail (this, true);
				_modified = false;
			}
		}

		_prevMousePosition = Input.mousePosition;
	}

	public void SaveProject(string thumbnail){
		_project.Thumbnail = thumbnail;
		if (_project.Guid==""){
			GVWebService.SaveNewProject (_project);
		}else{
			
			GVWebService.SaveProject(_project, _designController, false);
		}
		//string xml = _project.ToXML ();
		//Debug.Log ("GardenScript.SaveProject:" + xml);
	}

	public void AddHouse(string url, string guid, List<Vector2> vertices, float ar){
		Debug.Log ("GardenScript.AddHouse url:" + url);

		Product product = null;
		string xml;
		XmlDocument doc = new XmlDocument();

		if (url.Equals("default")){
			xml = "<product type=\"house\" product=\"\" color=\"\" pattern=\"texture_house\" size=\"8000\" aspectratio=\"1.33333\" " + 
				"coping=\"\" copingcolor=\"\" displayname=\"\" displaycolor=\"\" displaypattern=\"\" colorswatch=\"\" " + 
				"patternswatch=\"\" name=\"product_" + (_project.Products.Count+1) + "\" " + 
				"positionx=\"" + (-_project.Constraints.x/2) + "\" " + 
				"positiony=\"" + (_project.MaxProductY + _newProductSeparation) + "\" " + 
				"positionz=\"0\" rotation=\"0\" patternpositionx=\"0\" patternpositionz=\"0\" patternrotation=\"0\">" +
				"<sourcevertices>" +
				"	<vertex x=\"0\" y=\"86\" />" +
				"	<vertex x=\"927\" y=\"86\" />" +
				"	<vertex x=\"934\" y=\"81\" />" +
				"	<vertex x=\"941\" y=\"69\" />" +
				"	<vertex x=\"938\" y=\"11\" />" +
				"	<vertex x=\"938\" y=\"4\" />" +
				"	<vertex x=\"942\" y=\"1\" />" +
				"	<vertex x=\"976\" y=\"1\" />" +
				"	<vertex x=\"979\" y=\"4\" />" +
				"	<vertex x=\"979\" y=\"11\" />" +
				"	<vertex x=\"977\" y=\"69\" />" +
				"	<vertex x=\"984\" y=\"81\" />" +
				"	<vertex x=\"995\" y=\"90\" />" +
				"	<vertex x=\"995\" y=\"598\" />" +
				"	<vertex x=\"1011\" y=\"652\" />" +
				"	<vertex x=\"1011\" y=\"892\" />" +
				"	<vertex x=\"1022\" y=\"916\" />" +
				"	<vertex x=\"1022\" y=\"1023\" />" +
				"	<vertex x=\"10\" y=\"1023\" />" +
				"	<vertex x=\"10\" y=\"914\" />" +
				"	<vertex x=\"21\" y=\"891\" />" +
				"	<vertex x=\"21\" y=\"393\" />" +
				"	<vertex x=\"0\" y=\"393\" />" +
				"</sourcevertices>" +
				"</product>";
			doc.LoadXml(xml);
			product = new Product(doc.FirstChild);
			Debug.Log ("GardenScript.AddHouse product:" + product);
		}else{
			xml = "<product type=\"house\" product=\"" + guid + "\" color=\"\" pattern=\"" + url + "\" size=\"8000\" aspectratio=\"" + ar + "\" " + 
				"coping=\"\" copingcolor=\"\" displayname=\"\" displaycolor=\"\" displaypattern=\"\" colorswatch=\"\" " + 
					"patternswatch=\"\" name=\"product_" + (_project.Products.Count+1) + "\" " + 
					"positionx=\"" + (-_project.Constraints.x/2) + "\" " + 
					"positiony=\"" + (_project.MaxProductY + _newProductSeparation) + "\" " + 
					"positionz=\"0\" rotation=\"0\" patternpositionx=\"0\" patternpositionz=\"0\" patternrotation=\"0\">" +
					"<sourcevertices>";
			foreach(Vector2 pt in vertices){
				xml += "	<vertex x=\"" + pt.x + "\" y=\"" + pt.y + "\" />";
			}
			xml += "</sourcevertices></product>";
			doc.LoadXml(xml);
			product = new Product(doc.FirstChild);
			Debug.Log ("GardenScript.AddHouse product:" + product);
		}

		if (product!=null){
			House house = new House(product);
			_houses.Add(house);
			_project.Add(product);
			if (guid!="") _project.AddPhoto(guid);
			Modified = true;
		}
	}

	bool isPattern(Transform t){
		if (t==null) return false;
		foreach(Pattern pattern in _pavingAreas){
			if (pattern!=null && pattern.PatternGO == t.gameObject){
				if (pattern.Product.Type=="path" || pattern.SingleFlag){
					return false;
				}else{
					_pattern = pattern;
					return true;
				}
			}
		}
		_pattern = null;
		return false;
	}

	bool isHouse(Transform t){
		if (t==null) return false;
		foreach(House house in _houses){
			if (house!=null && house.HouseGO == t.gameObject){
				_house = house;
				return true;
			}
		}
		_house = null;
		return false;
	}

	Bounds GetBounds(Transform t){
		Bounds bounds = new Bounds();
		MeshFilter mf;
		//Mesh mesh;

		if (t.childCount==0){
			mf = t.gameObject.GetComponent<MeshFilter>();
			if (mf!=null){
				bounds = mf.mesh.bounds;
				bounds.center = t.localPosition;
				//Debug.Log ("GardenScript.GetBounds name:" + t.gameObject.name + " bounds:" + bounds);
			}
		}else{
			for(int i=0; i<t.childCount; i++){
				Bounds b = GetBounds(t.GetChild (i));
				bounds.Encapsulate(b.min);
				bounds.Encapsulate(b.max);
			}
		}

		return bounds;
	}
	
	Bounds GetRBounds(Transform t){
		//while(t.parent) t = t.parent;

		Bounds bounds = new Bounds();
		bool init = true;

		if (t.childCount==0){
			bounds = t.gameObject.GetComponent<Renderer>().bounds;
		}else{
			for(int i=0; i<t.childCount; i++){
				GameObject go = t.GetChild (i).gameObject;

				if (go!=null && go.GetComponent<Renderer>()!=null){
					if (init){
						bounds = go.GetComponent<Renderer>().bounds;
						init = false;
					}else{
						bounds.Encapsulate ( go.GetComponent<Renderer>().bounds.min );
						bounds.Encapsulate ( go.GetComponent<Renderer>().bounds.max );
					}
				}
			}
		}

		return bounds;
	}

	public void DrawPlot(){
		if (_plot!=null) Destroy(_plot);
		_drawShape = (GameObject)Instantiate(drawshape, Vector3.zero, Quaternion.identity);
		DrawShape script = _drawShape.GetComponent<DrawShape>();
		if (script!=null){
			if (Project.Products.Count == 0) {
				Product product = new Product ();
				product.Type = "plot";
				product.Name = "product_001";
				Project.Products.Add (product);
			}
			script.Product = Project.Products [0];
			script.GardenScript = this;
			script.Plot = true;
			_cameraScript.SetCamera (1, true);
		}
	}
		
	public void CreatePlot(string type, List<Vector3>vertices=null){
		if (_plot!=null) Destroy(_plot);

		Product product = new Product();

		product.Type = "plot";
		int count=32;
		float width, height, radius, theta = (float)((Math.PI*2)/count);

		if (type.Equals ("square")) {
			width = Math.Min (_project.Constraints.x, _project.Constraints.y);
			height = width;
			product.SourceVertices.Add (new Vector3 (-width / 2, 0, -height / 2));
			product.SourceVertices.Add (new Vector3 (-width / 2, 0, height / 2));
			product.SourceVertices.Add (new Vector3 (width / 2, 0, height / 2));
			product.SourceVertices.Add (new Vector3 (width / 2, 0, -height / 2));
		} else if (type.Equals ("rectangle")) {
			width = _project.Constraints.x;
			height = _project.Constraints.y;
			product.SourceVertices.Add (new Vector3 (-width / 2, 0, -height / 2));
			product.SourceVertices.Add (new Vector3 (-width / 2, 0, height / 2));
			product.SourceVertices.Add (new Vector3 (width / 2, 0, height / 2));
			product.SourceVertices.Add (new Vector3 (width / 2, 0, -height / 2));
		} else if (type.Equals ("lshape")) {
			width = _project.Constraints.x;
			height = _project.Constraints.y;
			product.SourceVertices.Add (new Vector3 (-width / 2, 0, -height / 2));
			product.SourceVertices.Add (new Vector3 (-width / 2, 0, height / 2));
			product.SourceVertices.Add (new Vector3 (width / 2, 0, height / 2));
			product.SourceVertices.Add (new Vector3 (width / 2, 0, 0));
			product.SourceVertices.Add (new Vector3 (0, 0, 0));
			product.SourceVertices.Add (new Vector3 (0, 0, -height / 2));
		} else if (type.Equals ("circle")) {
			radius = Math.Min (_project.Constraints.x, _project.Constraints.y) / 2;
			for (int i = 0; i < count; i++) {
				product.SourceVertices.Add (new Vector3 ((float)(Math.Cos (theta * i) * radius), 0, (float)(Math.Sin (theta * i) * radius)));
			}
		} else if (type.Equals ("semicircle")) {
			radius = Math.Min (_project.Constraints.x, (_project.Constraints.y * 2)) / 2;
			for (int i = 0; i <= (count / 2); i++) {
				product.SourceVertices.Add (new Vector3 ((float)(Math.Cos (theta * i) * radius), 0, (float)(Math.Sin (theta * i) * radius - radius / 2)));
			}
		} else if (type == "draw") {
			product.SourceVertices = vertices;
		}

		if (product.SourceVertices.Count>2){
			if (_project.Products.Count == 0) {
				_project.Add (product);
			} else {
				_project.Products [0] = product;
			}
			CreatePlot(product);
			Modified = true;
		}
	}

	public void CreatePlot(Product product){
		if (_plot!=null) Destroy(_plot);

		Texture2D t = (Texture2D)Resources.Load("images/surface_topsoil");
		_plot = CreatePlane(product, t);
		Vector3 pos = product.Position;
		pos.y = _newProductSeparation;
		product.Position = pos;
		_plot.transform.position = pos * 0.001f;

		GV_UI.DesignController.ShowCameraControls(false);
	}

	Vector3 GetBB(List<Vector3>vs){
		Vector3 min = GetMin(vs);
		Vector3 max = GetMax(vs);
		return max - min;
	}

	Vector3 GetMin(List<Vector3>vs){
		Vector3 min = vs[0];
		foreach(Vector3 v in vs){
			if (v.x<min.x) min.x=v.x;
			if (v.y<min.y) min.y=v.y;
			if (v.z<min.z) min.z=v.z;
		}
		return min;
	}

	Vector3 GetMax(List<Vector3>vs){
		Vector3 max = vs[0];
		foreach(Vector3 v in vs){
			if (v.x>max.x) max.x=v.x;
			if (v.y>max.y) max.y=v.y;
			if (v.z>max.z) max.z=v.z;
		}
		return max;
	}

	GameObject CreatePlane(Product product, Texture2D t)
	{
		// Create Vector2 vertices
		Vector2[] vertices2D = new Vector2[product.SourceVertices.Count];
		Vector2[] uvs = new Vector2[product.SourceVertices.Count];
		Vector3 bb = GetBB(product.SourceVertices);
		
		int i=0;
		foreach(Vector3 v in product.SourceVertices){
			vertices2D[i] = new Vector2(v.x*0.001f, v.z*0.001f);
			uvs[i] = new Vector2(v.x/bb.x, v.z/bb.z);
			i++;
		}
		
		// Use the triangulator to get indices for creating triangles
		Triangulator tr = new Triangulator(vertices2D);
		int[] indices = tr.Triangulate();
		
		// Create the Vector3 vertices
		Vector3[] vertices = new Vector3[vertices2D.Length];
		for (i=0; i<vertices.Length; i++) {
			vertices[i] = new Vector3(vertices2D[i].x, 0.0f, vertices2D[i].y );
		}
		
		GameObject go = new GameObject( product.Name );
		go.AddComponent<MeshFilter>();
		go.AddComponent<MeshRenderer>();
		
		Material mat = new Material (Shader.Find("Diffuse"));
		mat.mainTexture = t;
		mat.mainTextureScale = (new Vector2( bb.x, bb.z))/1600.0f;
		
		MeshFilter filter = go.GetComponent<MeshFilter>();
		Renderer renderer = go.GetComponent<MeshRenderer>().GetComponent<Renderer>();
		renderer.material = mat;
		
		// Create the mesh
		Mesh mesh = filter.mesh;
		mesh.vertices = vertices;
		mesh.triangles = indices;
		mesh.uv = uvs;
		mesh.RecalculateNormals();
		mesh.RecalculateBounds();
		
		go.transform.position = product.Position * 0.001f;
		go.transform.rotation = Quaternion.Euler (0, (float)(product.Rotation * (Math.PI/180)), 0);
		go.name = "plot";

		return go;
	}

	public GameObject CreateFeature(string type, Vector3 position, Quaternion rotation){
		Debug.Log ("GardenScript CreateFeature " + type);
		GameObject go = null;

		if (type.Equals("square_feature")){
			go = (GameObject)Instantiate(square_feature, position, rotation);
		}else if (type.Equals("circle_feature")){
			go = (GameObject)Instantiate(circle_feature, position, rotation);
		}else if (type.Equals("octant_feature")){
			go = (GameObject)Instantiate(octant_feature, position, rotation);
		}else if (type.Equals("bonding_feature")){
			go = (GameObject)Instantiate(bonding_feature, position, rotation);
		}

		Modified = true;

		return go;
	}

	bool UseSelectionBox(Transform t){
		string name = "";
		if (t.gameObject.GetComponent<Renderer>()!=null){
			name = t.gameObject.name;
		}else{
			for(int i=0; i<t.childCount; i++){
				Transform tt = t.GetChild (i);
				if (tt.GetComponent<Renderer>()!=null){
					name = tt.gameObject.name;
					break;
				}
			}
		}
		string [] items = {"vegetation", "tree", "greenhouse", "greenshourse", "summerhouse"};
		bool found = false;

		for(int i=0; i< items.Length; i++){
			if (name.Contains (items[i])){
				found = true;
				break;
			}
		}

		return found;
	}

	Boundary GetBoundary(Product product){
		Boundary boundary = null;

		if (_boundaries!=null){
			for(int i=0; i<_boundaries.Count; i++){
				if (_boundaries[i].Product==product){
					boundary = _boundaries[i];
					break;
				}
			}
		}

		return boundary;
	}

	public void ShowSelection(Transform t){
		if (t == _selectedT) return;

		if (_selectedT!=null) ClearSelection();

		_selectedProduct = GetSelectedProduct(t);

		Bounds bounds = GetBounds(t);
		if (t.childCount==0){
			_rotationCentre = t.position;
		}else{
			_rotationCentre = t.position + bounds.center;
		}

		if (_selectedProduct.Type == "boundary"){
			Boundary boundary = GetBoundary(_selectedProduct);
			_rotationCentre = boundary.GetRotationCentre();
		}

		if (UseSelectionBox (t)) {
			ShowSelectBox (bounds, t);
		}else if (isHouse(t)){
			_house.Highlight (true);
		}else if (isPattern(t)){
			_pattern.Highlight(true);
		}else{
			//storedMaterials = SaveSelectedMaterials(t);
			Material mat = new Material(Shader.Find("Self-Illumin/Diffuse"));
			mat.color = new Color(0.8f, 0.8f, 0.8f);
			ApplyMaterial(t, mat, true);
			//UpdateMaterials(t, new Color(0.1f, 0.1f, 0.1f), true);
		}
		_selectedT = t;
	}

	Material [] SaveSelectedMaterials(Transform t){
		Material [] materials = new Material[t.gameObject.GetComponent<Renderer>().materials.Length];
		for(int i=0; i<t.gameObject.GetComponent<Renderer>().materials.Length; i++){
			materials[i] = new Material(t.gameObject.GetComponent<Renderer>().materials[i]);
		}
		return materials;
	}

	public void ClearSelection(){
		if (_selectBox == null) return;
		if (_selectedT == null) {
			if (_selectedProduct!=null && _selectedProduct.GameObject!=null) _selectedT = _selectedProduct.GameObject.transform;
			if (_selectedT==null) return;
		}

		if (_selectBox.activeSelf){
			_selectBox.SetActive (false);
			_selectBox.transform.parent = null;
		}else{
			if (isPattern (_selectedT)) {
				_pattern.Highlight (false);
			}else if (isHouse(_selectedT)){
				_house.Highlight (false);
			}else{
//				Debug.Log ("GardenScript.ClearSelection '" + _selectedProduct.Type + "'");
				Material mat = GetMaterial();
				if (mat!=null) ApplyMaterial(_selectedT, mat, false);
				//UpdateMaterials(_selectedT, new Color(0.1f, 0.1f, 0.1f), false);
				//RestoreMaterials();
			}
		}
		_selectedT = null;
		_selectedProduct = null;
	}

	Material GetMaterial(){
		Material mat = null;
		if (_selectedProduct == null){
			mat = new Material(Shader.Find("Diffuse"));
			Debug.Log ("GardenScript.GetMaterial Diffuse");
		}else if(_selectedProduct.ProductName.Equals ("water")){
			Debug.Log ("GardenScript.GetMaterial FX/Water");
			GameObject water = GameObject.Find ("Water");
			_selectedT.gameObject.GetComponent<Renderer>().material = water.GetComponent<Renderer>().material;
		}else if (_selectedProduct.Type.Equals ("boundary")||_selectedProduct.Type.Equals ("vegetation")){
			Debug.Log ("GardenScript.GetMaterial Transparent/Diffuse Product.Type:" + _selectedProduct.Type);
			mat = new Material(Shader.Find("Transparent/Diffuse"));
		}else{
			Debug.Log ("GardenScript.GetMaterial Diffuse Product.Type:" + _selectedProduct.Type);
			mat = new Material(Shader.Find("Diffuse"));
		}
		return mat;
	} 

	void UpdateMaterials(Transform t, Color col, bool add){
		//Texture tex;
		
		if (t!=null && t.gameObject!=null && t.gameObject.GetComponent<Renderer>()!=null){
			if (t.gameObject.GetComponent<Renderer>().materials.Length>1){
				Material [] materials = new Material[t.gameObject.GetComponent<Renderer>().materials.Length];
				for(int i=0; i<t.gameObject.GetComponent<Renderer>().materials.Length; i++){
					//tex = t.gameObject.renderer.materials[i].mainTexture;
					if (add){
						materials[i].color += col;
					}else{
						materials[i].color -= col;
					}
				}
			
			}
		}else{
			Debug.Log ("GardenScript.ApplyMaterial something null " + t);
		}
		
		if (t!=null && t.childCount>0){
			for(int i=0; i<t.childCount; i++){
				Transform tt = t.GetChild (i);
				if (tt!=null) UpdateMaterials(tt, col, add);
			}
		}
	}
						
	public void ApplyMaterial(Transform t, Material mat, bool selected){
		Texture tex;

		if (t!=null && t.gameObject!=null && t.gameObject.GetComponent<Renderer>()!=null){
			if (t.gameObject.GetComponent<Renderer>().materials.Length>1){
				Material [] materials = new Material[t.gameObject.GetComponent<Renderer>().materials.Length];
				for(int i=0; i<t.gameObject.GetComponent<Renderer>().materials.Length; i++){
					tex = t.gameObject.GetComponent<Renderer>().materials[i].mainTexture;
					//materials[i] = mat;
					if (selected){
						materials[i] = new Material(Shader.Find("Self-Illumin/Diffuse"));
						materials[i].color = new Color(0.8f, 0.8f, 0.8f);
					}else{
						materials[i] = new Material(Shader.Find("Diffuse"));
						if (_selectedProduct!=null && (_selectedProduct.Type.Equals("circle"))){
							materials[i].color = new Color(0.8f, 0.8f, 0.8f);
						}else{
							//materials[i].color = new Color(1.0f, 1.0f, 1.0f);
							materials[i].color = new Color(0.8f, 0.8f, 0.8f);
						}
					}
					materials[i].mainTexture = tex;
				}
				t.gameObject.GetComponent<Renderer>().materials = materials;
			}else{
				Material material;
				tex = t.gameObject.GetComponent<Renderer>().material.mainTexture;
				Vector2 scale = t.gameObject.GetComponent<Renderer>().material.mainTextureScale;
				if (selected){
					material = new Material(Shader.Find("Self-Illumin/Diffuse"));
					material.color = new Color(0.8f, 0.8f, 0.8f);
				}else{
					material = new Material(Shader.Find("Diffuse"));
					if (_selectedProduct!=null && 
					    (_selectedProduct.Type.Equals("circle") || _selectedProduct.Type.Equals ("octant") || 
						(_selectedProduct.Type.Equals ("paving") && _selectedProduct.Pattern.StartsWith("flag_")))){
						//material.color = new Color(0.8f, 0.8f, 0.8f);
					}
				}
				material.mainTexture = tex;
				material.mainTextureScale = scale;
				t.gameObject.GetComponent<Renderer>().material = material;
			}
		}else{
			Debug.Log ("GardenScript.ApplyMaterial something null " + t);
		}

		if (t!=null && t.childCount>0){
			for(int i=0; i<t.childCount; i++){
				Transform tt = t.GetChild (i);
				if (tt!=null) ApplyMaterial(tt, mat, selected);
			}
		}
	}

	public void ClearSelectBox(){
		_selectBox.transform.parent = null;
		_selectBox.SetActive (false);
		_selectedT = null;
	}

	public void ShowSelectBox(Bounds bounds, Transform t){

		//Debug.Log ("GardenScript.ShowSelectBox bounds:" + bounds);

		_selectBox.SetActive (true);
		_selectBox.transform.position = t.position;
		_selectBox.transform.rotation = t.rotation;
		_selectBox.transform.parent = t;
		Vector3 pos = bounds.center;
		pos.y = bounds.extents.y;
		_selectBox.transform.localPosition = pos;

		Vector3 size = bounds.size;
		size *= 1.001f;
		size.y += 0.1f;
		//size *= 100;
		Vector3 extents = size/2.0f;

		_selectBox.transform.GetChild (0).transform.localPosition = new Vector3(extents.x, -extents.y, extents.z);
		_selectBox.transform.GetChild (1).transform.localPosition = new Vector3(-extents.x, -extents.y, extents.z);
		_selectBox.transform.GetChild (2).transform.localPosition = new Vector3(-extents.x, extents.y, extents.z);
		_selectBox.transform.GetChild (3).transform.localPosition = new Vector3(extents.x, extents.y, extents.z);
		_selectBox.transform.GetChild (4).transform.localPosition = new Vector3(extents.x, -extents.y, -extents.z);
		_selectBox.transform.GetChild (5).transform.localPosition = new Vector3(-extents.x, -extents.y, -extents.z);
		_selectBox.transform.GetChild (6).transform.localPosition = new Vector3(extents.x, extents.y, -extents.z);
		_selectBox.transform.GetChild (7).transform.localPosition = new Vector3(-extents.x, extents.y, -extents.z);
		_selectBox.transform.GetChild (8).transform.localPosition = Vector3.zero;
		_selectBox.transform.GetChild (8).transform.localScale = size * 100;

		_selectedT = t;
	}

	public static void DestroyObject(GameObject go){
		Destroy(go);

		Modified = true;

	}

	public static GameObject CloneGameObject(GameObject go){
		GameObject go1 = (GameObject)Instantiate(go);
		go1.SetActive (true);

		Modified = true;

		return go1;
	}

	public static GameObject InstantiateGameObject(AssetBundle asset){
		try{
			Debug.Log ("GardenScript.InstantiateGameObject asset:" + asset.name);
			GameObject go = (GameObject)Instantiate(asset.mainAsset);
			if (go.transform.childCount>0){
				for(int i=0; i<go.transform.childCount; i++){
					Transform t = go.transform.GetChild (i);
					GameObject go1 = t.gameObject;
					if (go1!=null && go1.GetComponent<Renderer>()!=null){
						go1.AddComponent<BoxCollider>();
					}
				}
			}else{
				go.AddComponent<BoxCollider>();
			}

			Modified = true;

			return go;
		}catch(ArgumentException e){
			Debug.Log ("GardenScript.InstantiateGameObject something null");
		}catch(NullReferenceException e){
			Debug.Log ("GardenScript.InstantiateGameObject something null");
		}
		return null;
	}

	public static GameObject InstantiateGameObject(WWW www){
		GameObject go = (GameObject)Instantiate(www.assetBundle.mainAsset);
		if (go.transform.childCount>0){
			for(int i=0; i<go.transform.childCount; i++){
				Transform t = go.transform.GetChild (i);
				GameObject go1 = t.gameObject;
				if (go1!=null && go1.GetComponent<Renderer>()!=null){
					go1.AddComponent<BoxCollider>();
				}
			}
		}else{
			go.AddComponent<BoxCollider>();
		}

		Modified = true;

		return go;

	}

	public void ChangePattern(Pattern prevPattern, Product product){
		product.Position = prevPattern.Product.Position;
		product.Rotation = prevPattern.Product.Rotation;
		product.PatternPosition = prevPattern.Product.PatternPosition;
		product.PatternRotation = prevPattern.Product.PatternRotation;
		product.SourceVertices = new List<Vector3>();
		product.Name = prevPattern.Product.Name;

		if ((prevPattern.Product.Pattern.StartsWith ("flag_") && !product.Pattern.StartsWith ("flag_")) ||
		    (!prevPattern.Product.Pattern.StartsWith ("flag_") && product.Pattern.StartsWith ("flag_"))){
			GV_UI.ShowAlert("Change Paving", "You cannot make this change");
			return;
		}else{
			for(int i=0; i<prevPattern.Product.SourceVertices.Count; i++){
				product.SourceVertices.Add (prevPattern.Product.SourceVertices[i]);
			}
		}
		DeleteSelected();
		Pattern pattern = new Pattern(_project, product);
		pattern.AutoSelect = true;
		pattern.GardenScript = this;
		if (_pavingAreas!=null) _pavingAreas.Add (pattern);
		_project.Add(product);
		Modified = true;
	}

	public void SelectPattern(Pattern pattern){
		_selectedT = null;
		ShowSelection(pattern.PatternGO.transform);
		//_gv_ui.ObjectSelected(true, true);
	}

	public void SelectBoundary(Boundary boundary){
		_selectedT = null;
		ShowSelection(boundary.BoundaryGO.transform);
		//_gv_ui.ObjectSelected(true, false);
	}

	public void CreateFromShape(DrawShape shape){
		//Take first vertex from the vertices list and delete this from each vertex.
		Debug.Log ("GardenScript.CreateFromShape " + shape.Product+  " type : " + shape.Product.Type);

		if (shape.Product.SourceVertices.Count>=2){
			Vector3 pos = shape.Product.SourceVertices[0];
			pos.y = 10 * _project.Products.Count;

			//Use first vertex as product position
			for(int i=0; i<shape.Product.SourceVertices.Count; i++){ 
				Vector3 p = shape.Product.SourceVertices[i] - pos;
				p.y = 0;
				shape.Product.SourceVertices[i] = p;
			}
			shape.Product.Position = pos;
			bool added = false;

			switch(shape.Product.Type){
			case "paving":
			case "driveway":
			case "path":
				Pattern pattern = new Pattern(_project, shape.Product);
				if (_pavingAreas!=null) _pavingAreas.Add (pattern);
				added = true;
				break;
			case "kerb":
			case "edging":
			case "edging_supported":
				Edging edging = new Edging(shape.Product);
				if (_edging!=null) _edging.Add (edging);
				added = true;
				break;
			case "surface":
			case "aggregate":
				Surface surface = new Surface(shape.Product);
				if (_surfaces!=null) _surfaces.Add (surface);
				added = true;
				break;
			case "walling":
				Walling wall = new Walling(shape.Product);
				if (_walling!=null) _walling.Add (wall);
				added = true;
				break;
			case "palisades":
				Palisade palisade = new Palisade(shape.Product);
				if (_palisades!=null) _palisades.Add (palisade);
				added = true;
				break;
			case "boundary":
				Boundary boundary = new Boundary(shape.Product);
				if (_boundaries!=null) _boundaries.Add(boundary);
				added = true;
				break;
			}

			if (added){
				if (!_project.Products.Contains (shape.Product)) {
					_project.Add (shape.Product);
				}
				Modified = true;
			}
		}

		Destroy(shape.gameObject);
	}

	public void ButtonSelected(){
		if (_drawShape!=null){
			Destroy(_drawShape);
			_drawShape = null;
		}
	}

	public void DeleteSelected(){
		DeleteSelected(true);
	}

	public void DeleteSelected(bool removeFromLists){
		if (_selectedT == null) return;

		_selectBox.transform.parent = null;
		_selectBox.SetActive (false);

		bool found = false;
		Product product = null;
		string name = "";
		//Find the selected object in the lists
		if (_pavingAreas!=null && _pavingAreas.Count>0){
			foreach(Pattern pattern in _pavingAreas){
				if (pattern!=null && pattern.PatternGO == _selectedT.gameObject){
					found = true;
					name = pattern.PatternGO.name;
					Destroy(pattern.PatternGO);
					if (removeFromLists){
						_pavingAreas.Remove (pattern);
						product = pattern.Product;
					}
					break;
				}
			}
		}
		if (!found && _houses!=null && _houses.Count>0){
			foreach(House house in _houses){
				if (house!=null && house.HouseGO == _selectedT.gameObject){
					found = true;
					name = house.HouseGO.name;
					Destroy(house.HouseGO);
					if (removeFromLists){
						_houses.Remove (house);
						product = house.Product;
						if (product.ProductName!= "") _project.RemovePhoto(product.ProductName);
					}
					break;
				}
			}
		}
		if (!found && _surfaces!=null && _surfaces.Count>0){
			foreach(Surface surface in _surfaces){
				if (surface!=null && surface.SurfaceGO == _selectedT.gameObject){
					found = true;
					name = surface.SurfaceGO.name;
					Destroy(surface.SurfaceGO);
					if (removeFromLists){
						_surfaces.Remove (surface);
						product = surface.Product;
					}
					break;
				}
			}
		}
		if (!found && _features!=null && _features.Count>0){
			foreach(Feature feature in _features){
				if (feature!=null && feature.FeatureGO == _selectedT.gameObject){
					found = true;
					name = feature.FeatureGO.name;
					Destroy(feature.FeatureGO);
					if (removeFromLists){
						_features.Remove (feature);
						product = feature.Product;
					}
					break;
				}
			}
		}
		if (!found && _assets!=null && _assets.Count>0){
			foreach(WWWAsset asset in _assets){
				if (asset!=null && asset.AssetGO == _selectedT.gameObject){
					if (asset.Product.Type == "post")
						continue;
					found = true;
					name = asset.AssetGO.name;
					Destroy(asset.AssetGO);
					if (removeFromLists){
						_assets.Remove (asset);
						product = asset.Product;
					}
					break;
				}
			}
		}
		if (!found && _edging!=null && _edging.Count>0){
			foreach(Edging edging in _edging){
				if (edging!=null && edging.EdgingGO == _selectedT.gameObject){
					found = true;
					name = edging.EdgingGO.name;
					Destroy(edging.EdgingGO);
					if (removeFromLists){
						_edging.Remove (edging);
						product = edging.Product;
					}
					break;
				}
			}
		}
		if (!found && _walling!=null && _walling.Count>0){
			foreach(Walling walling in _walling){
				if (walling!=null && walling.WallingGO == _selectedT.gameObject){
					found = true;
					name = walling.WallingGO.name;
					Destroy(walling.WallingGO);
					walling.Clear ();
					if (removeFromLists){
						_walling.Remove (walling);
						product = walling.Product;
					}
					break;
				}
			}
		}
		if (!found && _palisades!=null && _palisades.Count>0){
			foreach(Palisade palisade in _palisades){
				if (palisade!=null && palisade.PalisadeGO == _selectedT.gameObject){
					found = true;
					name = palisade.PalisadeGO.name;
					palisade.Clear();
					Destroy(palisade.PalisadeGO);
					if (removeFromLists){
						_palisades.Remove (palisade);
						product = palisade.Product;
					}
					break;
				}
			}
		}

		if (!found && _boundaries!=null && _boundaries.Count>0){
			foreach(Boundary boundary in _boundaries){
				if (boundary!=null && boundary.BoundaryGO == _selectedT.gameObject){
					found = true;
					name = boundary.BoundaryGO.name;
					Destroy(boundary.BoundaryGO);
					if (removeFromLists){
						_boundaries.Remove (boundary);
						product = boundary.Product;
					}
					break;
				}
			}
		}

		if (!found && _posts!=null && _posts.Count>0){
			foreach(Post post in _posts){
				if (post!=null && post.PostGO == _selectedT.gameObject){
					found = true;
					name = post.PostGO.name;
					Destroy(post.PostGO);
					if (post.CopingGO!=null) Destroy(post.CopingGO);
					if (removeFromLists){
						_posts.Remove (post);
						product = post.Product;
					}
					break;
				}
			}
		}

		if (product!=null) _project.Products.Remove (product);

		Modified = true;

		Debug.Log ("GardenScript.DeleteSelected found:" + found + " name:" + name);
	}

	public void FlipSelected(){
		if (_selectedProduct==null){
			Debug.Log ("GardenScript.FlipSelected called with no selected product");
			return;
		}
		//Remove the existing gameObjects and remove the product from the lists
		DeleteSelected(true);
		//Reverse the source vertices
		List<Vector3> verts = new List<Vector3>();
		for(int i=_selectedProduct.SourceVertices.Count-1; i>=0; i--){
			verts.Add (_selectedProduct.SourceVertices[i]);
		}
		_selectedProduct.SourceVertices = verts;

		if (_selectedProduct.Type.Equals ("kerb") || _selectedProduct.Type.Equals ("edging")){
			Edging edging = new Edging(_selectedProduct);
			_edging.Add (edging);
			_project.Add(_selectedProduct);
		}else if (_selectedProduct.Type.Equals ("walling")){
			Walling wall = new Walling(_selectedProduct);
			_walling.Add (wall);
			_project.Add(_selectedProduct);
		}else if (_selectedProduct.Type.Equals ("boundary")){
			Boundary boundary = new Boundary(_selectedProduct);
			_boundaries.Add (boundary);
			SelectBoundary(boundary);
			_project.Add(_selectedProduct);
		}

		Modified = true;
	}

	public Product GetSelectedProduct(Transform t){
		bool found = false;
		string name = "";
		Product product = null;
		//Find the selected object in the lists
		if (_pavingAreas!=null && _pavingAreas.Count>0){
			foreach(Pattern pattern in _pavingAreas){
				if (pattern!=null && pattern.PatternGO == t.gameObject){
					found = true;
					name = pattern.PatternGO.name;
					product = pattern.Product;
					break;
				}
			}
		}
		if (!found && _houses!=null && _houses.Count>0){
			foreach(House house in _houses){
				if (house!=null && house.HouseGO == t.gameObject){
					found = true;
					name = house.HouseGO.name;
					product = house.Product;
					break;
				}
			}
		}
		if (!found && _surfaces!=null && _surfaces.Count>0){
			foreach(Surface surface in _surfaces){
				if (surface!=null && surface.SurfaceGO == t.gameObject){
					found = true;
					name = surface.SurfaceGO.name;
					product = surface.Product;
					break;
				}
			}
		}
		if (!found && _features!=null && _features.Count>0){
			foreach(Feature feature in _features){
				if (feature!=null && feature.FeatureGO == t.gameObject){
					found = true;
					name = feature.FeatureGO.name;
					product = feature.Product;
					break;
				}
			}
		}
		if (!found && _edging!=null && _edging.Count>0){
			foreach(Edging edging in _edging){
				if (edging!=null && edging.EdgingGO == t.gameObject){
					found = true;
					name = edging.EdgingGO.name;
					product = edging.Product;
					break;
				}
			}
		}
		if (!found && _walling!=null && _walling.Count>0){
			foreach(Walling walling in _walling){
				if (walling!=null && walling.WallingGO == t.gameObject){
					found = true;
					name = walling.WallingGO.name;
					product = walling.Product;
					break;
				}
			}
		}
		if (!found && _palisades!=null && _palisades.Count>0){
			foreach(Palisade palisade in _palisades){
				if (palisade!=null && palisade.PalisadeGO == t.gameObject){
					found = true;
					name = palisade.PalisadeGO.name;
					product = palisade.Product;
					break;
				}
			}
		}
		if (!found && _boundaries!=null && _boundaries.Count>0){
			foreach(Boundary boundary in _boundaries){
				if (boundary!=null && boundary.BoundaryGO == t.gameObject){
					found = true;
					name = boundary.BoundaryGO.name;
					product = boundary.Product;
					break;
				}
			}
		}
		if (!found && _posts!=null && _posts.Count>0){
			foreach(Post post in _posts){
				if (post!=null && post.PostGO == t.gameObject){
					found = true;
					name = post.PostGO.name;
					product = post.Product;
					break;
				}
			}
		}
		if (!found && _assets!=null && _assets.Count>0){
			foreach(WWWAsset asset in _assets){
				if (asset!=null && asset.AssetGO == t.gameObject){
					found = true;
					name = asset.AssetGO.name;
					product = asset.Product;
					break;
				}
			}
		}

		Debug.Log ("GardenScript.GetSelectedProduct found:" + found + " name:" + name);
		return product;
	}

	IEnumerator DownloadAndCache (WWWAsset wwwAsset){
		// Wait for the Caching system to be ready
		while (!Caching.ready)
			yield return null;
		// Load the AssetBundle file from Cache if it exists with the same version or download and store it in the cache
		using(WWW www = WWW.LoadFromCacheOrDownload (wwwAsset.BundleURL, 2)){
			yield return www;
			if (www.error != null)
				throw new Exception("WWW download had an error:" + www.error);
			AssetBundle bundle = www.assetBundle;
			wwwAsset.AssetGO = (GameObject)Instantiate(bundle.mainAsset);
			bundle.Unload(false);
			
		} // memory is freed from the web stream (www.Dispose() gets called implicitly)
	}

	/*private string GrabThumbnail(){
		_cameraScript.SetThumbnailCamera(true);
		Texture2D newTexture = ScreenShoot(Camera.main, 416, 336);
		_cameraScript.SetThumbnailCamera(false);
		return System.Convert.ToBase64String(newTexture.EncodeToPNG());
	}*/

	public void TakeSnapshot(){
		Transform t = Camera.main.transform;
		Snapshot snapshot = new Snapshot(t.position * 1000, t.localEulerAngles);
		Debug.Log ("GardenScript.TakeSnapshot called " + snapshot.ToString());
		if (_project!=null) _project.Shots.Add (snapshot);
		Modified = true;
	}

	public void UpdateSnapshots(float scale){
		Debug.Log ("GardenScript.UpdateSnapshots called ");
		if (_project != null){
			Vector3 position = Camera.main.transform.position;
			Quaternion rotation = Camera.main.transform.rotation;
			int i=1;

			foreach(Snapshot snapshot in _project.Shots){
				Camera.main.transform.position = snapshot.Position * 0.001f;
				Camera.main.transform.rotation = Quaternion.Euler( snapshot.Rotation );
				//Application.CaptureScreenshot("snapshot" + i);
				Texture2D newTexture = ScreenShoot(Camera.main, (int)(200 * scale), (int)(160 * scale));
				//LerpTexture(bg, ref newTexture);
				snapshot.Texture = (Texture)newTexture;
				i++;
			}

			Camera.main.transform.position = position;
			Camera.main.transform.rotation = rotation;
		}
	}

	public void SnapshotSelected(Snapshot snapshot){
		if (_cameraScript!=null) _cameraScript.SnapshotSelected(snapshot);
	}

	private static Texture2D ScreenShoot(Camera srcCamera, int width, int height){
		var renderTexture = new RenderTexture(width, height, 24);
		var targetTexture = new Texture2D(width, height, TextureFormat.RGB24, false);
		
		srcCamera.targetTexture = renderTexture;
		srcCamera.Render();
		RenderTexture.active = renderTexture;
		targetTexture.ReadPixels(new Rect(0, 0, width, height), 0, 0);
		targetTexture.Apply();
		
		srcCamera.targetTexture = null;
		RenderTexture.active = null;
		srcCamera.ResetAspect();
		
		return targetTexture;
	}

	private static void LerpTexture(Texture2D alphaTexture, ref Texture2D texture){
		var bgColors = alphaTexture.GetPixels();
		var tarCols = texture.GetPixels();
		for (var i = 0; i < tarCols.Length; i++)
			tarCols[i] = bgColors[i].a > 0.99f ? bgColors[i] : Color.Lerp(tarCols[i], bgColors[i], bgColors[i].a);
		
		texture.SetPixels(tarCols);
		texture.Apply();
	}

	public void HouseImageCallback(string str) {
		Debug.Log ("GardenScript.HouseImageCallback " + str + " " + (GV_UI.AddHouseController!=null));
		if (GV_UI.AddHouseController!=null) GV_UI.AddHouseController.FileSelectedCallback(str);
	}
}
