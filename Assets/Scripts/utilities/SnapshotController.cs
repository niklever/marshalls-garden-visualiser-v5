﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SnapshotController : MonoBehaviour {
	bool _takeScreenshot;
	List<Snapshot> _snapshots;
	string [] _snaps;
	public string [] Snaps {
		get {
			return _snaps;
		}
	}
	Snapshot _thumbnail;
	public Snapshot Thumbnail {
		set {
			_thumbnail = value;
		}
	}
	int _snapIdx;
	Camera _camera;
	RenderTexture _rt;
	RenderTexture _renderTexture;
	ScreenshotsController _ssController;
	RealityController _rController;
	bool _saveProject;

	// Use this for initialization
	void Start () {
		_takeScreenshot = false;
		_camera = gameObject.GetComponent<Camera> ();
		_rt = _camera.targetTexture;
		_saveProject = false;
	}

	public void Grab(){
		_saveProject = true;
		_renderTexture = new RenderTexture(416, 336, 24);
		_camera.targetTexture = _renderTexture;
		_snapshots = new List<Snapshot>();
		_snapshots.Add (_thumbnail);
		_snaps = new string[_snapshots.Count];
		_snapIdx = -1;
		initNextSnap ();
	}

	public void Grab(List<Snapshot> snapshots, ScreenshotsController controller){
		_ssController = controller;
		_renderTexture = new RenderTexture(416, 336, 24);
		_camera.targetTexture = _renderTexture;
		_snapshots = snapshots;
		_snaps = new string[snapshots.Count];
		_snapIdx = -1;
		initNextSnap ();
	}

	public void Grab(List<Snapshot>  snapshots, RealityController controller){
		_rController = controller;
		_renderTexture = new RenderTexture(1024, 768, 24);
		_camera.targetTexture = _renderTexture;
		_snapshots = snapshots;
		_snaps = new string[snapshots.Count];
		_snapIdx = -1;
		initNextSnap ();
	}

	void initNextSnap(){
		_snapIdx++;
		if (_snapIdx >= _snapshots.Count) {
			//Time to let the parent know that we have all the images
			_renderTexture = null;
			_camera.targetTexture = _rt;
			RenderTexture.active = null;
			if (_ssController != null) {
				_ssController.Init ();
				_ssController = null;
			} else if (_rController != null) {
				_rController.SendPDF ();
				_rController = null;
			} else if (_saveProject) {
				GV_UI.GardenScript.SaveProject (_thumbnail.Base64);
				_saveProject = false;
			}
			_takeScreenshot = false;
		} else {
			Snapshot snapshot = _snapshots [_snapIdx];
			_camera.transform.position = snapshot.Position * 0.001f;
			_camera.transform.rotation = Quaternion.Euler (snapshot.Rotation);
			_takeScreenshot = true;
		}
	}

	void OnPostRender(){
		if (_takeScreenshot){
			RenderTexture prevRT = RenderTexture.active;
			RenderTexture.active = _renderTexture;
	
			var tex = new Texture2D(_renderTexture.width, _renderTexture.height);
			tex.ReadPixels(new Rect(0, 0, tex.width, tex.height), 0, 0);
			tex.Apply(false);
			_snapshots [_snapIdx].Texture = tex;

			initNextSnap ();
		}
	}
}
