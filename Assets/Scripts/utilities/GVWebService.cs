﻿//#define NEWSITE
#define LOGIN_POST

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Ucss;
using SimpleJSON;
using JsonFx.Json;
using Assets.Scripts.models;

public class GVWebService{
	//New urls
	//endPoint https://newsite.marshalls-etest.co.uk/garden-visualiser/services/api/
	//resources https://newsite.marshalls-etest.co.uk/garden-visualiser/ 
	//Previous
	//endPoint https://www.marshalls.co.uk/test-visualizer/services/api/
	//resources https://www.marshalls.co.uk/test-visualizer/dev/
#if (NEWSITE) 
	private static string _gvResourcePath = "https://newsite.marshalls-etest.co.uk/garden-visualiser/";
	private static string _gvEndPoint = _gvResourcePath + "services/api/";

	public static string EndPoint{
		get{
			return _gvEndPoint;
		}
	}
	public static string ResourcePath{
		get{
			return _gvResourcePath;
		}
	}
#else
	private static string _gvResourcePath = "https://www.marshalls.co.uk/garden-visualiser/";
	private static string _gvEndPoint = _gvResourcePath + "services/api/";

	public static string EndPoint{
	get{
	return _gvEndPoint;
	}
	}
	public static string ResourcePath{
	get{
	return _gvResourcePath;
	}
	}
#endif
	private static string _token;
	public static string Token{
		get{
			return _token;
		}
	}
	private static string _email;
	private static string _password;
	private static string _tempEmail;
	private static string _tempPassword;
	private static GV_UI _gv_ui;
	static bool _saveProject;
	static Project _project;
	private static List<string>_cloneGuids;
	public static List<string>CloneGuids{
		set{
			_cloneGuids = value;
			if (_cloneGuids.Count>0){
				CloneProject(_cloneGuids[0]);
			}
		}
	}
	static float _plannerVersion = 2.0f;
	static InstallerStockistController _isController;
	static bool _showProjectsPanel = true;//Default

	public static void init(){
		if (Application.isWebPlayer){
			Debug.Log("WebPlayer application path is " + Application.absoluteURL);
		}
	}

	static GV_UI GV_UI{
		get{
			if (_gv_ui==null){
				GameObject go = GameObject.Find ("Canvas");
				_gv_ui = go.GetComponent<GV_UI>();
			}
			return _gv_ui;
		}
	}
	private static ProjectsController _projectsController;
	static ProjectsController ProjectsController{
		get{
			if (_projectsController==null){
				_projectsController = GV_UI.ProjectsController;
			}
			return _projectsController;
		}
	}

	public static bool Busy {
		set {
			if (GV_UI != null)
				_gv_ui.ShowLoading (value);
		}
	}

	private static SettingsController _settingsController;
	static SettingsController SettingsController{
		get{
			if (_settingsController==null){
				GameObject go = GameObject.Find ("Canvas/ModalPanels/SettingsPanel");
				_settingsController = go.GetComponent<SettingsController>();
			}
			return _settingsController;
		}
	}
	static GardenScript _gardenScript;
	public static GardenScript GardenScript{
		get{
			if (_gardenScript==null){
				GameObject go = GameObject.Find ("3D");
				_gardenScript = go.GetComponent<GardenScript>();
			}
			return _gardenScript;
		}
		set{
			_gardenScript = value;
		}
	}

	static AddHouseController _addHouseController;
	ThumbnailController _thumbnailController = null;
	static ForgotController _forgotController = null;

	static DesignController _designController = null;
	static int _loginCount = 0;

	public static void LoadProductsConfig()
	{
		string url = ResourcePath + "data/products.json";
		Debug.Log("GVWebService.LoadProductsConfig: url:" + url);
		
		UCSS.HTTP.GetString ( url, GVWebService.LoadProductsConfigCallback, GVWebService.LoadProductsConfigError);
	}

	static void LoadProductsConfigError(string error, string id){
		Debug.Log("GVWebService.LoadProductsConfig download error:" + error);
	}

	static void LoadProductsConfigCallback(string str, string id){
		Debug.Log("GVWebService.LoadProductsConfig products:" + str);
		GardenScript.ProductsJSON = JSON.Parse(str);
		LoadPatterns();
	}

	public static void LoadPatterns()
	{
		string url = ResourcePath + "data/patterns.json";
		Debug.Log("GVWebService.LoadPatterns: url:" + url);
		
		UCSS.HTTP.GetString ( url, GVWebService.LoadPatternsCallback, GVWebService.LoadPatternsError);
	}
	
	static void LoadPatternsError(string error, string id){
		Debug.Log("GVWebService.LoadPatterns download error:" + error);
	}
	
	static void LoadPatternsCallback(string str, string id){
		Debug.Log("GVWebService.LoadPatterns products:" + str);
		Pattern.PatternsJSON = JSON.Parse(str);
		Pattern.ResourcePath = GVWebService.ResourcePath;
		LoadFeatures();
	}

	public static void LoadFeatures()
	{
		string url = ResourcePath + "data/features.json";
		Debug.Log("GVWebService.LoadFeatures: url:" + url);
		
		UCSS.HTTP.GetString ( url, GVWebService.LoadFeaturesCallback, GVWebService.LoadFeaturesError);
	}
	
	static void LoadFeaturesError(string error, string id){
		Debug.Log("GVWebService.LoadFeatures download error:" + error);
	}
	
	static void LoadFeaturesCallback(string str, string id){
		Debug.Log("GVWebService.LoadFeatures products:" + str);
		Feature.FeaturesJSON = JSON.Parse(str);
		Feature.ResourcePath = GVWebService.ResourcePath;
	}

	public static void LogVisit()
	{	
		string url = _gvEndPoint + "accesslog?logtype=visit";
		Debug.Log("GVWebService LogVisit url:" + url);
		Dictionary<string,string> headers = new Dictionary<string,string>();
		headers.Add ("X-HTTP-Method-Override", "POST");
		byte[] postData = new byte[1];
		UCSS.HTTP.PostBytes ( url, postData, headers, new EventHandlerHTTPString(GVWebService.LogVisitCallback), new EventHandlerServiceError(GVWebService.LogVisitError));
	}

	static void LogVisitError(string error, string id){
		Debug.Log("GVWebService.LogVisit download error:" + error);
	}

	static void LogVisitCallback(string str, string id){
		Debug.Log("GVWebService.LogVisit returned " + str);
	}

	#if (LOGIN_POST)
	public static void LoginHandler(string username, string password)
	{	
		string url = _gvEndPoint + "Login";
		Debug.Log("GVWebService LoginHandler url:" + url + " username:" + username + " pword:" + password);

		Dictionary<string,string> headers = new Dictionary<string,string>();
		headers.Add("Content-Type", "application/json");
		headers.Add("Accept", "application/json");
		headers.Add ("X-HTTP-Method-Override", "POST");

		JsonDataWriter writer = new JsonDataWriter(new JsonWriterSettings());

		LoginAccountModel accountDetails = new LoginAccountModel
		{
			UserName = username,
			Password = password
		};

		_email = username;
		_password = password;

		byte[] postData = writer.SerializeToByteArray(accountDetails);

		UCSS.HTTP.PostBytes(url, postData, headers, new EventHandlerHTTPString(GVWebService.LoginCallback), new EventHandlerServiceError(GVWebService.LoginError));

		if (GV_UI) _gv_ui.Busy = true;
	}
	#else
	public static void LoginHandler(string username, string password)
	{	
		string url = _gvEndPoint + "Login?username=" + username + "&password=" + password;
		Debug.Log("GVWebService LoginHandler url:" + url + " username:" + username + " pword:" + password);

		_email = username;
		_password = password;
		if (GV_UI) _gv_ui.Busy = true;

		UCSS.HTTP.GetString ( url, GVWebService.LoginCallback, GVWebService.LoginError);
	}
	#endif

	static void LoginError(string error, string id){
		Debug.Log("GVWebService.Login download error:" + error);
		if (error.StartsWith ("409")) {
			ForgottenPassword (null);
		}else if (error.StartsWith("404")){
			GV_UI.ShowError (error.Substring(4));
			if (GV_UI)
				_gv_ui.Busy = false;
			_loginCount = 0;
		}else if (_loginCount < 3) {
			_loginCount++;
			LoginHandler (_email, _password);
		} else {
			GV_UI.ShowError (string.Format ("There was a problem {0}", error));
			if (GV_UI)
				_gv_ui.Busy = false;
			_loginCount = 0;
		}
	}
	
	static void LoginCallback(string str, string id){
		Debug.Log("GVWebService.Login returned " + str);
		_loginCount = 0;
		JSONNode json = JSON.Parse(str);
		int responseCode = json["ResponseStatusCode"].AsInt;
		switch(responseCode){
		case 200:
			_token = json["Token"];
			PlayerPrefs.SetString("email", _email);
			PlayerPrefs.SetString("password", _password);
			LoadProjects();
			break;
		case 401:
			GV_UI.ShowAlert("Login", "Sorry this password is incorrect.");
			if (GV_UI) _gv_ui.Busy = false;
			break;
		case 404:
			GV_UI.ShowAlert("Login", "Sorry this account does not exist.\nPlease create a new account.");
			if (GV_UI) _gv_ui.Busy = false;
			break;
		case 409:
			ForgottenPassword (null);
			break;
		default:
			GV_UI.ShowAlert("Login", "Sorry there is an error with the email or the password.");
			if (GV_UI) _gv_ui.Busy = false;
			break;
		}
	}

	public static void LoadProjects(bool show=true){
		string url = string.Format("{0}Project?authentication-token={1}&thumbWidth={2}&thumbHeight={3}&thumbResizeMode=4", _gvEndPoint, _token, 416, 336);
		Debug.Log("GVWebService.LoadProject url:" + url);
		
		UCSS.HTTP.GetString ( url, GVWebService.LoadProjectsCallback, GVWebService.LoadProjectsError);
		_showProjectsPanel = show;
	}
		
	static void LoadProjectsError(string error, string id){
		Debug.Log("GVWebService.LoadProjects download error:" + error);
		if (GV_UI) _gv_ui.Busy = false;
	}
	
	static void LoadProjectsCallback(string str, string id){
		Debug.Log("GVWebService.LoadProjects returned " + str);
		JSONNode json = JSON.Parse(str);
		int responseCode = json["ResponseStatusCode"].AsInt;
		if (GV_UI) _gv_ui.Busy = false;
		switch(responseCode){
		case 0:
			if (ProjectsController) _projectsController.UpdatePanel(json, _showProjectsPanel);
			_showProjectsPanel = true;//Default
			break;
		default:
			GV_UI.ShowAlert("Projects", json["Message"]);
			break;
		}
	}

	public void GetThumbnail(string url, ThumbnailController controller){
		//Debug.Log ("GVWebService.GetThumbnail url:" + url);
		_thumbnailController = controller;
		
		UCSS.HTTP.GetBytes (url, new EventHandlerHTTPBytes(this.GetThumbnailCallback), new EventHandlerServiceError(this.GetThumbnailError));
	}
	
	void GetThumbnailError(string error, string id){
		_thumbnailController.WSError("GVWebService.GetThumbnail download error:" + error);
	}
	
	void GetThumbnailCallback(byte[] bytes, string id){
		Texture2D tex = new Texture2D(2, 2);
		if (!tex.LoadImage(bytes)) {
			_thumbnailController.WSError("GVWebService.GetThumbnail Failed loading image data!");
		}else {
			tex.Apply ();
			//Debug.Log(String.Format ("GVWebService.GetThumbnail LoadImage - size:{0}x{1} bytes length:{2}", tex.width, tex.height, bytes.Length ));
		}
		_thumbnailController.Thumbnail = tex;
	}

	public static void CreateAccount(string firstname, string lastname, string username, string password)
	{
		if (firstname.Equals(""))
		{
			GV_UI.ShowAlert("ERROR", "Please enter your firstname");
			return;
		}
		else if (lastname.Equals(""))
		{
			GV_UI.ShowAlert("ERROR", "Please enter your lastname");
			return;
		}
		else if (username.Equals(""))
		{
			GV_UI.ShowAlert("ERROR", "Please enter your email address");
			return;
		}
		else if (password.Equals(""))
		{
			GV_UI.ShowAlert("ERROR", "Please enter a password");
			return;
		}
		
		string url = _gvEndPoint + "Account";
		
		Debug.Log("LoginScript.CreateAccount");
		
		Dictionary<string,string> headers = new Dictionary<string,string>();
		headers.Add("Content-Type", "application/json");
		headers.Add("Accept", "application/json");
		headers.Add("X-Test-Custom-Header", "Hello Rich!");
		
		JsonDataWriter writer = new JsonDataWriter(new JsonWriterSettings());
		
		CreateAccountModel accountDetails = new CreateAccountModel
		{
			UserName = username,
			Password = password,
			FirstName = firstname,
			LastName = lastname
		};

		_tempEmail = username;
		_tempPassword = password;

		byte[] postData = writer.SerializeToByteArray(accountDetails);
		
		UCSS.HTTP.PostBytes(url, postData, headers, new EventHandlerHTTPString(GVWebService.CreateAccountCallback), new EventHandlerServiceError(GVWebService.CreateAccountError));
	}

	static void CreateAccountError(string error, string id){
		Debug.Log("GVWebService.CreateAccount download error:" + error);
		GV_UI.ShowError ("There was a problem creating your account. Please try later");
		_email = _password = "";
	}
	
	static void CreateAccountCallback(string data, string id){
		Debug.Log("GVWebService.CreateAccount data:" + data);
		JSONNode json = JSON.Parse(data);

		int responseCode = json["ResponseStatusCode"].AsInt;
		string message = json ["ResponseStatusMessage"].ToString ();
		switch(responseCode){
		case 200:
			_token = json["Token"];
			_email = _tempEmail;
			_password = _tempPassword;
			PlayerPrefs.SetString("email", _email);
			PlayerPrefs.SetString("password", _password);
			LoadProjects();
			GV_UI.ShowModalPanel ("NewProject");
			break;
		case 409:
			GV_UI.ShowAlert("Create Account", message);
			break;
		default:
			GV_UI.ShowAlert("Create Account", message);
			break;
		}
		if (GV_UI) _gv_ui.Busy = false;
	}
	
	public static bool UpdateAccount(string firstname, string lastname, string username, string password)
	{	
		if (firstname.Equals(""))
		{
			GV_UI.ShowAlert("ERROR", "Please enter your firstname");
			return false;
		}
		else if (lastname.Equals(""))
		{
			GV_UI.ShowAlert("ERROR", "Please enter your lastname");
			return false;
		}
		else if (username.Equals(""))
		{
			GV_UI.ShowAlert("ERROR", "Please enter your email address");
			return false;
		}
		else if (password.Equals(""))
		{
			GV_UI.ShowAlert("ERROR", "Please enter a password");
			return false;
		}

		string url;
		
		url = _gvEndPoint + "Account";
		
		Debug.Log("LoginScript.UpdateAccount");
		
		Dictionary<string,string> headers = new Dictionary<string,string>();
		headers.Add("Content-Type", "application/json");
		headers.Add("Accept", "application/json");
		headers.Add ("Authentication-Token", _token);
		headers.Add ("X-HTTP-Method-Override", "PUT");
		
		JsonDataWriter writer = new JsonDataWriter(new JsonWriterSettings());
		
		CreateAccountModel accountDetails = new CreateAccountModel
		{
			UserName = username,
			Password = password,
			FirstName = firstname,
			LastName = lastname
		};

		_tempEmail = username;
		_tempPassword = password;
		
		byte[] postData = writer.SerializeToByteArray(accountDetails);
		
		UCSS.HTTP.PostBytes(url, postData, headers, new EventHandlerHTTPString(GVWebService.UpdateAccountCallback), new EventHandlerServiceError(GVWebService.UpdateAccountError));
		if (GV_UI) _gv_ui.Busy = true;

		return true; 
	}

	static void UpdateAccountError(string error, string id){
		Debug.Log("GVWebService.UpdateAccount download error:" + error);
		GV_UI.ShowAlert ("ERROR", "There was a problem updating your account details");
		if (GV_UI) _gv_ui.Busy = false;
	}
	
	static void UpdateAccountCallback(string data, string id){
		Debug.Log("GVWebService.UpdateAccount data:" + data);
		//Save revised info
		_email = _tempEmail;
		_password = _tempPassword;
		PlayerPrefs.SetString("email", _email);
		PlayerPrefs.SetString("password", _password);
		if (GV_UI) _gv_ui.Busy = false;
		GV_UI.ShowAlert ("Update", "Your account details have been updated");
	}

	public static void GetAccount()
	{
		if (_token==null || _token.Equals(""))
		{
			GV_UI.ShowAlert("ERROR", "You need to be logged in to use this function");
			return;
		}
		
		string url = _gvEndPoint + string.Format("Account?authentication-token={0}", _token);
		
		Debug.Log("GVWebService.GetAccount " + url);

		if (GV_UI) _gv_ui.Busy = true;
		
		UCSS.HTTP.GetString ( url, GVWebService.GetAccountCallback, GVWebService.GetAccountError);
	}

	static void GetAccountError(string error, string id){
		Debug.Log("GVWebService.GetAccount download error:" + error);
		GV_UI.ShowAlert("ERROR", "There was a problem accessing your account details. Please try later.");
		if (GV_UI) _gv_ui.Busy = false;
	}
	
	static void GetAccountCallback(string data, string id){
		Debug.Log("GVWebService.GetAccount data:" + data);
		JSONNode json = JSON.Parse(data);
		if (SettingsController!=null) _settingsController.UpdatePanel(_email, _password, json["FirstName"], json["LastName"]);
		if (GV_UI) _gv_ui.Busy = false;
	}
	
	public static void SaveNewProject(Project project)
	{
		string xml = project.ToXML();
		Debug.Log("GVWebService.SaveNewProject xml:" + xml);
		
		xml = xml.Replace("\n", "");
		xml = xml.Replace("\t", "");
		xml = xml.Replace("\"", "\\\"");
		
		string url;
		
		JsonDataWriter writer = new JsonDataWriter(new JsonWriterSettings());
		
		CreateNewProjectModel projectDetails = new CreateNewProjectModel
		{
			PlannerVersion = _plannerVersion,
			Name = project.Name,
			Size = project.Size,
			Xml = project.ToXML(),
			ProjectImageFileContent = project.Thumbnail
		};
		
		byte[] postData = writer.SerializeToByteArray(projectDetails);

		url = _gvEndPoint + "Project";
		
		Debug.Log("GVWebService.SaveNewProject data:" + writer.SerializeToString(projectDetails));
		Dictionary<string,string> headers = new Dictionary<string,string>();
		headers.Add("Content-Type", "application/json");
		headers.Add("Accept", "application/json");
		headers.Add ("Authentication-Token", _token);
		headers.Add ("X-HTTP-Method-Override", "POST");
		
		UCSS.HTTP.PostBytes(url, postData, headers, new EventHandlerHTTPString(GVWebService.SaveNewProjectCallback), new EventHandlerServiceError(GVWebService.SaveNewProjectError));
		_project = project;

		if (GV_UI) _gv_ui.Busy = true;
	}

	static void SaveNewProjectError(string error, string id){
		Debug.Log("GVWebService.SaveNewProject download error:" + error);
		if (GV_UI) _gv_ui.Busy = false;
	}
	
	static void SaveNewProjectCallback(string data, string id){
		Debug.Log("GVWebService.SaveNewProject data:" + data);
		_project.Guid = data;
		if (GV_UI) _gv_ui.Busy = false;
		LoadProjects(false);
		Analytics.ga ("Project", "New project");
	}

	public static void SaveProject(Project project, DesignController designController=null, bool showBusy=true)
	{
		//string guid = (project.Guid.Equals("")) ? "" : ("\"Guid\": \"" + project.Guid + "\",");
		string xml = project.ToXML();
		Debug.Log("GVWebService.SaveProject xml:" + xml);
		
		xml = xml.Replace("\n", "");
		xml = xml.Replace("\t", "");
		xml = xml.Replace("\"", "\\\"");
		
		string url;
		//project.Photos.Add ("04008dfe-c787-491c-8de5-cade0a4ff5d5");
		
		JsonDataWriter writer = new JsonDataWriter(new JsonWriterSettings());
		List<ProjectPhoto> photos = new List<ProjectPhoto>();
		foreach (string str in project.Photos) {
			ProjectPhoto photo = new ProjectPhoto (str);
			if (photo.Guid!=null) photos.Add (photo);
		}
		
		CreateProjectModel projectDetails = new CreateProjectModel
		{
			Guid = project.Guid,
			PlannerVersion = _plannerVersion,
			Name = project.Name,
			Size = project.Size,
			Xml = project.ToXML(),
			ProjectImageFileContent = project.Thumbnail,
			ProjectPhotos = photos
		};
		
		Debug.Log ("GVWebService.SaveProject photos:" + writer.SerializeToString(photos));
		byte[] postData = writer.SerializeToByteArray(projectDetails);
		
		url = _gvEndPoint + "Project";
		
		Debug.Log("GVWebService.SaveProject data:" + postData);
		Dictionary<string,string> headers = new Dictionary<string,string>();
		headers.Add("Content-Type", "application/json");
		headers.Add("Accept", "application/json");
		headers.Add ("Authentication-Token", _token);
		headers.Add ("X-HTTP-Method-Override", "PUT");

		_designController = designController;
		UCSS.HTTP.PostBytes(url, postData, headers, new EventHandlerHTTPString(GVWebService.SaveProjectCallback), new EventHandlerServiceError(GVWebService.SaveProjectError));
		_project = project;
		
		if (GV_UI && showBusy) _gv_ui.Busy = true;
		if (!showBusy && _designController != null && _designController.Busy != null)
			_designController.Busy.Show ();
	}

	static void SaveProjectError(string error, string id){
		Debug.Log("GVWebService.SaveProject download error:" + error);
		if (GV_UI) _gv_ui.Busy = false;
		if (_designController != null) {
			if (_designController.Closing)
				_designController.BackAfterSave ();
			_designController.Busy.Hide ();
		}
		_designController = null;
	}
	
	static void SaveProjectCallback(string data, string id){
		Debug.Log("GVWebService.SaveProject data:" + data);
		if (GV_UI) _gv_ui.Busy = false;
		Analytics.ga ("Project", "Save project");
		if (_designController != null) {
			if (_designController.Closing)
				_designController.BackAfterSave ();
			_designController.Busy.Hide ();
		}
		_designController = null;
	}

	public static void DeleteProjects(List<string> guids)
	{
		string url;

		JsonDataWriter writer = new JsonDataWriter(new JsonWriterSettings());
		byte[] postData = writer.SerializeToByteArray(guids);
		
		url = _gvEndPoint + "Project";
		
		Debug.Log("LoginScript.DeleteProjects data:" + writer.SerializeToString(guids));
		Dictionary<string,string> headers = new Dictionary<string,string>();
		headers.Add("Content-Type", "application/json");
		headers.Add("Accept", "application/json");
		headers.Add("Authentication-Token", _token);
		headers.Add ("X-HTTP-Method-Override", "DELETE");
		
		UCSS.HTTP.PostBytes(url, postData, headers, new EventHandlerHTTPString(GVWebService.DeleteProjectsCallback), new EventHandlerServiceError(GVWebService.DeleteProjectsError));
		if (GV_UI) _gv_ui.Busy = true;
	}

	static void DeleteProjectsError(string error, string id){
		Debug.Log("GVWebService.DeleteProjects download error:" + error);
		if (GV_UI) _gv_ui.Busy = false;
	}
	
	static void DeleteProjectsCallback(string data, string id){
		Debug.Log("GVWebService.DeleteProjects data:" + data);
		if (GV_UI) _gv_ui.Busy = false;
	}
	
	static byte[] GetBytes(string str)
	{
		byte[] bytes = new byte[str.Length * sizeof(char)];
		System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
		return bytes;
	}

	public static void GetInstallers(string postcode, bool installers, InstallerStockistController controller)
	{
		if (_token==null || _token.Equals(""))
		{
			GV_UI.ShowAlert("ERROR", "You need to be logged in to use this function");
			return;
		}
		
		postcode = postcode.Replace (" ", "%20");
		string service = (installers) ? "Installer" : "Stockist";
		string url = _gvEndPoint + string.Format("Find{0}?postcode={1}&authentication-token={2}", service, postcode, _token);
		
		Debug.Log("GVWebService.GetInstallers " + url);
		UCSS.HTTP.GetString ( url, GVWebService.GetInstallersCallback, GVWebService.GetInstallersError);
		_isController = controller;
		if (GV_UI) _gv_ui.Busy = true;
	}

	static void GetInstallersError(string error, string id){
		Debug.Log("GVWebService.GetInstallers download error:" + error);
		GV_UI.ShowError ("Problem getting the information");
		if (GV_UI) _gv_ui.Busy = false;
	}
	
	static void GetInstallersCallback(string data, string id){
		Debug.Log("GVWebService.GetInstallers data:" + data);
		JSONNode json = JSON.Parse(data);
		if (_isController!=null){
			_isController.UpdateList(json.AsArray);
			_isController = null;
		}
		if (GV_UI) _gv_ui.Busy = false;
	}

	public static void CloneProject(string guid){
		//Load the project first
		string url;

		_cloneGuids.Remove (guid);
		
		url = _gvEndPoint + string.Format("Project?authentication-token={0}&projectId={1}", _token, guid);
		Debug.Log("LoginScript.CloneProject url:" + url + " token:" + _token);
		
		UCSS.HTTP.GetString ( url, GVWebService.CloneProjectCallback, GVWebService.CloneProjectError);
	}

	static void CloneProjectError(string error, string id){
		Debug.Log("GVWebService.CloneProject download error:" + error);
		GV_UI.ShowAlert("ERROR", error);
	}
	
	static void CloneProjectCallback(string data, string id){
		Debug.Log("GVWebService.CloneProject data:" + data);
		JSONNode json = JSON.Parse(data);
		_project = new Project(json["Xml"]);
		_project.Guid = "";
		_project.Name = json["Name"] + " - Copy";
		_project.Size = json["Size"].AsFloat;
		_project.Thumbnail = _projectsController.GetBase64FromGuid(json["Guid"]);
		
		//Save the project to get a new Guid
		SaveCloneProject(_project);
		if (GV_UI) _gv_ui.Busy = false;
	}
	
	public static void SaveCloneProject(Project project)
	{
		string xml = project.ToXML();
		Debug.Log("LoginScript.SaveCloneProject xml:" + xml);
		
		xml = xml.Replace("\n", "");
		xml = xml.Replace("\t", "");
		xml = xml.Replace("\"", "\\\"");
		
		string url;
		
		JsonDataWriter writer = new JsonDataWriter(new JsonWriterSettings());
		
		CreateNewProjectModel projectDetails = new CreateNewProjectModel
		{
			PlannerVersion = _plannerVersion,
			Name = project.Name,
			Size = project.Size,
			Xml = project.ToXML(),
			ProjectImageFileContent = project.Thumbnail
		};
		
		byte[] postData = writer.SerializeToByteArray(projectDetails);

		url = _gvEndPoint + "Project";
		
		Debug.Log("LoginScript.SaveCloneProject data:" + writer.SerializeToString(projectDetails));
		Dictionary<string,string> headers = new Dictionary<string,string>();
		headers.Add("Content-Type", "application/json");
		headers.Add("Accept", "application/json");
		headers.Add ("Authentication-Token", _token);
		headers.Add ("X-HTTP-Method-Override", "POST");
		
		UCSS.HTTP.PostBytes(url, postData, headers, new EventHandlerHTTPString(GVWebService.SaveCloneCallback), new EventHandlerServiceError(GVWebService.SaveCloneError));
		if (GV_UI) _gv_ui.Busy = true;
	}

	static void SaveCloneError(string error, string id){
		Debug.Log("GVWebService.SaveClone download error:" + error);
		GV_UI.ShowAlert("ERROR", error);
	}
	
	static void SaveCloneCallback(string data, string id){
		Debug.Log("GVWebService.SaveClone data:" + data);
		_project.Guid = data;
		if (_cloneGuids.Count>0){
			CloneProject(_cloneGuids[0]);
		}else{
			LoadProjects(false);
		}

		if (GV_UI) _gv_ui.Busy = false;
	}
	
	public static void GetProject(string guid)
	{
		string url;

		Debug.Log (string.Format("GetProject guid:{0}", guid));

		url = _gvEndPoint + string.Format("Project?authentication-token={0}&projectId={1}", _token, guid);
		Debug.Log("LoginScript GetProject url:" + url + " token:" + _token);
		
		UCSS.HTTP.GetString ( url, GVWebService.GetProjectCallback, GVWebService.GetProjectError);

		if (GV_UI) _gv_ui.Busy = true;
	}

	static void GetProjectError(string error, string id){
		Debug.Log("GVWebService.GetProject download error:" + error);
		GV_UI.ShowAlert("ERROR", error);
		if (GV_UI) _gv_ui.Busy = false;
	}
	
	static void GetProjectCallback(string data, string id){
		Debug.Log("GVWebService.GetProject data:" + data);
		JSONNode json = JSON.Parse(data);
		_project = new Project(json["Xml"]);
		_project.Guid = json["Guid"];
		_project.Name = json["Name"];
		_project.Size = json["Size"].AsFloat;
		JSONArray photos = (JSONArray)json["ProjectPhotos"];
		_project.Photos = new List<string>();
		for(int i=0; i<photos.Count; i++){
			JSONNode photo = photos[i];
			_project.Photos.Add (photo["Guid"]);
		}

		GV_UI.GardenScript.InitGarden(_project);
		
		if (GV_UI){
			//_gv_ui.Busy = false;
			GV_UI.ShowDesignPanel(true);
			GV_UI.ShowMainBg (false);
			GV_UI.DesignController.SetFooterState ("edit");
		}

		Analytics.ga ("Project", "Open project");
	}

	public static void SendPDF(string email){
		if (_token == null || _token.Equals ("")) {
			GV_UI.ShowAlert ("ERROR", "You need to be logged in to use this function");
			return;
		}

		Debug.Log ("GVWebService.SendEmail");

		JsonDataWriter writer = new JsonDataWriter (new JsonWriterSettings ());

		string[] shots = null;

		if (GardenScript.Project.Shots != null && GardenScript.Project.Shots.Count > 0) {
			shots = new string[GardenScript.Project.Shots.Count];
			int idx = 0;
			foreach (Snapshot snapshot in GardenScript.Project.Shots) {
				shots [idx] = snapshot.Base64;
				idx++;
			}
		}

		ProjectPDF projectPDF = new ProjectPDF {
			ProjectId = GardenScript.Project.Guid,
			EmailAddress = email,
			ProjectShots = shots
		};

		byte[] postData = writer.SerializeToByteArray (projectPDF);

		_gv_ui.Busy = true;

		string url = _gvEndPoint + "PDF";

		Debug.Log ("LoginScript.GeneratePDF data:" + postData);
		Dictionary<string,string> headers = new Dictionary<string,string>();
		headers.Add ("Content-Type", "application/json");
		headers.Add ("Accept", "application/json");
		headers.Add ("Authentication-Token", _token);
		headers.Add ("X-HTTP-Method-Override", "POST");

		UCSS.HTTP.PostBytes (url, postData, headers, new EventHandlerHTTPString (GVWebService.SendPDFCallback), new EventHandlerServiceError (GVWebService.SendPDFError));

	}

	static void SendPDFError(string data, string id){
		_gv_ui.Busy = false;
		GV_UI.ShowAlert ("Warning", "There was a problem please try later");
		Debug.Log("GVWebService.SendPDF download error:" + data);
	}

	static void SendPDFCallback(string data, string id){
		_gv_ui.Busy = false;
		GV_UI.ShowAlert ("Info", "Look out for the PDF in your email");
	}

	public static void UploadImage(byte[] bytes, AddHouseController controller=null){
		_addHouseController = controller;
		WWWForm form = new WWWForm();
		form.AddBinaryData("FileContent", bytes, "projectimage.png", "image/png");

		// Upload to a cgi script
		string url = string.Format ("{0}ProjectPhoto?authentication-token={1}", _gvEndPoint, _token);
		Debug.Log ("GVWebService.UploadImage " + url);
		UCSS.HTTP.PostForm ( url, form, GVWebService.UploadHouseImageCallback, GVWebService.UploadImageError);

		if (GV_UI) _gv_ui.Busy = true;
	}

	static void UploadHouseImageCallback(string data, string id){
		Debug.Log("GVWebService.UploadImage data:" + data);
		if (_addHouseController!=null) _addHouseController.FileSelectedCallback (data);
	}
		
	public static void UploadImage(byte[] bytes, bool saveProject=false){
		// Create a Web Form
		WWWForm form = new WWWForm();
		form.AddBinaryData("FileContent", bytes, "projectimage.png", "image/png");
		
		// Upload to a cgi script
		string url = string.Format ("{0}ProjectPhoto?authentication-token={1}", _gvEndPoint, _token);
		Debug.Log ("GVWebService.UploadImage " + url);
		UCSS.HTTP.PostForm ( url, form, GVWebService.UploadImageCallback, GVWebService.UploadImageError);

		_saveProject = saveProject;
		if (GV_UI) _gv_ui.Busy = true;
	}

	static void UploadImageError(string error, string id){
		Debug.Log("GVWebService.UploadImage download error:" + error);
		GV_UI.ShowAlert("ERROR", error);
		if (GV_UI) _gv_ui.Busy = false;
		_saveProject = false;
	}
	
	static void UploadImageCallback(string data, string id){
		Debug.Log("GVWebService.UploadImage data:" + data);
		JSONNode json = JSON.Parse(data);
		if (json["Url"]!=null){
			if (_saveProject){
				_project = GardenScript.Project;
				//_project.HousePattern = json["Url"];
				_project.Photos.Add (json["Guid"]);
				SaveNewProject(_project);
				_saveProject = false;
				if (GardenScript) _gardenScript.InitGarden(_project);
				GV_UI.ShowDesignPanel (true);
				GameObject go = GameObject.Find ("Canvas/ModalPanels/PhotoPanel");
				PhotoController pc = go.GetComponent<PhotoController>();
				pc.Show (true);
			}
		}
	}

	public static void LoadProjectImage(string url){
		Debug.Log ("GVWebService.LoadProjectImage url:" + url);

		UCSS.HTTP.GetBytes (url, new EventHandlerHTTPBytes(GVWebService.LoadProjectImageCallback), new EventHandlerServiceError(GVWebService.LoadProjectImageError));
	}
	
	static void LoadProjectImageError(string error, string id){
		GV_UI.ShowAlert("ERROR", error);
	}
	
	static void LoadProjectImageCallback(byte[] bytes, string id){
		Texture2D tex = new Texture2D(2, 2);
		if (!tex.LoadImage(bytes)) {
			GV_UI.ShowAlert("ERROR", "GVWebService.GetThumbnail Failed loading image data!");
		}else {
			tex.Apply ();
			//Debug.Log(String.Format ("GVWebService.GetThumbnail LoadImage - size:{0}x{1} bytes length:{2}", tex.width, tex.height, bytes.Length ));
		}
		//GV_UI.NewProjectImage(tex);
	}

	public static void ForgottenPassword(ForgotController forgotController)
	{
		_forgotController = forgotController;
		string url;
		if (forgotController == null) {
			url = string.Format ("{0}ForgottenPassword?username={1}", _gvEndPoint, _email);
		} else {
			url = string.Format ("{0}ForgottenPassword?username={1}", _gvEndPoint, _forgotController.UserName);
		}
		Debug.Log("GVWebService.ForgottenPassword: url:" + url);

		UCSS.HTTP.GetString ( url, GVWebService.ForgottenPasswordCallback, GVWebService.ForgottenPasswordError);
	}

	static void ForgottenPasswordError(string error, string id){
		Debug.Log("GVWebService.ForgottenPassword download error:" + error);
		GV_UI.ShowAlert ("Warning", "There was a problem, please try later");
		_forgotController = null;
	}

	static void ForgottenPasswordCallback(string str, string id){
		Debug.Log("GVWebService.ForgottenPassword products:" + str);
		if (_forgotController != null) { 
			_forgotController.ForgottenPasswordSuccess ();
		} else {
			GV_UI.ShowAlert ("Information", string.Format("We have sent an email to {0} with a link that will allow you to create a new password for this account.", _email));
			GV_UI.Busy = false;
		}
		_forgotController = null;
	}
}
