using UnityEngine;
using System;
using System.Collections;

public class CameraScript : MonoBehaviour
{
	public float minHeight = 0.2f;

	GameObject _player;
	GameObject _dummy;
	GameObject _target;
	Vector2 _constraints;
	bool _lookAtZero;
	int _cameraID;
	Vector3 _viewZero;
	bool _interpolate;
	bool _useMouseMove;
	Vector2 _mouseInfo;
	float _constraintRadius;
	GameObject _debugCube;
	Vector3 _storedPosition, _storedRotation;
	bool _saveProject;
	bool _takeScreenShot;
	GardenScript _gardenScript;
	RenderTexture renderTexture;
	RenderTexture _storedRT;
	Vector3 _dummyOrgPos;
	Quaternion _playerOrgRot;
	Quaternion _dummyOrgRot;

	public bool UseMouseMove {
		get {
			return _useMouseMove;
		}
		set {
			_useMouseMove = value;
		}
	}

	public Vector2 Constraints {
		get {
			return _constraints;
		}
		set {
			Debug.Log ("CameraScript.Constraints " + value.ToString());
			_constraints = value;
			_constraintRadius = (float)(Math.Max ( value.x, value.y) * 1.6);
			Vector3 dummyPos = _dummy.transform.position;
			Quaternion dummyRot = _dummy.transform.rotation;
			Vector3 playerPos = _player.transform.position;
			Quaternion playerRot = _player.transform.rotation;
			_player.transform.position = Vector3.zero;
			_dummy.transform.position = Vector3.zero;
			float size = Math.Max(_constraints.x, _constraints.y);
			_viewZero = new Vector3(0.0f, 2.5f, 0.0f);
			_dummy.transform.Translate (0.0f, 2.5f, -size*0.9f);
			_dummy.transform.LookAt (_viewZero);
			_dummyOrgPos = _dummy.transform.position;
			_dummyOrgRot = _dummy.transform.rotation;
			_player.transform.position = playerPos;
			_dummy.transform.position = dummyPos;
			_player.transform.rotation = playerRot;
			_dummy.transform.rotation = dummyRot;
		}
	}

	bool _interactive = false;
	public bool Interactive {
		get {
			return _interactive;
		}
	}

	bool _overHead = true;
	public bool Overhead {
		get {
			return _overHead;
		}
	}

	// Use this for initialization
	void Start ()
	{
		_player = new GameObject();
		_dummy = new GameObject();
		_target = new GameObject();
		_player.name = "Player";
		_dummy.name = "Dummy";
		_target.name = "Target";
		_viewZero = new Vector3(0.0f, 2.5f, 0.0f);
		_dummy.transform.parent = _player.transform;
		_dummy.transform.Translate (0.0f, 2.5f, -10.0f);
		_dummy.transform.LookAt (_viewZero);
		_debugCube = GameObject.Find ("Cube");
		_saveProject = false;
		_takeScreenShot = false;
		if (_debugCube!=null) _debugCube.transform.parent = _player.transform;
		_dummyOrgPos = _dummy.transform.position;
		_playerOrgRot = _player.transform.rotation;
		_dummyOrgRot = _dummy.transform.rotation;
	}

	public void Reset(){
		_dummy.transform.position = _dummyOrgPos;
		_player.transform.rotation = _playerOrgRot;
		_dummy.transform.rotation = _dummyOrgRot;
	}

	// Update is called once per frame
	void Update ()
	{
		float speed = Time.deltaTime * 3.0f;
		Vector3 pos;
		if (_interactive){
			pos = _dummy.transform.position;
			_dummy.transform.Translate ( -_dummy.transform.right * _mouseInfo.x, Space.World );
			_dummy.transform.Translate ( -_dummy.transform.forward * _mouseInfo.y, Space.World );
			_dummy.transform.position = new Vector3( _dummy.transform.position.x, pos.y, _dummy.transform.position.z );
			if (_dummy.transform.position.magnitude>_constraintRadius && _dummy.transform.position.magnitude>pos.magnitude) _dummy.transform.position = pos;
			_mouseInfo.x = _mouseInfo.y = 0;
			gameObject.transform.position = Vector3.Slerp(gameObject.transform.position, _dummy.transform.position, speed);
			gameObject.transform.rotation = Quaternion.Slerp(gameObject.transform.rotation, _dummy.transform.rotation, speed);
		}else if (_interpolate){
			gameObject.transform.position = Vector3.Slerp(gameObject.transform.position, _target.transform.position, speed);
			gameObject.transform.rotation = Quaternion.Slerp(gameObject.transform.rotation, _target.transform.rotation, speed*2.0f);
			if (_lookAtZero) gameObject.transform.LookAt (_viewZero);
			if (Vector3.Distance (gameObject.transform.position, _target.transform.position)<0.05f){
				_dummy.transform.position = _target.transform.position;
				_dummy.transform.rotation = _target.transform.rotation;
				_interpolate = false;
				if (!_overHead) _interactive = true;
			}
		}
		if (_dummy.transform.position.y<minHeight){
			pos = _dummy.transform.position;
			pos.y = minHeight;
			_dummy.transform.position = pos;
		}
		if (gameObject.transform.position.y<minHeight){
			pos = gameObject.transform.position;
			pos.y = minHeight;
			gameObject.transform.position = pos;
		}
	}

	public void SetThumbnailCamera(bool mode){
		if (mode){
			_storedPosition = transform.position;
			_storedRotation = transform.localEulerAngles;

			float height = (_constraints.x>_constraints.y) ? _constraints.x * 0.9f : _constraints.y * 1.3f;

			transform.position = new Vector3(0.0f, height, -height/25);
			transform.rotation = Quaternion.Euler (90.0f, 0.0f, 0.0f);

			if (renderTexture == null) renderTexture = new RenderTexture(416, 336, 24);

			//_storedRT = Camera.main.targetTexture;

			Camera.main.targetTexture = renderTexture;
			RenderTexture.active = renderTexture;
	
			Camera.main.Render ();
		}else{
			transform.position = _storedPosition;
			transform.localEulerAngles = _storedRotation;

			//if (_storedRT==null) _storedRT = new RenderTexture(1024, 768, 24);
			RenderTexture.active = null;	
			Camera.main.targetTexture = null;
			Debug.Log ("CameraScript.SetThumbnailCamera viewport aspect " + Camera.main.aspect);
			Camera.main.ResetAspect();
			Debug.Log ("CameraScript.SetThumbnailCamera viewport aspect after RespectAspect:" + Camera.main.aspect);
		}
	}

	public void GrabThumbnail(GardenScript gardenScript, bool saveProject){
		_gardenScript = gardenScript;
		_saveProject = saveProject;
		_takeScreenShot = true;
		SetThumbnailCamera(true);
	}
	
	void OnPostRender(){
		if (_takeScreenShot){
			//RenderTexture rt = RenderTexture.active;
			Debug.Log ("CameraScript.OnPostRender");// + rt.width + " x " + rt.height);

			int width = 416;
			int height = 336;

			var tex = new Texture2D(width, height);
			tex.ReadPixels(new Rect(0, 0, width, height), 0, 0);
			tex.Apply(false);

			//var thumbnail = ResizeTexture(tex, 416, 336);

			if (_saveProject){
				Debug.Log ("CameraScript.OnPostRender _saveProject=True");
				string str = System.Convert.ToBase64String(tex.EncodeToPNG());
				_gardenScript.SaveProject(str);
				_saveProject = false;
			}

			_takeScreenShot = false;
			SetThumbnailCamera(false);
		}
	}

	public Texture2D ResizeTexture(Texture2D tex, int width, int height){
		Texture2D texture = new Texture2D(width, height, TextureFormat.RGBA32, false);
		if (texture == null) return null;
		
		for(int x=0; x<width; x++){
			for(int y=0; y<height; y++){
				Color c = tex.GetPixelBilinear((float)x/(float)width, (float)y/(float)height);
				texture.SetPixel(x, y, c);
			}
		}

		return texture;
	}

	public IEnumerator TakeScreenShot(int width, int height)
	{
		yield return new WaitForEndOfFrame();
		
		Camera camOV = Camera.main;
		
		RenderTexture currentRT = RenderTexture.active;

		var renderTexture = new RenderTexture(width, height, 0);

		camOV.targetTexture = renderTexture;
		RenderTexture.active = camOV.targetTexture;
		camOV.Render();

		Texture2D imageOverview = new Texture2D(width, height, TextureFormat.RGB24, false);
		imageOverview.ReadPixels(new Rect(0, 0, width, height), 0, 0);
		imageOverview.Apply();
		
		RenderTexture.active = currentRT;	
		camOV.targetTexture = null;

		SetThumbnailCamera(false);
		
		if (_gardenScript != null){
			Debug.Log ("CameraScript.GrabThumbnail _gardenScript not null _saveProject=" + _saveProject);
			
			if (_saveProject){
				string thumbnail = System.Convert.ToBase64String(imageOverview.EncodeToPNG());
				_gardenScript.SaveProject(thumbnail);
				_saveProject = false;
			}
		}
	}

	public void SnapshotSelected(Snapshot snapshot){
		_cameraID = 0;
		_interactive = false;
		_interpolate = true;
		_lookAtZero = false;
		_overHead = false;
		_target.transform.position = snapshot.Position * 0.001f;
		_target.transform.rotation = Quaternion.Euler (snapshot.Rotation);
	}

	public void PointCamera( Vector3 pos ){
		Vector3 vec = new Vector3( -pos.y, pos.x, 0 );
		vec /= 10;
		Vector3 rot = _dummy.transform.eulerAngles;
		float prevX = rot.x;
		rot.x += vec.x;
		rot.y += vec.y;
		if (rot.x<100 && rot.x>80) rot.x = prevX;//Avoid gimbal lock
		//Debug.Log ("CameraScript.PointCamera rotation h:" + rot.y + " p:" + rot.x + " b:" + rot.z);
		_dummy.transform.rotation = Quaternion.Euler ( rot );
	}

	public Snapshot GetOverheadCameraAsSnapshot(){
		float height = (_constraints.x>_constraints.y) ? _constraints.x * 0.9f : _constraints.y * 1.3f;
		Vector3 position = new Vector3(0.0f, height, -height/25) * 1000.0f;
		Vector3 rotation = new Vector3 (90.0f, 0, 0);
		return new Snapshot (position, rotation);
	}

	public void SetCamera(int i, bool direct){
		Debug.Log ("Camera.SetCamera " + i + " " + _constraints.ToString());
		float size = Math.Max(_constraints.x, _constraints.y);
		float height = (_constraints.x>_constraints.y) ? _constraints.x * 0.9f : _constraints.y * 1.3f;

		_overHead = false;
		_dummy.transform.rotation = _dummyOrgRot;
		_dummy.transform.position = _dummyOrgPos;
		_player.transform.position = Vector3.zero;

		switch(i){
		case 1://overhead
			_target.transform.position = new Vector3(0.0f, height, -height/25);
			_target.transform.rotation = Quaternion.Euler (90.0f, 0.0f, 0.0f);
			_player.transform.rotation = Quaternion.identity;
			_overHead = true;
			break;
		case 2://front
			_target.transform.position = new Vector3(0.0f, 2.5f, size*0.9f);
			_target.transform.rotation = Quaternion.Euler (0.0f, 180.0f, 0.0f);
			_player.transform.rotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
			break;
		case 3://back
			_target.transform.position = new Vector3(0.0f, 2.5f, -size*0.9f);
			_target.transform.rotation = Quaternion.Euler (0.0f, 0.0f, 0.0f);
			_player.transform.rotation = Quaternion.Euler(0.0f, 180.0f, 0.0f);
			break;
		case 4://left
			_target.transform.position = new Vector3(size*0.9f, 2.5f, 0.0f);
			_target.transform.rotation = Quaternion.Euler (0.0f, 270.0f, 0.0f);
			_player.transform.rotation = Quaternion.Euler(0.0f, 90.0f, 0.0f);
			break;
		case 5://right
			_target.transform.position = new Vector3(-size*0.9f, 2.5f, 0.0f);
			_target.transform.rotation = Quaternion.Euler (0.0f, 90.0f, 0.0f);
			_player.transform.rotation = Quaternion.Euler(0.0f, 270.0f, 0.0f);
			break;
		}

		if (direct){
			gameObject.transform.position = _target.transform.position;
			gameObject.transform.rotation = _target.transform.rotation;
			_lookAtZero = false;
		}else{
			if (i!=1){
				_dummy.transform.parent = null;
				_dummy.transform.position = _target.transform.position;
				_dummy.transform.rotation = _target.transform.rotation;
				_dummy.transform.parent = _player.transform;
			}
			_lookAtZero = (i==1 || (_cameraID==1 && i!=1) || (_cameraID==2 && i==3) || (_cameraID==3 && i==2) || (_cameraID==4 && i==5) || (_cameraID==4 && i==4)) ? false : true;
		}

		_cameraID = i;
		_interactive = false;
		_interpolate = !direct;
	}

	public void Move( float x, float y ){
		//Debug.Log(string.Format("CameraScript.Move {0:0.00},{1:0.00}", x, y));
		_interactive = true;
		if (_overHead){
			_dummy.transform.rotation = _dummyOrgRot;
			_dummy.transform.position = _dummyOrgPos;
			_player.transform.position = Vector3.zero;
		}
		_overHead = false;
		_mouseInfo = new Vector2(-x, y) * 0.2f;
	}

	public void MoveB( float deltaTranslation, float deltaRotation ){
		_interactive = true;
		_mouseInfo = new Vector2(deltaTranslation, deltaRotation);
	}

	public void AdjustHeight(float delta){
		_dummy.transform.Translate (0, delta*10, 0, Space.World);
		if (_overHead) gameObject.transform.Translate (0, delta*10, 0, Space.World);
	}
}

