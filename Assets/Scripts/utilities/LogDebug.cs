using UnityEngine;
using System.Collections;

public class Debug : MonoBehaviour
{
	private static string hr = "\n\n-------------------------------------------------------------------------------";
	
	
	public static void Log(object message)
	{
		#if UNITY_EDITOR
		UnityEngine.Debug.Log(message.ToString() + hr);
		#else
		#if UNITY_WEBPLAYER 
		Application.ExternalCall( "console.log", message.ToString() );
		#else
		UnityEngine.Debug.Log(message.ToString() + hr);
		#endif
		#endif
	}
	
	public static void Log(object message, Object context)
	{
		//UnityEngine.Debug.Log(message.ToString() + hr, context);
	}
	
	public static void LogError(object message)
	{
		//UnityEngine.Debug.LogError(message.ToString() + hr);
	}
	
	public static void LogError(object message, Object context)
	{
		//		UnityEngine.Debug.LogError(message.ToString() + hr, context);
	}
	
	public static void LogWarning(object message)
	{
		//		UnityEngine.Debug.LogWarning(message.ToString() + hr);
	}
	
	public static void LogWarning(object message, Object context)
	{
		//		UnityEngine.Debug.LogWarning(message.ToString() + hr, context);
	}
	
	public static void DrawLine(object m1, object m2, object m3) {
		
	}
	
}
