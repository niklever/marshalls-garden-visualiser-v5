// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.1
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using UnityEngine;
using Ucss;
using SimpleJSON;
using Random=UnityEngine.Random;

public class WWWAsset
{
	Product _product;
	List<WWWAsset> _clones;
	static List<string> specials = new List<string>(){"accessory_step_haworth_moor_sawn", "accessory_step_coach_house"};
	JSONNode _postJSON;

	public Product Product {
		get {
			return _product;
		}
	}

	GameObject _gameObject;

	public GameObject AssetGO {
		get {
			return _gameObject;
		}
		set{
			_gameObject = value;
			if (_gameObject!=null){
				InitGameObject();
			}
		}
	}

	string _bundleURL;

	public string BundleURL {
		get {
			return _bundleURL;
		}
	}

	string _name;

	public string Name {
		get {
			return _name;
		}
	}

	bool _ready;

	public bool Ready {
		get {
			return _ready;
		}
	}

	bool _rootParent = false;

	public bool RootParent {
		get {
			return _rootParent;
		}
		set {
			_rootParent = value;
		}
	}

	bool _loading;
	public bool Loading{
		set{
			_loading = value;
		}
		get{
			return _loading;
		}
	}

	bool _gardenLoad;
	public bool GardenLoad {
		set {
			_gardenLoad = value;
		}
	}
	bool _updating = false;

	public event EventHandler<WWWAssetReadyEventArg> OnReadyEvent = (s, e) => { };
	public event EventHandler<WWWAssetReadyEventArg> OnErrorEvent = (s, e) => { };

	private void InitGameObject(){
		if (_gameObject == null) {
			Debug.Log ("WWWAsset.InitGameObject _gameObject is null url:" + _bundleURL);
			return;
		}
		_gameObject.transform.parent = null;
		_gameObject.transform.position = _product.Position * 0.001f;
		_gameObject.transform.rotation = Quaternion.Euler (0.0f, _product.Rotation, 0.0f);

		if (_product.Type.Equals ("greenhouse") || _product.Type.Equals ("summerhouse")) {
			_gameObject.transform.localScale = new Vector3 (-1, 1, -1);
		} else {
			_gameObject.transform.localScale = new Vector3 (1, 1, -1);
		}
		_gameObject.name = _product.Name;

		int i = specials.IndexOf (_product.Type + "_" + _product.ProductName);
		Debug.Log ("WWWAsset.InitGameObject " + (_product.Type + "_" + _product.ProductName) + " " + i + " " + _bundleURL);

		switch (i) {
		case 0://"accessory_step_haworth_moor_sawn"
			//InitStepHaworthMoorSawn();
			InitStep ();
			break;
		case 1://"accessory_step_coach_house"
			InitStep ();
			break;
		}

		//if (_product.Type.Equals ("post")) GetPostTexture();

		OnReadyEvent (this, new WWWAssetReadyEventArg (this));

		//if (_rootParent) _gameObject.transform.parent = GardenScript.Root;
		_ready = true;
		_loading = false;

		ReapplyShaders ();

		if (_gardenLoad) GV_UI.GardenScript.LoadNextProduct ();
	}

	void ReapplyShaders(){
		if (_gameObject == null)
			return;
		
		Renderer[] renderers;
		Material[] materials;
		string[] shaders;

		renderers = _gameObject.GetComponentsInChildren<Renderer>();

		foreach(Renderer rend in renderers)
		{
			materials = rend.sharedMaterials;
			shaders =  new string[materials.Length];

			for( int i = 0; i < materials.Length; i++)
			{
				shaders[i] = materials[i].shader.name;
			}        

			for( int i = 0; i < materials.Length; i++)
			{
				materials[i].shader = Shader.Find(shaders[i]);
			}
		}
	}

	void GetPostTexture(){
		_postJSON = GardenScript.GetProductJSON(_product.Type + "_" + _product.ProductName);

		if (_postJSON!=null){
			string url = GVWebService.ResourcePath + "texture/" + _postJSON["texture"] + _product.Color + ".jpg";

			Debug.Log("WWWAsset.GetPostTexture url:" + url);

			UCSS.HTTP.GetTexture ( url, this.LoadTextureCallback, this.LoadTextureError);
		}
	}

	void LoadTextureError(string error, string id){
		Debug.Log("TextureCache.Load download error:" + error);
		string imageName = _postJSON["texture"] + _product.Color + ".jpg";
		GV_UI.ShowError ( imageName + " not found");
	}

	void LoadTextureCallback(Texture2D texture, string id){
		Debug.Log("TextureCache.Load texture:" + id);
		_gameObject.GetComponent<Renderer>().material.color = Color.white;
		_gameObject.GetComponent<Renderer>().material.mainTexture = texture;
	}

	private void InitStep(){
		int count = (int)((_product.Size - 710) / 570);
		//count can be 1,2 or 3. If 3 do nothing
		GameObject go;
		int i, j;
		int n = 3 - count;
		Color col = new Color(0.8f, 0.8f, 0.8f, 1.0f);
		for(i=0; i<3; i++){
			go = _gameObject.transform.GetChild (i+2).gameObject;
			Debug.Log ("InitStepCoachHouse: i:" + i + " n:" + n + " count:" + count + " " + go.name);
			go.SetActive ((i-n)>=0);
			go.transform.localPosition = new Vector3(0.570f*n, 0f, 0f);
		}
		//Debug.Log ("InitStepCoachHouse: childCount:" + _gameObject.transform.childCount);
		for(i=0; i<_gameObject.transform.childCount; i++){
			go = _gameObject.transform.GetChild (i).gameObject;
			if (go.GetComponent<Renderer>()==null) continue;
			for(j=0; j<go.GetComponent<Renderer>().materials.Length; j++) go.GetComponent<Renderer>().materials[j].color = col;
		}
	}

	public WWWAsset (Product product, string name)
	{
		_loading = true;
		_product = product;
		_gardenLoad = true;

		initAsset(name);
	}

	void initAsset(string name){
		Debug.Log("WWWAsset initAsset name:" + name);
		_name = name;
		string folder;
#if UNITY_WEBPLAYER
		folder = "web";
#elif UNITY_IOS
		folder = "ios";
#elif UNITY_ANDROID
		folder = "android";
#else
		folder = "web";
#endif
		_bundleURL = string.Format ( "{0}asset_bundles/{1}/{2}.unity3d", GVWebService.ResourcePath, folder, name);
		_ready = false;
		bool found = false;
		AssetBundle assetBundle;

		if (GardenScript.AssetBundles.TryGetValue(_bundleURL, out assetBundle)){
			Debug.Log ("WWWAsset initAsset assetBundle found " + assetBundle);
			if (assetBundle != null){
				if (assetBundle.mainAsset == null) {
					/*foreach(WWWAsset asset in GardenScript.Assets){
						if (asset == this) continue;
						if (asset.BundleURL.Equals(_bundleURL)){
							found = true;
							if (asset.AssetGO==null){
								Debug.Log ("WWWAsset adding clone " + _bundleURL);
								asset.AddClone(this);
							}else{
								_gameObject = GardenScript.CloneGameObject(asset.AssetGO);
								InitGameObject();
							}
							break;
						}
					}
					if (!found) {*/
						assetBundle.Unload (false);
						GardenScript.AssetBundles.Remove (_bundleURL);
					//}
				} else {
					_gameObject = GardenScript.InstantiateGameObject (assetBundle);
					InitGameObject ();
					found = true;
				}
			}else{
				Debug.Log ("WWWAsset trying to instantiate a null asset. unity3d file was not found on first pass " + _bundleURL);
				foreach(WWWAsset asset in GardenScript.Assets){
					if (asset.BundleURL.Equals(_bundleURL)){
						if (asset.AssetGO == null) {
							asset.AddClone (this);
						}else{
							_gameObject = GardenScript.CloneGameObject(asset.AssetGO);
							InitGameObject();
						}
						found = true;
						break;
					}
				}
			}
		}else if (GardenScript.Assets!=null){
			Debug.Log ("WWWAsset initAsset GardenScript.Assets!=null");
			foreach(WWWAsset asset in GardenScript.Assets){
				if (asset == this) continue;
				if (asset.BundleURL.Equals(_bundleURL)){
					found = true;
					if (asset.AssetGO==null){
						asset.AddClone(this);
					}else{
						_gameObject = GardenScript.CloneGameObject(asset.AssetGO);
						InitGameObject();
					}
					break;
				}
			}
		}

		if (!found) Load();
	}

	public void AddClone(WWWAsset asset){
		if (_clones==null) _clones = new List<WWWAsset>();
		_clones.Add (asset );
	}

	void UpdateClones(bool mode){
		if (_clones != null){
			foreach(WWWAsset asset in _clones){
				asset.Loading = mode;
			}
		}
	}

	void Load(){
		Debug.Log("WWWAsset Load url:" + _bundleURL);	
		GardenScript.Assets.Add (this);
		GardenScript.AssetBundles.Add (_bundleURL, null);

		UCSS.HTTP.GetAssetBundle (_bundleURL, new EventHandlerAssetBundle(this.LoadCallback), new EventHandlerServiceError(this.LoadError));
	}

	void LoadError(string error, string id){
		Debug.Log("GVWebService.GetThumbnail download error:" + error);
	}

	void LoadCallback(AssetBundle assetBundle, string id){
		GardenScript.AssetBundles[_bundleURL] = assetBundle;
		_gameObject = GardenScript.InstantiateGameObject(assetBundle);
		InitGameObject();
		if (_clones != null){
			foreach(WWWAsset asset in _clones){
				asset.AssetGO = GardenScript.CloneGameObject(_gameObject);
				asset.InitGameObject ();
			}
		}
	}

	public void Change(Product product){
		Debug.Log ("WWWAsset.Change " + product);
		string name = (product.Color=="") ? (product.Type + "_" + product.ProductName) : (product.Type + "_" + product.ProductName + "_" + product.Color);
		if (!name.Equals (_name)){
			GardenScript gardenScript = GV_UI.GardenScript;
			gardenScript.ClearSelectBox();
			GardenScript.Destroy(_gameObject);
			_gameObject = null;
			_product.Size = product.Size;
			_product.Color = product.Color;
			_product.Type = product.Type;
			_product.ProductName = product.ProductName;
			_product.CopyPDFProperties(product, false);
			_updating = true;
			initAsset(name);
		}else if (_product.ProductName.Contains ("step") && _product.Size!=product.Size){
			_product.Size = product.Size;
			InitStep();
		}
	}
}
