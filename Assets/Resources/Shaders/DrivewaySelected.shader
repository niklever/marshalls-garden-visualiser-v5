﻿Shader "Custom/DrivewaySelected" {
	Properties {
		_Color ( "Main Color", Color) = (1,1,1,1)
	    _MainTex ("Base (RGB)", 2D) = "white" {}
	    _Emission ("Emmisive Color", Color) = (0.1,0.1,0.1,0)
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		Cull Off

		CGPROGRAM
		#pragma surface surf Lambert  vertex:vert

		sampler2D _MainTex;
		fixed4 _Color;
		fixed4 _Emission;

		struct Input {
			float2 uv_MainTex;
		};
		
		float4x4 _Rotation;
		
		void vert (inout appdata_full v) {
            float2 uv = v.texcoord.xy;
            v.texcoord.xy = mul (_Rotation, float4(uv,0,1)).xy;
        }
        
		void surf (Input IN, inout SurfaceOutput o) {
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb * 1.5;
			o.Alpha = c.a;
			o.Emission = _Emission;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
