﻿Shader "Custom/DoubleSided" {
    Properties {
        _MainTex ("Base (RGB)", 2D) = "white" { }
    }
    SubShader {
        // We use a simple white material, and apply the main texture.
        Pass {
            Material {
                Diffuse (1,1,1,1)
            }
            Lighting On
            Cull Off
            SetTexture [_MainTex] {
                Combine Primary * Texture
            }
        }
    }
}