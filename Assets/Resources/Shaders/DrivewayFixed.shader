﻿Shader "Custom/DrivewayFixed" {
	Properties { 
		_Color ( "Main Color", Color) = (1,1,1,1)
		_MainTex ("Base", 2D) = "white" {}
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		Pass {
			Material { Diffuse (1,1,1,0) Ambient (1,1,1,0) }
			Lighting On
			SetTexture [_MainTex] {
				matrix [_Rotation]
	           	constantColor[_Color]
	           	combine constant * texture
		       	//combine texture * primary double, texture
			}
		}
	}
	FallBack "Diffuse"
}
