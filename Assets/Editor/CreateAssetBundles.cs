﻿using UnityEditor;

public class CreateAssetBundles 
{
	[MenuItem("Assets/Build Asset Bundles")]
	static void DoCreateAssetBundles() 
	{
		string folder = "";
		BuildTarget target;
		// Bring up save panel
		#if UNITY_IPHONE	
		//folder = "ios/";
		folder = "test/";
		target = BuildTarget.iOS;
		#elif UNITY_ANDROID
		folder = "android/";
		target = BuildTarget.Android;
		#elif UNITY_WEBPLAYER
		folder = "web/";
		target = BuildTarget.WebPlayer;
		#endif	

		//string path = "Bundles/" + folder;
		string path = "../distribute/www/asset_bundles/" + folder;
		BuildPipeline.BuildAssetBundles (path, BuildAssetBundleOptions.None, target);
	}
}